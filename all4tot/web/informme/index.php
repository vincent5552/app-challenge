<?php
@session_start();
//check for login !!

$_SESSION['RedirectURL']="informme/";
if( !isset($_SESSION['uid']) || trim($_SESSION['uid'])=="" ){

echo ("<SCRIPT LANGUAGE='JavaScript'>
    window.alert('Please Login With Your Google Account to Access This Page')
    window.location.href='../login.php';
    </SCRIPT>");

}else{
	$temp_userid=$_SESSION['uid'];	
}
	
require_once("pre_process_text.php");

	function geticon($n){
		$image_file="";
		
		switch ($n) {
			case "Welcome":
				$image_file="./images/welcome.png";
				break;
			case "Supports":
				$image_file="./images/support.png";
				break;
			case "Visit Reminder":
				$image_file="./images/visit.png";
				break;
			case "Nutrition":
				$image_file="./images/nutrition.png";
				break;	
			case "Physical Activity":
				$image_file="./images/activity.png";
				break;		
			case "Happy Birthday":
				$image_file="./images/birthday.png";
				break;	
			case "Rest":
				$image_file="./images/rest.png";
				break;		
			case "General":
				$image_file="./images/general.png";
				break;	
		}//enf of switch
		return $image_file;
	}//end of function
?>

<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <title>All4TOT - TXT4Tots Message</title>
  <link rel="shortcut icon" href="http://all4tot.org/web/favicon.ico" />
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel='stylesheet' href='css/style.css'>
  <script>
  $(function() {
    $( "#tabs" ).tabs();
	
	var current;
					
			function rotate() {
			
				// This seems like a sucky way to do it, but you can't select by classes because they execute in order
							
				if (current == 1) {
					$("#block-1").removeClass().addClass("active");
					$("#block-2").removeClass().addClass("non-active-top");
					$("#block-3").removeClass().addClass("non-active-bottom");
				} else if (current == 2) {
					$("#block-1").removeClass().addClass("non-active-bottom");
					$("#block-2").removeClass().addClass("active");
					$("#block-3").removeClass().addClass("non-active-top");
				} else if (current == 3) {
					$("#block-1").removeClass().addClass("non-active-top");
					$("#block-2").removeClass().addClass("non-active-bottom");
					$("#block-3").removeClass().addClass("active");
				}
				<?
				for($n=1;$n<count($_SESSION['tot_name_array']);$n++){
					$tempn=($n)*3+1;
					$tempn1=$tempn+1;
					$tempn2=$tempn+2;
				?>
				else if (current == <?echo $tempn ?>) {
					$("#block-<?echo $tempn ?>").removeClass().addClass("active");
					$("#block-<?echo $tempn1 ?>").removeClass().addClass("non-active-top");
					$("#block-<?echo $tempn2 ?>").removeClass().addClass("non-active-bottom");
				}
				else if (current == <?echo $tempn1 ?>) {
					$("#block-<?echo $tempn ?>").removeClass().addClass("non-active-bottom");
					$("#block-<?echo $tempn1 ?>").removeClass().addClass("active");
					$("#block-<?echo $tempn2 ?>").removeClass().addClass("non-active-top");
				}
				else if (current == <?echo $tempn2 ?>) {
					$("#block-<?echo $tempn ?>").removeClass().addClass("non-active-top");
					$("#block-<?echo $tempn1 ?>").removeClass().addClass("non-active-bottom");
					$("#block-<?echo $tempn2 ?>").removeClass().addClass("active");
				}
				<?
				}
				?>
			}
			
			$("#rotator div").click(function() {
			
				// Enables reversing, idea via Andrea Canton: https://twitter.com/andreacanton/status/24954634279849985
				current = this.id.substr(6);			
				rotate();		
			});
  });
  
  
  
  </script>
  <style>
		#rotator { width: 920px; height: 280px; position: relative; background: white; padding: 20px; }
		
		
		<?
		for($n=0;$n<count($_SESSION['tot_gender_array']);$n++){
		if($_SESSION['tot_gender_array'][$n]==1){
		?>
		#block-<?echo (($n*3)+1)?>  { background: #d5fcff; }
		#block-<?echo (($n*3)+2)?>  { background: #e1ffd5; }
		#block-<?echo (($n*3)+3)?>  { background: #ffffd8; }
		<?
		}else{
		?>
		#block-<?echo (($n*3)+1)?> { background: #FBDBFA; }
		#block-<?echo (($n*3)+2)?> { background: #FEFBCD; }
		#block-<?echo (($n*3)+3)?> { background: #DECFFC; }
		<?
			}
		}
		?>
		
		#rotator > div { 
			position: absolute; 
			overflow: hidden;
			-webkit-transition: all 0.5s ease;
			-moz-transition: all 0.5s ease;
			-o-transition: all 0.5s ease;
		}
		#rotator > div > div { 
			padding: 20px; 
		}
		#rotator > div > div img {
			float: right;
			margin: 0 0 2px 10px;
		}
		#rotator .active { top: 20px; left: 20px; width: 580px; height: 280px; }
		#rotator .non-active-top { top: 20px; left: 620px; height: 130px; width: 320px; }
		#rotator .non-active-bottom { top: 170px; left: 620px; height: 130px; width: 320px; }		
		#rotator .non-active-top:hover, #rotator .non-active-bottom:hover { 
			cursor: pointer;
			-webkit-box-shadow: 0 0 10px rgba(0,0,0,0.4); 
			-moz-box-shadow: 0 0 10px rgba(0,0,0,0.4); 
			box-shadow: 0 0 10px rgba(0,0,0,0.4); 
		}
		#rotator h2 {
			text-align: center; 
			line-height: 130px;
		}
		#rotator .active h2 {
			display: none;
		}
		.header {
	font-size: 24px;
}

#tabs{margin-left:auto;
margin-right:auto;
width:990px; }

body {background-color:#333333;}

	
</style>
</head>
<body bgColor="#333333">

<center>
<iframe src="../homepage_header.html" width="1000" height="270" scrolling="no" frameBorder="0" style="height:270; width:1000px;margin: 0px 0px -5px 0px; " >
</iframe>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#FFFFFF" >
<tr><td height=20></td></tr>
<tr><td>
<div id="tabs">
  <ul>
 <?   
  for($n=0;$n<count($_SESSION['tot_name_array']);$n++){
	echo "<li><a href=\"#tabs-".($n+1)."\">".$_SESSION['tot_name_array'][$n]." - ".$_SESSION['tot_dob_age'][$n]."</a></li>";
  }
  ?>
  </ul>
<? 
  for($n=0;$n<count($_SESSION['tot_name_array']);$n++){
	//do the query !!
	$photo_0="";
	$photo_1="";
	$photo_2="";
	$tot_in_week=$_SESSION['tot_age_week'][$n];
	$sql_a1="select MessageType, TXT4TotsMessage from vivianpe_all4tot.TXT4Tots where Age_In_Week between '".($tot_in_week-20)."' and '". ($tot_in_week+20)."'  order by RAND() LIMIT 3";
	$result_sql_a1=mysql_query($sql_a1);
	
	$text_message_title=array();
	$text_message_content=array();
	
	while ($row_a1 = mysql_fetch_assoc($result_sql_a1)) {
		array_push($text_message_title, $row_a1["MessageType"]);
		//array_push($text_message_content, mysql_real_escape_string($row_a1["TXT4TotsMessage"]));	
		array_push($text_message_content,  str_replace("'","''", $row_a1["TXT4TotsMessage"]));	
		str_replace("'","''", $query);
	}
	$text_title_0=$text_message_title[0];
	$text_title_1=$text_message_title[1];
	$text_title_2=$text_message_title[2];
	
	 echo "<div id=\"tabs-". ($n+1)."\">";

?>
	<div id="page-wrap">
	<h1>Message of the day, Powered by TXT4Tots</h1>
	<p>Click on the smaller blocks on right to rotate. Learn more about it at <a href="http://www.hrsa.gov/healthit/txt4tots/" target="_blank">TXT4Tots Web Site</a></p>
		<div id="rotator">
			<div id="block-<?php echo ($n*3+1) ?>" class="active">
				<h2><? echo $text_title_0 ?></h1>
				<div>
					<h1><? echo $text_title_0 ?></h1>
					<img src='<?echo geticon($text_title_0);?>' alt="space frog">
					<p><? echo  $text_message_content[0]  ?></p>
				</div>
			</div>
			
			<div id="block-<?php echo ($n*3+2) ?>" class="non-active-top">
				<h2><? echo $text_title_1 ?></h1>
				<div>
					<h1><? echo $text_title_1 ?></h1>
					<img src='<?echo geticon($text_title_1); ?>' alt="space frog">
					<p><? echo  $text_message_content[1]  ?></p>
				</div>			
			</div>
			
			<div id="block-<?php echo ($n*3+3) ?>" class="non-active-bottom">
				<h2><? echo $text_title_2 ?></h1>
				<div>
					<h1><? echo $text_title_2 ?></h1>
					<img src='<?echo geticon($text_title_2);?>' alt="space frog">
					<p><? echo  $text_message_content[2]  ?></p>
				</div>		
			</div>
		
		</div>
		<p></p>
	Other useful infomration and web site links for your toddler - Check them out !!
	<p></p>
	<p><a href="http://www.cdc.gov/ncbddd/childdevelopment/positiveparenting/index.html" target="_blank">Positive Parenting Tips</a>
	 / <a href="http://www.cdc.gov/vaccines/parents/infants-toddlers.html" target="_blank">Vaccines for Your Children</a>
	 / <a href="http://www.fda.gov/Safety/Recalls/default.htm" target="_blank">FDA Recall Information</a> </P>
	 <p><a href="http://kids.usa.gov/" target="_blank">Kids USA</a>
	 / <a href="http://www.nlm.nih.gov/medlineplus/toddlerhealth.html" target="_blank">MdelinePlus</a>
	 / <a href="http://www.mayoclinic.com/health/infant-and-toddler-health/MY00362" target="_blank">Mayo Clinic for Infant and Toddler Health</a>
	 / <a href="http://www.healthfinder.gov/HealthTopics/Population/babies-and-toddlers" target="_blank">Health Finder</a> </P>	

	 
	</div>	
 </div>	
  
 
	<?
	
}	
	
  ?>
  
  
  
</div>

</td></tr></table>
<iframe src="../footer.html" width="1000" height="75" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>