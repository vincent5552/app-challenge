<?
	session_start();
	if(empty($_SESSION['uid'])) {
		header('Location: ../login.php');
	}
	$_SESSION['mytab'] = $_POST['mytabid'];
	
	$_SESSION['RedirectURL'] = "profile/index.php";

	$Name = $_POST['Name'];
	$DOB = $_POST['DOB'];
	$Gender = $_POST['Gender'];
	$Immunization = 0;
	$BDReminder = 0;
	$CheckUpsReminder = 0;

	if(isset($_POST['Immunization'])) {
		$Immunization = 1;
		$_SESSION['ImmunizationChecked'] = 1;
	} else {
		$_SESSION['ImmunizationChecked'] = 0;
	}
	if(isset($_POST['BirthdayReminder'])) {
		$BDReminder = 1;
		$_SESSION['BDReminderChecked'] = 1;
	} else {
		$_SESSION['BDReminderChecked'] = 0;
	}
	if(isset($_POST['CheckUps_ExamsReminder'])) {
		$CheckUpsReminder = 1;
		$_SESSION['CheckUpsChecked'] = 1;
	} else {
		$_SESSION['CheckUpsChecked'] = 0;
	}
	
	if($Immunization==1||$BDReminder==1||$CheckUpsReminder==1) {
		$_SESSION['TOTName'] = $Name;
		$_SESSION['DOB'] = $DOB;
		$_SESSION['Gender'] = $Gender;
		header('Location: Login.php');
		exit;
	}	

	$http = "http://all4tot.org/systemAPI/action.php?action=AddProfile&userID=".$_SESSION['uid']."&Name=".$Name."&Gender=".$Gender."&DOB=".$DOB;
	$g = file_get_contents($http);
	$dis = json_decode($g,true);

	header('Location: index.php');
	exit;
?>