<?php
require_once 'Zend/Loader.php';

session_start();

Zend_Loader::loadClass('Zend_Gdata');
Zend_Loader::loadClass('Zend_Gdata_AuthSub');
Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
Zend_Loader::loadClass('Zend_Gdata_HttpClient');
Zend_Loader::loadClass('Zend_Gdata_Calendar');

$my_calendar = 'http://www.google.com/calendar/feeds/default/private/full';
 
if (!isset($_SESSION['cal_token'])) {
	if (isset($_GET['token'])) {
		// You can convert the single-use token to a session token.
		$session_token =
			Zend_Gdata_AuthSub::getAuthSubSessionToken($_GET['token']);
		// Store the session token in our session.
		$_SESSION['cal_token'] = $session_token;
	} else {
		// Display link to generate single-use token
		$googleUri = Zend_Gdata_AuthSub::getAuthSubTokenUri(
			'http://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'],
			$my_calendar, 0, 1);

		echo "<script type=\"text/javascript\">";
		echo "	window.location = \"".$googleUri."\"";
		echo "</script>";

		exit();
	}
}

header('Location: CreateProfileWithCalendar.php');
exit;
 
?>

