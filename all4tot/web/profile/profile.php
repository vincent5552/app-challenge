<?
session_start();
$_SESSION['RedirectURL'] = "profile/profile.php";
if(empty($_SESSION['uid'])) {
	header('Location: ../login.php');
	exit;
}

?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8" />
  <title>ALL4TOT - Profile</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <link rel='stylesheet' href='style_w.css'>
  <script>
  <?
   if($_SESSION['Display_Error_Weather'])
	  echo "alert('We Currently Only Support US Cities, Please Re-Select. Thanks!!');";
  ?>
  </script>
<style type="text/css">
#wrapper {
	background-color: #EEEEEE;
}
#won	{
	font-size: 11px;
}
td	{
	font-size: 13px;
}
th {
	align: left;
	background-color: #81BEF7;
}
</style>
</head>
<body bgColor="#333333">

<center>
<iframe src="../homepage_header.html" width="1000" height="270" scrolling="no" frameBorder="0" style="height:270; width:1000px;margin: 0px 0px -5px 0px; " >
</iframe>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#FFFFFF" >
<tr><td height=20></td></tr>
<tr><td>
<div id="tabs">
  <ul>
<?
	
	$http = "http://all4tot.org/systemAPI/action.php?action=DisplayProfile&userID=".$_SESSION['uid'];
	$g = file_get_contents($http);
	$TOTS = json_decode($g,true);

	foreach($TOTS['DisplayProfile'] as $TOT => $value) {
		echo "<li><a href='#tabs_".$value["TOTID"]."'>".$value["Name"]."</a></li>";
	}
	echo "<li><a href='#tabs_0'>+</a></li>";
?>
  </ul>
<?
	$i = 0;
	foreach($TOTS['DisplayProfile'] as $TOT => $value) {
		//title
		echo "<div id='tabs_".$value["TOTID"]."'>";
		echo "	<table width=\"100%\">";
		echo 	"<tr><th width=\"200\">Name: </th><td align=\"left\">".$value["Name"]."</td></tr>";
		echo 	"<tr><th>Date of Birth: </th><td align=\"left\">".$value["DOB"]."</td></tr>";
		echo 	"<tr><th>Gender: </th><td align=\"left\">";
		if($value["Gender"]==1)
			echo "Male";
		else
			echo "Female";
		echo "	</td></tr>";
		echo "	</table>";

		echo "	<ul>";
		echo "		<li><a href='#tabs_".$value["TOTID"]."_Profile'>Profile</a></li>";
		echo "		<li><a href='#tabs_".$value["TOTID"]."_Measurement'>Measurement</a></li>";
		echo "		<li><a href='#tabs_".$value["TOTID"]."_Subscribe'>Subscribe</a></li>";
		echo "	</ul>";
		//	profile div
		echo "	<div id='tabs_".$value["TOTID"]."_Profile'>";
		echo "	<form id='UpdateForm_".$value["TOTID"]."_Profile' name='UpdateProfile' method='POST' action='UpdateProfile.php'>";
		echo "	<table width=\"100%\">";
		echo "		<tr><th width=\"200\">Name:</th> <td align=\"left\"><input type='text' name='Name' value='".$value["Name"]."'></td></tr>";
		echo "		<tr><th>Date of Birth:</th> <td align=\"left\"><input type='text' name='DOB' value='".$value["DOB"]."'> Day/Month/Year</td></tr>";
		if($value["Gender"]==1)
			echo "		<tr><th>Gender:</th> <td align=\"left\"><input type='radio' name='Gender' value=1 checked> Male <input type='radio' name='Gender' value=2 > Female</td></tr>";
		else
			echo "		<tr><th>Gender:</th> <td align=\"left\"><input type='radio' name='Gender' value=1 > Male <input type='radio' name='Gender' value=2 checked> Female</td></tr>";
		echo "		<tr><td colspanc=\"2\"><input type='hidden' name='TOTID' value='".$value["TOTID"]."'>";
		echo "		<input type='submit' value='Update' onClick='UpdateForm_".$value["TOTID"]."_Profile.action=\"UpdateProfile.php\";'>";
		echo "		<input type='submit' value='Remove' onClick='UpdateForm_".$value["TOTID"]."_Profile.action=\"RemoveProfile.php\";'></td></tr>";
		echo "	</table>";
		echo "	</form>";
		echo "	</div>";
		//	measurement div
		echo "	<div id='tabs_".$value["TOTID"]."_Measurement'>";
		echo "		<p>Measurement: enter the head, height, weight and measurement date for ".$value["Name"]."</p>";
		//		measurement form
		echo "	<form id='UpdateForm_".$value["TOTID"]."_Measurement' name='UpdateMeasurement' method='POST' action='UpdateMeasurement.php'>";
		echo "	<table width=\"100%\">";
		echo "		<tr><th width=\"200\">Head:</th> <td align=\"left\"><input type='text' name='Head' > cm</td></tr>";
		echo "		<tr><th>Height:</th> <td align=\"left\"><input type='text' name='Height' > cm</td></tr>";
		echo "		<tr><th>Weight:</th> <td align=\"left\"><input type='text' name='Weight' > lb.</td></tr>";
		echo "		<tr><th>Measurement date:</th> <td align=\"left\"><input type='text' name='MeasurementDate' > Day/Month/Year</td></tr>";
		echo "		<tr><td colspan=\"2\"><input type='hidden' name='TOTID' value='".$value["TOTID"]."'>";
		echo "		<input type='submit' value='Submit' ></td></tr>";
		echo "	</table>";
		echo "	</form>";
		//		measurement list
		$http = "http://all4tot.org/systemAPI/action.php?action=DisplayGrowthChart&TOTID=".$value["TOTID"];
		$g = file_get_contents($http);
		$Measurements = json_decode($g,true);
		echo "	<table border=\"0\" width=\"100%\" >";
		echo "		<tr><th>Head (cm)</th><th>Height (cm)</th><th>Weight (lb)</th><th>Measurement Date</th></tr>";
		$i = 0;
		foreach($Measurements['DisplayGrowthChart'] as $Measure => $Mvalue) {
			echo "<tr ";
			if($i%2==1)
				echo "bgcolor=\"#E0ECF8\"";
			echo ">";
			echo "	<td>".$Mvalue["Head"]."</td>";
			echo "	<td>".$Mvalue["Height"]."</td>";
			echo "	<td>".$Mvalue["Weight"]."</td>";
			echo "	<td>".$Mvalue["MeasurementDate"]."</td>";
			echo "</tr>";
			$i++;
		}
		echo "	</table>";
		echo "	</div>";
		//	subscribe
		echo "	<div id='tabs_".$value["TOTID"]."_Subscribe'>";
		echo "	<p>Subscribe: subscribe handy notifications for ".$value["Name"]."</p>";
		echo "	<form id='UpdateForm_".$value["TOTID"]."_Measurement' name='UpdateSubscribe' method='POST' action='UpdateSubscribe.php'>";
		echo "	<table width=\"100%\">";
		
		$http = "http://all4tot.org/systemAPI/action.php?action=GetMessageCategories";
		$g = file_get_contents($http);
		$MessageCategories = json_decode($g,true);
		
		echo "		<tr><th width=\"200\">Message Category:</th> <td align=\"left\">";
		$i=0;
		foreach($MessageCategories['GetMessageCategories'] as $MessageCategory => $MCvalue) {
			echo "		<input type='checkbox' name='MessageCategoryID' value=".$MCvalue["MessageCategoryID"].">".$MCvalue["MessageCategory"]." ";
			$i++;
			if($i%4==0)
				echo "<br>";
		}
		echo "		</td></tr>";
		
		$http = "http://all4tot.org/systemAPI/action.php?action=GetCarriers";
		$g = file_get_contents($http);
		$Carriers = json_decode($g,true);
		
		echo "		<tr><th>Carrier:</th> <td align=\"left\">";
		echo "			<select name=\"CarrierID\">";
		foreach($Carriers['GetCarriers'] as $Carrier => $Cvalue) {
			echo "			<option value=\"".$Cvalue["CarrierID"]."\">".$Cvalue["Name"]."</option>";
		}
		echo "			</select>";
		echo "		</td></tr>";
		echo "		<tr><th>PhoneNo:</th> <td align=\"left\"><input type='text' name='PhoneNo' >(10 digits)</td></tr>";
		
		$http = "http://all4tot.org/systemAPI/action.php?action=GetIntervals";
		$g = file_get_contents($http);
		$Intervals = json_decode($g,true);
		
		echo "		<tr><th>Receiving Interval:</th> <td align=\"left\">";
		foreach($Intervals['GetIntervals'] as $Interval => $Ivalue) {
			echo "		<input type=\"radio\" value=\"".$Ivalue["FrequencyID"]."\"> Every ".$Ivalue["Interval"]." ".$Ivalue["Type"]."</option>";
		}
		echo "		</td></tr>";
		echo "		<tr><th>Subscribe date:</th> <td align=\"left\"><input type='text' name='SubscribeDate' > Day/Month/Year</td></tr>";
		echo "		<tr><td colspan=\"2\"><input type='hidden' name='TOTID' value='".$value["TOTID"]."'>";
		echo "		<input type='submit' value='Update' ></td></tr>";
		echo "	</table>";
		echo "	</form>";
		echo "	</div>";
		echo "</div>";
	}
  
?>

	<div id='tabs_0'>
		<form name='CreateProfile' method='POST' action='CreateProfile.php'>
		<table width="100%">
			<tr><th width=\"200\">Name:</th> <td><input type='text' name='Name' ></td></tr>
			<tr><th>Date of Birth:</th> <td><input type='text' id="datepicker" name='DOB' > Day/Month/Year</td></tr>
			<tr><th>Gender:</th> <td>Gender: <input type='radio' name='Gender' value=1 checked> Male <input type='radio' name='Gender' value=2 > Female</td><tr>
			<tr><td colspanc=\"2\"><input type='submit'></td></tr>
		</table>
		</form>
	</div>
</div>

<script>
$(function() {
	$( "#tabs" ).tabs();
<?
	foreach($TOTS['DisplayProfile'] as $TOT => $value) {
?>
		$( "#tabs_<?echo $value["TOTID"]; ?>" ).tabs();
<?
	}
?>
$( "#datepicker" ).datepicker({
	  dateFormat: "dd/mm/yy",
      changeMonth: true,
      changeYear: true
    });
    });
</script>

</td></tr></table>
<iframe src="../footer.html" width="1000" height="75" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>
