<?
	session_start();
	if(empty($_SESSION['uid'])) {
		header('Location: ../login.php');
	}

	$_SESSION['RedirectURL'] = "profile/index.php";

	$Name = $_SESSION['TOTName'];
	$DOB = $_SESSION['DOB'];
	$Gender = $_SESSION['Gender'];
	$ImmunizationChecked = $_SESSION['ImmunizationChecked'];
	$BDReminderChecked = $_SESSION['BDReminderChecked'];
	$CheckUpsChecked = $_SESSION['CheckUpsChecked'];

	$http = "http://all4tot.org/systemAPI/action.php?action=AddProfile&userID=".$_SESSION['uid']."&Name=".$Name."&Gender=".$Gender."&DOB=".$DOB;
	$g = file_get_contents($http);
	$djson = json_decode($g,true);
	
	$TOTID=0;	
	foreach($djson['AddProfile'] as $TOT => $value) {
		$TOTID=$value["TOTID"];
	}
	
	$http = "http://all4tot.org/systemAPI/UpdateCalendar.php?access_token=".$_SESSION['cal_token']."&TOTID=".$TOTID."&ImmunizationChecked=".$ImmunizationChecked."&BDReminderChecked=".$BDReminderChecked."&CheckUpsChecked=".$CheckUpsChecked;
	//echo $http."<br>";
	$g = file_get_contents($http);
	$djson = json_decode($g,true);
	
	header('Location: index.php');
	exit;
?>