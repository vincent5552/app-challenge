<?
session_start();
$_SESSION['RedirectURL'] = "profile/index_old.php";
if(empty($_SESSION['uid'])) {
	header('Location: ../login.php');
	exit;
}
if(empty($_SESSION['mytab'])) {
	$_SESSION['mytab'] = 0;
}

?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8" />
  <title>ALL4TOT - Profile</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <link rel='stylesheet' href='style_w.css'>
  <script>
  <?
   if($_SESSION['Display_Error_Weather'])
	  echo "alert('We Currently Only Support US Cities, Please Re-Select. Thanks!!');";
  ?>
  </script>
	<script>
		function checkProfileForm(id) {
			//alert(id);
			//return false;
			return true;
		}
	</script>
  
<style type="text/css">
#wrapper {
	background-color: #FFFFFF;
}
#won	{
	font-size: 11px;
}
td	{
	font-size: 13px;
}
th {
	align: left;
	background-color: #81BEF7;
}
</style>
</head>
<body bgColor="#333333">

<center>
<iframe src="../homepage_header.html" width="1000" height="270" scrolling="no" frameBorder="0" style="height:270; width:1000px;margin: 0px 0px -5px 0px; " >
</iframe>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#FFFFFF" >
<tr><td height=20></td></tr>
<tr><td>
<div id="tabs">
  <ul>
<?
	
	$http = "http://all4tot.org/systemAPI/action.php?action=DisplayProfile&userID=".$_SESSION['uid'];
	$g = file_get_contents($http);
	$TOTS = json_decode($g,true);

	foreach($TOTS['DisplayProfile'] as $TOT => $value) {
		echo "<li><a href='#tabs_".$value["TOTID"]."'>".$value["Name"]."</a></li>";
	}
	echo "<li><a href='#tabs_0' alt='Add'>+Add</a></li>";
?>
  </ul>
<?
	$i = 0;
	$k = 0;
	foreach($TOTS['DisplayProfile'] as $TOT => $value) {
		//title
		echo "<div id='tabs_".$value["TOTID"]."'>";
		echo "	<table width=\"100%\">";
		echo 	"<tr><th width=\"200\">Name: </th><td align=\"left\">".$value["Name"]."</td></tr>";
		echo "<tr><th>Date of Birth: </th><td class='Birth' align=\"left\">";
		echo 		$value["DOB"];
		echo "	</td></tr>";
		echo 	"<tr><th>Gender: </th><td align=\"left\">";
		if($value["Gender"]==1)
			echo "Male";
		else
			echo "Female";
		echo "	</td></tr>";
		echo "	</table>";

		echo "	<ul>";
		echo "		<li><a href='#tabs_".$value["TOTID"]."_Profile'>Profile</a></li>";
		echo "		<li><a href='#tabs_".$value["TOTID"]."_Measurement'>Measurement</a></li>";
		echo "		<li><a href='#tabs_".$value["TOTID"]."_Subscribe'>Subscribe</a></li>";
		echo "	</ul>";
		//	profile div
		
		echo "		<script>";
		echo "			$(function() {";
		echo "				$( \"#DOB".$value["TOTID"]."\" ).datepicker({ dateFormat: \"mm-dd-yy\" });";
		echo "			});";
		echo "		</script>";
		
		echo "	<div id='tabs_".$value["TOTID"]."_Profile'>";
		echo "	<form id='UpdateForm_".$value["TOTID"]."_Profile' name='UpdateProfile' method='POST' action='UpdateProfile.php' >";
		echo "	<table width=\"100%\">";
		echo "		<tr><th width=\"200\">Name:</th> <td align=\"left\"><input type='text' name='Name' value='".$value["Name"]."'></td></tr>";
		echo "		<tr><th>Date of Birth:</th> <td align=\"left\"><input class='datepicker' type='text' name='DOB' id='DOB".$value["TOTID"]."' value='";
		//echo 			date('m-d-Y', strtotime($value["DOB"]));
		echo 				$value["DOB"];
		echo "			'> Month-Day-Year</td></tr>";
		if($value["Gender"]==1)
			echo "		<tr><th>Gender:</th> <td align=\"left\"><input type='radio' name='Gender' value=1 checked> Male <input type='radio' name='Gender' value=2 > Female</td></tr>";
		else
			echo "		<tr><th>Gender:</th> <td align=\"left\"><input type='radio' name='Gender' value=1 > Male <input type='radio' name='Gender' value=2 checked> Female</td></tr>";
		echo "		<tr><td colspanc=\"2\"><input type='hidden' name='TOTID' value='".$value["TOTID"]."'>";
		echo "		<input type='submit' class='update' value='Update' onClick='checkProfileForm(\'DOB".$value["TOTID"]."\'); UpdateForm_".$value["TOTID"]."_Profile.action=\"UpdateProfile.php\";'>";
		echo "		<input type='submit' value='Remove' onClick='UpdateForm_".$value["TOTID"]."_Profile.action=\"RemoveProfile.php\";'></td></tr>";
		echo "	</table>";
		echo "	</form>";
		echo "	</div>";
		//	measurement div
		echo "	<div id='tabs_".$value["TOTID"]."_Measurement'>";
		echo "		<p>Measurement: enter the head, height, weight and measurement date for ".$value["Name"]."</p>";
		//		measurement form
		echo "	<form id='UpdateForm_".$value["TOTID"]."_Measurement' name='UpdateMeasurement' method='POST' action='UpdateMeasurement.php'>";
		echo "	<table width=\"100%\">";
		echo "		<tr><th width=\"200\">Circumference:</th> <td align=\"left\"><input type='text' id='Head' name='Head' > <select name='mhead'><option value=1>cm</option><option value=2>inch</option></select></td></tr>";
		echo "		<tr><th>Height:</th> <td align=\"left\"><input type='text' id='Height' name='Height' > <select name='mheight'><option value=1>cm</option><option value=2>inch</option></select></td></tr>";
		echo "		<tr><th>Weight:</th> <td align=\"left\"><input type='text' id='Weight' name='Weight' > <select name='mweight'><option value=1>kg</option><option value=2>lb</option></select></td></tr>";
		echo "		<script>";
		echo "			$(function() {";
		echo "				$( \"#MeasurementDate".$value["TOTID"]."\" ).datepicker({ dateFormat: \"mm-dd-yy\" });";
		echo "			});";
		echo "		</script>";
		echo "		<tr><th>Measurement date:</th> <td align=\"left\"><input type='text' name='MeasurementDate' class='datepicker2' id='MeasurementDate".$value["TOTID"]."' > Month-Day-Year</td></tr>";
		echo "		<tr><td colspan=\"2\"><input type='hidden' name='TOTID' value='".$value["TOTID"]."'>";
		echo "		<input class='measure' type='submit' value='Submit' ></td></tr>";
		echo "	</table>";
		echo "	</form>";
		//		measurement list
		$http = "http://all4tot.org/systemAPI/action.php?action=DisplayGrowthChart&TOTID=".$value["TOTID"];
		$g = file_get_contents($http);
		$Measurements = json_decode($g,true);
		echo "	<table border=\"0\" width=\"100%\" >";
		echo "		<tr><th>Circumference (inch)</th><th>Height (inch)</th><th>Weight (lb)</th><th>Measurement Date</th></tr>";
		$i = 0;
		foreach($Measurements['DisplayGrowthChart'] as $Measure => $Mvalue) {
			$Mvalue["Head"] = $Mvalue["Head"] * 0.393701;
			$Mvalue["Height"] = $Mvalue["Height"] * 0.393701;
			$Mvalue["Weight"] = $Mvalue["Weight"] * 2.20462;
			$Mvalue["Head"] = round($Mvalue["Head"], 1);
			$Mvalue["Height"] = round($Mvalue["Height"], 1);
			$Mvalue["Weight"] = round($Mvalue["Weight"], 1);
			echo "<tr ";
			if($i%2==1)
				echo "bgcolor=\"#E0ECF8\"";
			echo ">";
			echo "	<td>".$Mvalue["Head"]."</td>";
			echo "	<td>".$Mvalue["Height"]."</td>";
			echo "	<td>".$Mvalue["Weight"]."</td>";
			echo "	<td>".$Mvalue["MeasurementDate"]."</td>";
			echo "</tr>";
			$i++;
		}
		echo "	</table>";
		echo "	</div>";
		//	subscribe
		echo "	<div id='tabs_".$value["TOTID"]."_Subscribe'>";
		echo "	<p>Subscribe: subscribe handy notifications for ".$value["Name"]."</p>";
		echo "	<form id='UpdateForm_".$value["TOTID"]."_Measurement' name='UpdateSubscribe' method='POST' action='UpdateSubscribe_old.php'>";
		echo "	<table width=\"100%\">";
		$http1 = "http://all4tot.org/systemAPI/action.php?action=ShowSubscription&TOTID=".$value["TOTID"];
		//echo $http;
		$g1 = file_get_contents($http1);
		$Show = json_decode($g1,true);
		foreach ($Show['ShowSubscription'] as $Show => $Svalue) {
		
		$http = "http://all4tot.org/systemAPI/action.php?action=GetMessageCategories";
		$g = file_get_contents($http);
		$MessageCategories = json_decode($g,true);
		
		echo "		<tr><th width=\"200\">Message Category:</th> <td align=\"left\">";
		$i=0;
		
		// echo "<pre>";
		// print_r($Svalue['MessageCategories']);
		// echo "</pre>";
		foreach($MessageCategories['GetMessageCategories'] as $MessageCategory => $MCvalue) {
			$flag = false;
			foreach($Svalue['MessageCategories'] as $key => $MMvalue) {
				if($MCvalue["MessageCategory"] == $MMvalue) {
					$flag = true;
					break;
				}
				else
					$flag = false;
			}
			if($flag == false)
				echo "		<input type='checkbox' name='MessageCategoryID[]' value=".$MCvalue["MessageCategoryID"].">".$MCvalue["MessageCategory"]." ";
			else
				echo "		<input type='checkbox' name='MessageCategoryID[]' value=".$MCvalue["MessageCategoryID"]."  checked>".$MCvalue["MessageCategory"]." ";
			$i++;
			if($i%4==0)
				echo "<br>";
			
		}
		}
		echo "		</td></tr>";
		
		$http = "http://all4tot.org/systemAPI/action.php?action=GetCarriers";
		$g = file_get_contents($http);
		$Carriers = json_decode($g,true);
		
		echo "		<tr><th>Carrier:</th> <td align=\"left\">";
		echo "			<select name=\"CarrierID\">";
		foreach($Carriers['GetCarriers'] as $Carrier => $Cvalue) {
			if($Cvalue["Name"] == $Svalue['Name'])
			echo "			<option value=\"".$Cvalue["CarrierID"]."\" selected>".$Cvalue["Name"]."</option>";
			else
			echo "			<option value=\"".$Cvalue["CarrierID"]."\">".$Cvalue["Name"]."</option>";
		}
		echo "			</select>";
		echo "		</td></tr>";
		echo "		<tr><th>PhoneNo:</th> <td align=\"left\"><input id='phone' type='text' name='PhoneNo' value='".$Svalue['PhoneNo']."' >(10 digits)</td></tr>";
		
		$http = "http://all4tot.org/systemAPI/action.php?action=GetIntervals";
		$g = file_get_contents($http);
		$Intervals = json_decode($g,true);
		
		echo "		<tr><th>Receiving Interval:</th> <td align=\"left\">";
		//$i=0;
		foreach($Intervals['GetIntervals'] as $Interval => $Ivalue) {
			echo "		<input type=\"radio\" name=\"FrequencyID\" value=\"".$Ivalue["FrequencyID"]."\" ";
			//if($i==0)
			if($Ivalue["Intervals"] == $Svalue['Intervals'] && $Ivalue["Type"] == $Svalue['Type'])
				echo " checked ";
			echo "	> Every ".$Ivalue["Intervals"]." ".$Ivalue["Type"];
			//$i++;
		}
		echo "		</td></tr>";
		//echo "		<tr><th>Immunizations:</th> <td align=\"left\"><input type='checkbox' name='Immunization' > Add Immunization notification for ".$value["Name"]." to your google calendar</td></tr>";
		echo "	<input type='hidden' name='SubscribeDate' >";
		echo "	<input type='hidden' name='mytabid' value='".$k."'>";
		//echo "		<tr><th>Subscribe date:</th> <td align=\"left\"><input type='text' name='SubscribeDate' > Month-Day-Year</td></tr>";
		echo "		<tr><td colspan=\"2\"><input type='hidden' name='TOTID' value='".$value["TOTID"]."'>";
		echo "		<input class='sms' type='submit' value='Update' ></td></tr>";
		echo "	</table>";
		echo "	</form>";
		
		echo "	</div>";
		echo "</div>";
		$k = $k + 1;
	}
  
?>

	<div id='tabs_0'>
		<form name='CreateProfile' method='POST' action='CreateProfile.php'>
		<table width="100%">
			<tr><th width=\"200\">Name:</th> <td><input id='cname' type='text' name='Name' ></td></tr>
			<script>
				$(function() {
					$( "#DOB" ).datepicker({ dateFormat: "mm-dd-yy" });
				});
			</script>
			<tr><th>Date of Birth:</th> <td><input type='text' class='datepicker1' name='DOB' id='DOB'> Month-Day-Year</td></tr>
			<tr><th>Gender:</th> <td>Gender: <input type='radio' name='Gender' value=1 checked> Male <input type='radio' name='Gender' value=2 > Female</td><tr>
			<tr><th>Immunizations:</th> <td align=\"left\"><input type='checkbox' name='Immunization' id='Imm' > Add vaccine notification to your google calendar</td></tr>
			<tr><td colspanc="2"><input id='create' type='submit' onClick='return checkProfileForm("DOB");' ></td></tr>
		</table>
		</form>
	</div>
</div>

<script>
	var current_index;
	//$("#tabs").tabs();
	
	// $('#tabs').click(function() {
		// current_index= $("#tabs").tabs('option','active');
		// alert(current_index);
	// });
<?
	foreach($TOTS['DisplayProfile'] as $TOT => $value) {
?>
		$( "#tabs_<?echo $value["TOTID"]; ?>" ).tabs();
<?
	}
?>
	$('#Imm').change(function(){
		if($("#Imm").is(':checked')) {
			alert("It will take about 2~3 minutes to create a new calendar in Google Calendar \nand to add all the vaccination notifications to calendar for you.");
		}
	});
	$('.update').click(function() {
		var errors;
		if($('.datepicker').val() == "") {
			errors = "Date of Birth Field is blank.\n";
			alert(errors);
			success = false;
			return false;
		}
		current_index= $("#tabs").tabs('option','active');
	});

	$('#create').click(function() { 
		var errors;
		if($('#cname').val() == "") {
			errors = "Name Field is blank.\n";
			alert(errors);
			success = false;
			return false;
		}
		if($('.datepicker1').val() == "") {
			errors = "Date of Birth Field is blank.\n";
			alert(errors);
			success = false;
			return false;
		}
	});
	
	$('.sms').click(function() {
		var errors;
		if($('#phone').val() != "") {
			var value = $('#phone').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			var intRegex = /^\d+$/;
			if(!intRegex.test(value)) {
				errors = "PhoneNo Field must be numeric.\n";
				alert(errors);
				success = false;
				return false;
			}
			if(value.length != 10) {
				errors = "PhoneNo Field must be 10 digits.\n";
				alert(errors);
				success = false;
				return false;
			}
		} else {
			errors = "PhoneNo Field is blank.\n";
			alert(errors);
			success = false;
			return false;
		}
		current_index= $("#tabs").tabs('option','active');
		//alert(current_index);
		$.ajax({
			type: 'POST', // type of request either Get or Post
			url: "UpdateSubscribe.php", // Url of the page where to post data and receive response 
			data: '{index, "John"}', // data to be post
			success: function(data){ alert(data); } //function to be called on successful reply from server
		});
	});

	$(".measure").click(function() {
		var errors;
		if($('#Head').val() != "") {
			var value = $('#Head').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			var intRegex = /^\d+$/;
			if(!intRegex.test(value)) {
				errors = "Circumference Field must be numeric.\n";
				alert(errors);
				success = false;
				return false;
			}
		} else {
			errors = "Circumference Field is blank.\n";
			alert(errors);
			success = false;
			return false;
		}
		
		if($('#Height').val() != "") {
			var value = $('#Height').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			var intRegex = /^\d+$/;
			if(!intRegex.test(value)) {
				errors = "Height Field must be numeric.\n";
				alert(errors);
				success = false;
				return false;
			}
		} else {
			errors = "Height Field is blank.\n";
			alert(errors);
			success = false;
			return false;
		}
		if($('#Weight').val() != "") {
			var value = $('#Weight').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			var intRegex = /^\d+$/;
			if(!intRegex.test(value)) {
				errors = "Weight Field must be numeric.\n";
				alert(errors);
				success = false;
				return false;
			}
		} else {
			errors = "Weight Field is blank.\n";
			alert(errors);
			success = false;
			return false;
		}
		if($('.datepicker2').val() == "") {
			errors = "Measurement Date Field is blank.\n";
			alert(errors);
			success = false;
			return false;
		}
		if($('.datepicker2').val() == "") {
			errors = "Measurement Date Field is blank.\n";
			alert(errors);
			success = false;
			return false;
		}
		
		//if(date("Y-m-d",strtotime($date))>date("Y-m-d",strtotime($date2))){   
		if($('.datepicker2').val() == $('.Birth').val()) {
			errors = "Measurement Date Field is invalid.\n";
			alert(errors);
			success = false;
			return false;
		}
		
		current_index= $("#tabs").tabs('option','active');
	});
	
	$("#tabs").tabs({ active: <?echo $_SESSION['mytab'];?> });
	<? 
	if(!empty($_SESSION['mytotid'])) {
	?>
		$( "#tabs_<?echo $_SESSION['mytotid']; ?>" ).tabs({ active: <?echo $_SESSION['mytabloc'];?> });
	<? 
	} 
	?>
</script>

</td></tr></table>
<iframe src="../footer.html" width="1000" height="75" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>
