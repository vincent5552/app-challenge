<?
session_start();
$_SESSION['RedirectURL'] = "protectMe/index.php";
if(empty($_SESSION['uid'])) {
	header('Location: ../login.php');
	exit;
}
if(empty($_SESSION['mytabp'])) {
	$_SESSION['mytabp'] = 0;
}
if(empty($_SESSION['county'])) {
	$your_key = '5baaa0b862440e65dded909b2b1169e0d546ec52c8c3629e6b2cd405c7f6266e';
	$url = "http://api.ipinfodb.com/v3/ip-city/?key=$your_key&format=json&ip=".$_SERVER['REMOTE_ADDR'];
	$d = file_get_contents($url);
	$data = json_decode($d , true);
	$http = "http://all4tot.org/systemAPI/Report/action.php?action=getCountybyZIP&zip=". $data['zipCode'];

	$g = file_get_contents($http);
	$geo = json_decode($g,true);
	
	$_SESSION['county'] = $geo['County&State'][0]['County'];
	$_SESSION['state'] = $geo['County&State'][0]['State'];
}

?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8" />
  <title>ALL4TOT - Profile</title>
  <link rel="shortcut icon" href="http://all4tot.org/web/favicon.ico" />
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <link rel='stylesheet' href='style_w.css'>
  <script>
  <?
   if($_SESSION['Display_Error_Weather'])
	  echo "alert('We Currently Only Support US Cities, Please Re-Select. Thanks!!');";
  ?>
  $(function() {
	var postData = {
		sid: []
	};
	$('#state').change(function() {
		Zip.value='';
		postData['sid']=$(this).val();
		$.ajax({url: 'getCounty.php',
		data: postData,
		success : function(data) {
			$("#county").html(data);
			}
		});
	});		
  });
  </script>
<style type="text/css">
#wrapper {
	background-color: #FFFFFF;
}
#won	{
	font-size: 11px;
}
td	{
	font-size: 13px;
}
th {
	align: left;
	background-color: #81BEF7;
}
</style>
</head>
<body bgColor="#333333">

<center>
<iframe src="../homepage_header.html" width="1000" height="270" scrolling="no" frameBorder="0" style="height:270; width:1000px;margin: 0px 0px -5px 0px; " >
</iframe>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#FFFFFF" >
<tr><td height=20></td></tr>
<tr><td><table cellspacing="5" cellpadding="5"><tr>
	<th>Location:</th>
	<td><?echo $_SESSION['county'].", ".$_SESSION['state']?></td>
</tr></table></td></tr>
<tr><td>
<div id="tabs">
  <ul>
	<li><a href='#tabs_0'>Around Me</a></li>
	<li><a href='#tabs_1'>Report</a></li>
	<li><a href='#tabs_2'>History</a></li>
  </ul>
  <div id="tabs_0">
	<p>Change Location</p>
	<?

		$http = "http://all4tot.org/systemAPI/Report/action.php?action=getStateList";
		$g = file_get_contents($http);
		$states = json_decode($g,true);

		$http = "http://all4tot.org/systemAPI/Report/action.php?action=getCounty&state=".$_SESSION['state'];
		$g = file_get_contents($http);
		$counties = json_decode($g,true);		

		echo "	<table width=\"100%\">";
		echo "	<form name='ChangeLocation' method='POST' action='changeLocation.php'>";
		echo "		<tr><th width=\"200\">Use current location?</th><td><input type='hidden' name='CurrentLocation' value='1' ><input type='submit' value='Use Current Location' ></tD></tr>";
		echo "	</form>";
		echo "		<tr><td width=\"200\"></td><td>OR</td></tr>";
		echo "	<form id='ChangeLocation' name='ChangeLocation' method='POST' action='changeLocation.php'>";
		echo "		<tr><th width=\"200\">Zip:</th> <td align=\"left\"><input type='text' id='Zip' name='Zip' ></td></tr>";
		echo "		<tr><td width=\"200\"></td><td>OR</td></tr>";
		echo "		<tr><th>State:</th> <td align=\"left\"><select id='state' name='state' >";
		$i=0;
		foreach($states['StateList'] as $state=>$Svalue) {
			if($i>0)
				echo "		<option value='".$Svalue['STATE']."'";
				if($_SESSION['state']==$Svalue['STATE']) {
					echo "		selected";
				}
				echo "		>".$Svalue['STATE']."</option>";
			$i++;
		}
		echo "		</select></td></tr>";
		echo "		<tr><th>County:</th> <td align=\"left\"><select id='county' name='county'>";
		foreach($counties[$_SESSION['state']] as $county=>$Cvalue) {
			echo "		<option value='".$Cvalue['COUNTY']."'"; 
			if($_SESSION['county']==$Cvalue['COUNTY']) {
				echo "		selected";
			}
			echo "		>".$Cvalue['COUNTY']."</option>";
				
		}
		echo "		</select></td></tr>";
		echo "		<tr><td colspanc=\"2\">";
		echo "		<input type='submit' value='Submit' ></td></tr>";
		echo "	</table>";
		echo "	</form>";

		$http = "http://all4tot.org/systemAPI/Report/action.php?action=getNotification&state=".$_SESSION['state']."&county=".urlencode($_SESSION['county'])."&range=30";
		$g = file_get_contents($http);
		$selfReports = json_decode($g,true);
		
		$diseases = array();
		$diseasesCnt = array();
		$i = 0;
		foreach($selfReports['NotificationList']['self report'] as $selfReport => $Svalue) {
			$diseases[$Svalue['Disease']] .= ";".$Svalue['County'].", ".$Svalue['State'];
			$diseasesCnt[$Svalue['Disease']] += intval($Svalue['Number']);
		}

		foreach($diseases as $key => $value) {
			$diseases[$key] = substr($value, 1);
		}
	?>		
	<h2>Disease around me</h2>
	<table border="0" width="100%">
		<tr><th align="left">Disease</th>
		<th align="left">Total Number</th>
		<th align="left">County Lists</th></tr>
	<?
		$i=0;
		foreach($diseases as $key => $value) {
			echo "<tr ";
			if($i%2==1)
				echo "bgcolor=\"#E0ECF8\"";
			echo ">";
			echo "	<td>".$key."</td>";
			echo "	<td>".$diseasesCnt[$key]."</td>";
			echo "	<td>".$diseases[$key]."</td>";
			echo "</tr>";
			$i++;
		}
	?>
	</table>
  </div>
  <?
	$http = "http://all4tot.org/systemAPI/Report/action.php?action=getDiseasesList&description=N"; 
	$g = file_get_contents($http);
	$diseases = json_decode($g,true);
  ?>
  <div id="tabs_1">
	<h2>Report known disease</h2>
	<table border="0" width="100%">
		<form name="SelfReport" id="SelfReport" action="SelfReport.php" method="POST">
		<tr><th align="left">Disease</th><td>
		<?
			$i=0;
			foreach($diseases['DiseaseList'] as $key=>$disease) {
				echo "<input type='radio' name='DiseaseID' value='".$disease['ID']."'";
				if($i==0)
					echo "	checked";
				echo "	>".$disease['DISEASE']."<br>";
				$i++;
			}
		?>
		</td></tr>
		<tr><td widht="200"></td><td><input type="submit" value="submit" ></td></tr>
		</form>
	</table>
  </div>
  <?
	$http = "http://all4tot.org/systemAPI/Report/action.php?action=getHistory&userid=".$_SESSION['uid']."&usergroupid=1"; 
	$g = file_get_contents($http);
	$historys = json_decode($g,true);
  ?>
  <div id="tabs_2">
	<h2>History of reported diseases</h2>
	<table border="0" width="100%">
		<tr><th>Disease</th><th>County</th><th>Created Time</th></tr>
		<?
			$i=0;
			foreach($historys[HistoryList] as $key=>$history) {
				echo "<tr ";
				if($i%2==1)
					echo "bgcolor=\"#E0ECF8\"";
				echo ">";
				echo "	<td>".$history['Disease']."</td>";
				echo "	<td>".$history['County'].", ".$history['State']."</td>";
				echo "	<td>".$history['CreateTime']."</td>";
				echo "</tr>";
				$i++;
			}
		?>
	</table>
  </div>
</div>

<script>
	//$( "#tabs" ).tabs();
	$("#tabs").tabs({ active: <?echo $_SESSION['mytabp'];?> });
</script>

</td></tr></table>
<iframe src="../footer.html" width="1000" height="75" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>
