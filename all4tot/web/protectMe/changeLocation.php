<?
	session_start();
	if(empty($_SESSION['uid'])) {
		header('Location: ../login.php');
	}
	$_SESSION['mytabp'] = 0;

	$_SESSION['RedirectURL'] = "protectMe/index.php";

	$Zip = $_POST['Zip'];
	$state = $_POST['state'];
	$county = $_POST['county'];

	if(strlen($Zip)==5) {
		$http = "http://all4tot.org/systemAPI/Report/action.php?action=getCountybyZIP&zip=". $Zip;
		$g = file_get_contents($http);
		$geo = json_decode($g,true);
	
		$_SESSION['county'] = $geo['County&State'][0]['County'];
		$_SESSION['state'] = $geo['County&State'][0]['State'];
	} else {
		$_SESSION['county'] = $county;
		$_SESSION['state'] = $state;
	}

	header('Location: index.php');
	exit;
?>