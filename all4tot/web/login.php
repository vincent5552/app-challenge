<?
session_start(); 
//print_r($_SESSION);
if(!empty($_SESSION['uid'])){
	$_SESSION['uid']=NULL;
	unset($_SESSION['access_token']);  
	header('Location: index.html');
	exit;
}
?>
<!DOCTYPE HTML>
<html>
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>Login</title>
</head>
<style type="text/css">
#wrapper {
	background-color: #FFFFFF;
}
#fb:hover {
    background:url(image/02_button_Facebook_d.png) no-repeat;
    cursor:pointer;
    border: none;
}
#gp:hover {
    background:url(image/02_button_Google_d.png) no-repeat;
    cursor:pointer;
    border: none;
}
p{	font-family:"Arial", Arial, serif;
	font-size:1.200em;
}

</style>
<body bgColor="#333333">

<center>
<iframe src="homepage_header.html" width="1000" height="270" scrolling="no" frameBorder="0" style="height:270; width:1000px;margin: 0px 0px -5px 0px; " >
</iframe>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#FFFFFF" >
<tr><td height=20></td></tr>
<tr><td>

<div align="center">
	<table border="0" width="700" height="300" id="table1" cellspacing="0" cellpadding="0" style="background-color:#FFFFFF;">
		<tr><td colspan="2" height="50"></td></tr>
		<tr><td valign="top" align="center" width="300" ><a href="GP_login.php"><img src="image/02_button_Google.png" alt="Login Google+" id="gp" style="border-style: none" width="150" height="189" /></a></td>
			<td valign="top" align="left">
				<article>
					<p>By logging in with your Google account, you understand and allow us to identify you by your Google ID.</p>
					<p>Accounts authentication and authorization is directly provided by Google, all requests for access and permission must be approved by you as the account holder.</p>
				</article>
		</td></tr>
	</table>	
</div>
</td></tr></table>
<iframe src="footer.html" width="1000" height="75" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>