<?
session_start();
$_SESSION['RedirectURL'] = "whattowear/index.php";

if( !isset($_SESSION['uid']) || trim($_SESSION['uid'])=="" ){
echo ("<SCRIPT LANGUAGE='JavaScript'>
    window.alert('Please Login With Your Google Account to Access This Page')
    window.location.href='../login.php';
    </SCRIPT>");

}else{
	$uid=$_SESSION['uid'];	
}

$counter=2;
$counter2=2;
$counter3=2;
$counter4=2;

$state_curren=$_SESSION['state_current'];
$city_current=$_SESSION['city_current'];

if($_SESSION['REMOTE_ADDR']==$_SERVER['REMOTE_ADDR']){

}else{
	$your_key = '5baaa0b862440e65dded909b2b1169e0d546ec52c8c3629e6b2cd405c7f6266e';
	$url = "http://api.ipinfodb.com/v3/ip-city/?key=$your_key&format=json&ip=".$_SERVER['REMOTE_ADDR'];
	$d = file_get_contents($url);
	$data = json_decode($d , true);
	$state_current=$data['regionName'];
	$city_current=$data['cityName'];
	$_SESSION['REMOTE_ADDR']=$_SERVER['REMOTE_ADDR'];
	$_SESSION['city_current']=$city_current;
	$_SESSION['state_current']=$state_current;
}
if($state_current=="")
	$state_current="Texas";
if($city_current=="")
	$city_current="Bryan";
if($city_current=="-")
	$city_current="College_Station";	
?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8" />
  <title>ALL4TOT - Weather</title>
  <link rel="shortcut icon" href="http://all4tot.org/web/favicon.ico" />
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <link rel='stylesheet' href='style_w.css'>
  <script>
  <?
   if($_SESSION['Display_Error_Weather'])
	  echo "alert('We Currently Only Support US Cities, Please Re-Select. Thanks!!');";
  ?>
  $(function() {
    $( "#tabs" ).tabs();
	function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
 
 
	$( "#tabs_1" ).tabs();
	function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
 
	$( "#tabs_2" ).tabs();
	function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
	
    $( "#city" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "http://ws.geonames.org/searchJSON",
          dataType: "jsonp",
          data: {
            featureClass: "P",
            style: "full",
            maxRows: 12,
            name_startsWith: request.term
          },
          success: function( data ) {
            response( $.map( data.geonames, function( item ) {
              return {
                label: item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName
                //value: item.name
              }
            }));
          }
        });
      },
      minLength: 2,
      select: function( event, ui ) {
        log( ui.item ?
          "Selected: " + ui.item.label :
          "Nothing selected, input was " + this.value);
      },
      open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });
  });
  </script>
<style type="text/css">
.header {
	font-size: 24px;
}
#tabs_1{
margin-left:auto;
margin-right:auto;
width:990px;
font-size: 15px;
}
<!--body {background-color:#202020;}-->


#tabs_1-3{
text-align:left;
}

td{
 text-align:center;
 vertical-align:middle;
}

</style>
</head>
<body bgColor="#333333">

<center>
<iframe src="../homepage_header.html" width="1000" height="270" scrolling="no" frameBorder="0" style="height:270; width:1000px;margin: 0px 0px -5px 0px; " >
</iframe>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#FFFFFF" >
<tr><td height=20></td></tr>
<tr><td>
 <div id="tabs_1">
  <ul>
    <li><a href="#tabs_1-1">Current Weather</a></li>
    <li><a href="#tabs_1-2">5 Days Weather Forecast</a></li>
    <li><a href="#tabs_1-3">Edit Location</a></li>
  </ul>
  <div id="tabs_1-1">
   
 <!--tab_1-1-->     
   <div id="tabs_2">
	<ul>
<?
require_once("db_conn.php");
//$uid='100';
$sql="select city from vivianpe_all4tot.WEATHERPROFILE where userID ='".$uid."'";


		$result_sql=mysql_query($sql);
		$row = mysql_fetch_assoc($result_sql);
		$city_db = split('@@', $row[city]);	
?>  
    <li><a href="#tabs_2-1">Current Location - <? echo  ucfirst(strtolower($city_current)).", ".ucfirst(strtolower($state_current));  ?></a></li>
<?

if(count($city_db)>1 || !trim($city_db[0])==''){
	foreach($city_db as $city_temp) {
	echo "<li><a href=\"#tabs_2-".$counter3."\">".$city_temp."</a></li>";
	$counter3++;
	}
}
?>	
	</ul>
  <div id="tabs_2-1">
	<?php
	$state =strtoupper($_GET["state"]);
	$city =$_GET["city"];
	
	
	$city=str_replace(" ", "_", $city_current);
	$city=str_replace(" ", "%20", $city_current);
	$state=str_replace(" ", "_", $state_current);
	$state=str_replace(" ", "%20", $state_current);
	
	
	
	
	
	?>
	
	<table id="ver-minimalist" summary="weather">
	<thead>
	  <tr>
			<th scope="col">Current Condition</th>
			<th scope="col">Current Temp</th>
            <th scope="col">Today High</th>
            <th scope="col">Today Low</th>
	  </tr>
	  </thead>
	  <tbody>
	  <?
	    
		$url="http://api.wunderground.com/api/3a2d617823075c31/conditions/q/".$state."/".$city.".json";
		$json_string = file_get_contents($url);
		$parsed_json = json_decode($json_string, true);
		
		$url1="http://api.wunderground.com/api/3a2d617823075c31/forecast/q/".$state."/".$city.".json";
		$json_string1 = file_get_contents($url1);
		$parsed_json1 = json_decode($json_string1, true);
		
	   echo "<tr>";
	   //echo "<td>".$parsed_json[current_observation][weather]."</td>";
	   echo "<td style=\"text-align: center;\" ><img src=\"". $parsed_json[current_observation][icon_url]."\" alt=\"\" style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][weather]."</span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\currenttemp.png\" alt=\"\" height=\"37\" width=\"20\"   style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][temperature_string]."</span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\hi.png\" alt=\"\" height=\"37\" width=\"20\"     style=\"vertical-align: middle;\"><span> ".$parsed_json1[forecast][simpleforecast][forecastday][0][high][fahrenheit]." F (". $parsed_json1[forecast][simpleforecast][forecastday][0][high][celsius]." C)"."</span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\low.png\" alt=\"\" height=\"37\" width=\"20\"     style=\"vertical-align: middle;\"><span> ".$parsed_json1[forecast][simpleforecast][forecastday][0][low][fahrenheit]." F (". $parsed_json1[forecast][simpleforecast][forecastday][0][low][celsius]." C)"."</span></td>";
	   //echo "<td>".$parsed_json1[forecast][simpleforecast][forecastday][0][high][fahrenheit]." F (". $parsed_json1[forecast][simpleforecast][forecastday][0][high][celsius]." C)"."</td>";
	   //echo "<td>".$parsed_json1[forecast][simpleforecast][forecastday][0][low][fahrenheit]." F (". $parsed_json1[forecast][simpleforecast][forecastday][0][low][celsius]." C)"."</td>";
	   echo "</tr>";   
	   
	  ?>
	   </tbody>
	</table>	
	
	<table id="ver-minimalist" summary="weather">
	<thead>
	  <tr>
			<th scope="col">Chance of Rain</th>
			<th scope="col">Wind</th>
            <th scope="col">UV Index</th>
            <!--<th scope="col">All4TOT Message</th>-->
			<th scope="col">Humidity</th>
			
	  </tr>
	  </thead>
	  <tbody>
	  <?
		
		
	   echo "<tr>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\lrain.png\" alt=\"\" height=\"37\" width=\"20\"   style=\"vertical-align: middle;\"><span> ".$parsed_json1[forecast][simpleforecast][forecastday][0][pop]."%</span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\wind.png\" alt=\"\" height=\"37\" width=\"20\"   style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][wind_dir]."," . $parsed_json[current_observation][wind_mph]." mph"."</span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\uv_high.png\" alt=\"\" height=\"37\" width=\"37\"   style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][UV]."<span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\humid.png\" alt=\"\" height=\"37\" width=\"37\"   style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][relative_humidity]."</span></td>";
	   
	   //echo "<td>".$parsed_json1[forecast][simpleforecast][forecastday][0][pop]."</td>";
	   //echo "<td>".$parsed_json[current_observation][wind_dir]."," . $parsed_json[current_observation][wind_mph]." mph"."</td>";
	   //echo "<td>".$parsed_json[current_observation][UV]."</td>";
	   
	   echo "</tr>";   
	   
	  ?>
	   </tbody>
	</table>
	
	
	
</div>
<?
if(count($city_db)>1 || !trim($city_db[0])==''){
	foreach($city_db as $city_temp) {
	   echo "<div id='tabs_2-".$counter4."'>";
	   $city_temp1 = split(',', $city_temp);	
	   $state=trim($city_temp1[1]);
	   $city=trim($city_temp1[0]);
	   $city=str_replace(" ", "_", $city);
	   $city=str_replace(" ", "%20", $city);
	   $state=str_replace(" ", "_", $state);
	   $state=str_replace(" ", "%20", $state);
?>   
		  <table id="ver-minimalist" summary="weather">
			<thead>
				<tr>
					<th scope="col">Current Condition</th>
					<th scope="col">Current Temp</th>
					<th scope="col">Today High</th>
					<th scope="col">Today Low</th>
				</tr>
			</thead>
			<tbody>
		  
<?
			$url="http://api.wunderground.com/api/3a2d617823075c31/conditions/q/".$state."/".$city.".json";
			$json_string = file_get_contents($url);
			$parsed_json = json_decode($json_string, true);
			
			$url1="http://api.wunderground.com/api/3a2d617823075c31/forecast/q/".$state."/".$city.".json";
			$json_string1 = file_get_contents($url1);
			$parsed_json1 = json_decode($json_string1, true);
			
			
			echo "<tr>";
			//echo "<td>".$parsed_json[current_observation][weather]."</td>";
			//echo "<td>".$parsed_json[current_observation][temperature_string]."</td>";
			//echo "<td>".$parsed_json1[forecast][simpleforecast][forecastday][0][high][fahrenheit]." F (". $parsed_json1[forecast][simpleforecast][forecastday][0][high][celsius]." C)"."</td>";
			//echo "<td>".$parsed_json1[forecast][simpleforecast][forecastday][0][low][fahrenheit]." F (". $parsed_json1[forecast][simpleforecast][forecastday][0][low][celsius]." C)"."</td>";
			
			echo "<td style=\"text-align: center;\" ><img src=\"". $parsed_json[current_observation][icon_url]."\" alt=\"\" style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][weather]."</span></td>";
			echo "<td style=\"text-align: center;\" ><img src=\".\image\currenttemp.png\" alt=\"\" height=\"37\" width=\"20\"   style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][temperature_string]."</span></td>";
			echo "<td style=\"text-align: center;\" ><img src=\".\image\hi.png\" alt=\"\" height=\"37\" width=\"20\"     style=\"vertical-align: middle;\"><span> ".$parsed_json1[forecast][simpleforecast][forecastday][0][high][fahrenheit]." F (". $parsed_json1[forecast][simpleforecast][forecastday][0][high][celsius]." C)"."</span></td>";
			echo "<td style=\"text-align: center;\" ><img src=\".\image\low.png\" alt=\"\" height=\"37\" width=\"20\"     style=\"vertical-align: middle;\"><span> ".$parsed_json1[forecast][simpleforecast][forecastday][0][low][fahrenheit]." F (". $parsed_json1[forecast][simpleforecast][forecastday][0][low][celsius]." C)"."</span></td>";
			
			echo "</tr>";  
?>
			</tbody>
		</table>	
		
		<table id="ver-minimalist" summary="weather">
	<thead>
	  <tr>
			<th scope="col">Chance of Rain</th>
			<th scope="col">Wind</th>
            <th scope="col">UV Index</th>
            <!--<th scope="col">All4TOT Message</th>-->
			<th scope="col">Humidity</th>
	  </tr>
	  </thead>
	  <tbody>
	  <?
		
		
	   echo "<tr>";
	   
	   //echo "<td>".$parsed_json1[forecast][simpleforecast][forecastday][0][pop]."</td>";
	   //echo "<td>".$parsed_json[current_observation][wind_dir]."," . $parsed_json[current_observation][wind_mph]." mph"."</td>";
	   //echo "<td>".$parsed_json[current_observation][UV]."</td>";
	   //echo "<td style=\"text-align: center;\" ><img src=\".\image\lrain1.png\" alt=\"\" height=\"37\" width=\"20\"   style=\"vertical-align: middle;\"><span> ".$parsed_json1[current_observation][relative_humidity]."%</span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\lrain.png\" alt=\"\" height=\"37\" width=\"20\"   style=\"vertical-align: middle;\"><span> ".$parsed_json1[forecast][simpleforecast][forecastday][0][pop]."%</span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\wind.png\" alt=\"\" height=\"37\" width=\"20\"   style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][wind_dir]."," . $parsed_json[current_observation][wind_mph]." mph"."</span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\uv.png\" alt=\"\" height=\"37\" width=\"40\"   style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][UV]."<span></td>";
	   echo "<td style=\"text-align: center;\" ><img src=\".\image\lrain1.png\" alt=\"\" height=\"37\" width=\"20\"   style=\"vertical-align: middle;\"><span> ".$parsed_json[current_observation][relative_humidity]."%</span></td>";
	   echo "</tr>";   
	   
	  ?>
	   </tbody>
	</table>
	
		
		
<?	





	  echo "</div>";  
	   $counter4++;
	}
}
?>
 </div> <!--end of tab_1-2-->
 <!--end of tab_1-1-->  
  </div>
  <div id="tabs_1-2">
   
   
      <div id="tabs">
	<ul>
<?
require_once("db_conn.php");
//$uid='100';
$sql="select city from vivianpe_all4tot.WEATHERPROFILE where userID ='".$uid."'";
		$result_sql=mysql_query($sql);
		$row = mysql_fetch_assoc($result_sql);
		$city_db = split('@@', $row[city]);	
?>  
    <li><a href="#tabs-1">Current Location - <? echo  ucfirst(strtolower($city_current)).", ".ucfirst(strtolower($state_current));  ?></a></li>
<?

if(count($city_db)>1 || !trim($city_db[0])==''){
	foreach($city_db as $city_temp) {
	echo "<li><a href=\"#tabs-".$counter."\">".$city_temp."</a></li>";
	$counter++;
	}
}
?>	
	</ul>
  <div id="tabs-1">
	<?php
	$state =strtoupper($_GET["state"]);
	$city =$_GET["city"];
	$city=str_replace(" ", "_", $city_current);
	$city=str_replace(" ", "%20", $city_current);
	$state=str_replace(" ", "_", $state_current);
	$state=str_replace(" ", "%20", $state_current);
	?>
	
	<table id="rounded-corner" summary="Weather">
	<thead>
	  <tr>
			<th scope="col" class="rounded-company">Date</th>
            <th scope="col" class="rounded-q1">Temp High</th>
            <th scope="col" class="rounded-q2">Temp Low</th>
            <th scope="col" class="rounded-q3">Condition</th>
			<th scope="col" class="rounded-q3">Icon</th>
            <th scope="col" class="rounded-q4">Chance of Rain</th>
	  </tr>
	  </thead>
	 
	  
	  <tbody>
	  <?
		$url="http://api.wunderground.com/api/3a2d617823075c31/forecast10day/q/".$state."/".$city.".json";
		$json_string = file_get_contents($url);
		$parsed_json = json_decode($json_string, true);
		
	   for($n=0;$n<10;$n++){
	   
		if($n%2==0){
			   echo "<tr >";
			   echo "<td>".$parsed_json[forecast][txt_forecast][forecastday][$n][title]."</td>";
			   echo "<td>".$parsed_json[forecast][simpleforecast][forecastday][$n][high][fahrenheit]."</td>";
			   echo "<td>".$parsed_json[forecast][simpleforecast][forecastday][$n][low][fahrenheit]."</td>";
			   echo "<td>".$parsed_json[forecast][simpleforecast][forecastday][$n][conditions]."</td>";
			   echo "<td>"."<img src='".$parsed_json[forecast][simpleforecast][forecastday][$n][icon_url]."' width='50' height='50' />";  
			   echo "<td>".$parsed_json[forecast][simpleforecast][forecastday][$n][pop]."</td>";
			   echo "</tr>";   
		}
	   }
	  ?>
	   </tbody>
	</table>
</div>
<?
if(count($city_db)>1 || !trim($city_db[0])==''){
	foreach($city_db as $city_temp) {
	   echo "<div id='tabs-".$counter2."'>";
	   $city_temp1 = split(',', $city_temp);	
	   $state=trim($city_temp1[1]);
	   $city=trim($city_temp1[0]);
	   $city=str_replace(" ", "_", $city);
	   $city=str_replace(" ", "%20", $city);
	   $state=str_replace(" ", "_", $state);
	   $state=str_replace(" ", "%20", $state);
?>   
		 <table id="rounded-corner" summary="Weather">
			<thead>
				<tr>
					<th scope="col" class="rounded-company">Date</th>
					<th scope="col" class="rounded-q1">Temp High</th>
					<th scope="col" class="rounded-q2">Temp Low</th>
					<th scope="col" class="rounded-q3">Condition</th>
					<th scope="col" class="rounded-q3">Icon</th>
					<th scope="col" class="rounded-q4">Chance of Rain</th>
				</tr>
			</thead>
			<tbody>
		  
<?
			$url="http://api.wunderground.com/api/3a2d617823075c31/forecast10day/q/".$state."/".$city.".json";
		   $json_string = file_get_contents($url);
			$parsed_json = json_decode($json_string, true);
		   for($n=0;$n<10;$n++){
			if($n%2==0){
				   echo "<tr >";
				   echo "<td>".$parsed_json[forecast][txt_forecast][forecastday][$n][title]."</td>";
				   echo "<td>".$parsed_json[forecast][simpleforecast][forecastday][$n][high][fahrenheit]."</td>";
				   echo "<td>".$parsed_json[forecast][simpleforecast][forecastday][$n][low][fahrenheit]."</td>";
				   echo "<td>".$parsed_json[forecast][simpleforecast][forecastday][$n][conditions]."</td>";
				   echo "<td>"."<img src='".$parsed_json[forecast][simpleforecast][forecastday][$n][icon_url]."' width='50' height='50' />";  
				   echo "<td>".$parsed_json[forecast][simpleforecast][forecastday][$n][pop]."</td>";
				   echo "</tr>";   
			}
		   }
?>
			</tbody>
		</table>	
<?	
	  echo "</div>";  
	   $counter2++;
	}
}
?>
 
	</div> <!--end of tab_1-2-->
   
   

	</div><!--end of tab_1-2-->
  
  <div id="tabs_1-3">
  
  
  <form name="form1" method="post" action="weather_process.php">
	 <h3>Remove Selected City</h3>
	 <?
	 $cb_count=1; 
	 if(count($city_db)>1 || !trim($city_db[0])==''){
		 foreach($city_db as $city_temp) {
		 echo "<p>";
		 echo $city_temp;
		 ?>
		 <input type="checkbox" name="checkbox-<?echo $cb_count?>" id="checkbox-<?echo $cb_count?> " value='<? echo $city_temp ?>'>
		 <label for="checkbox-<?echo $cb_count?>"></label>
		 <?
		 echo "</p>";
		 $cb_count++;
		 }
	 }
	 ?>
	  <h3>Add New City</h3>
      <p>
       <div class="ui-widget">
  <label for="city">Your city: </label>
  <input id="city" name="city" type="text" size="50"/>
</div> 
      </p>
      </p>
	 <p>
        <input type="submit" name="button" id="button" value="Submit">
      </p>
    </form>	
  
  
  
  </div>
  
</td></tr></table>
<iframe src="../footer.html" width="1000" height="75" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>


