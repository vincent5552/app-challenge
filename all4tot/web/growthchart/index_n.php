<?
@session_start();
//check for login !!
$_SESSION['RedirectURL']="growthchart/";
if( !isset($_SESSION['uid']) || trim($_SESSION['uid'])=="" ){

echo ("<SCRIPT LANGUAGE='JavaScript'>
    window.alert('Please Login With Your Google Account to Access This Page')
    window.location.href='../login.php';
    </SCRIPT>");

}else{
	$temp_userid=$_SESSION['uid'];	
}


//echo $temp_userid;


require_once("pre_process_text.php");

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>ALL4TOT - Weather beta Function</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  
  <script src="js/jquery.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" charset="utf-8" />
<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
  
  
  <script>
    $(function() {
		
    $( "#tabs" ).tabs();
	function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
 
 
	$( "#tabs_1" ).tabs();
	function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
 
 
		$( "#tabs_2" ).tabs();
	function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
 
 
  });
  </script>
  
  <script type="text/javascript" charset="utf-8">
  
  $(document).ready(function(){
    $("a[rel^='prettyPhoto']").prettyPhoto();
  });
</script>
<style type="text/css">
.header {
	font-size: 24px;
}
#tabs{margin-left:auto;
margin-right:auto;
width:990px;
font-size: 15px;
}

<?
		for($n=0;$n<count($_SESSION['tot_gender_array']);$n++){
		if($_SESSION['tot_gender_array'][$n]==1){
		?>
			#tabs_1-<?echo ($n+1)?>{
			background-color:#BCD0F7;
			font-size: 13px;
			}
			#tabs_2-<?echo ($n+1)?>{
			background-color:#BCD0F7;
			font-size: 13px;
			}
		<?
		}else{
		?>
		#tabs_1-<?echo ($n+1)?>{
			background-color:#FFE7DE;
			font-size: 13px;
		}
		#tabs_2-<?echo ($n+1)?>{
			background-color:#FFE7DE;
			font-size: 13px;
		}
		td.title{
			font-size: 17px;
		}
		<?
			}
		}
		?>
</style>
</head>
<body bgColor="#333333">

<center>
<iframe src="../homepage_header.html" width="1000" height="270" scrolling="no" frameBorder="0" style="height:270; width:1000px;margin: 0px 0px -5px 0px; " >
</iframe>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#FFFFFF" >
<tr><td height="20"></td></tr>
<tr><td>
 
 <div id="tabs">
  <ul>
    <li><a href="#tabs-1">Where I am now?</a></li>
	<li><a href="#tabs-2">My Growth Chart</a></li>
    <li><a href="#tabs-3">Enter Growth Data</a></li>
  </ul>
  <div id="tabs-1">
    
		<div id="tabs_1">
  <ul>
  
  <?   
  for($n=0;$n<count($_SESSION['tot_name_array']);$n++){
	echo "<li><a href=\"#tabs_1-".($n+1)."\">".$_SESSION['tot_name_array'][$n]." - ".$_SESSION['tot_dob_age'][$n]."</a></li>";
  }
  ?>
  
  </ul>
  
  
    
<!--APRIL-->

<?


for($nt=0;$nt<count($TID=$_SESSION['tot_id']);$nt++){

	echo "<div id=\"tabs_1-".($nt+1)."\">";


$TID=$_SESSION['tot_id'][$nt];
$ainw=$_SESSION['tot_age_week'][$nt];
$gender_temp=$_SESSION['tot_gender_array'][$nt];

$sql_a1="select A.Head, A.Height, A.Weight, A.MeasurementDate, B.DOB from vivianpe_all4tot.TB_GrowthChart A, vivianpe_all4tot.TB_TOTProfile B where A.TOTID='".$TID."' and A.TOTID=B.TOTID order by A.MeasurementDate DESC limit 3";	
$result_sql_a1=mysql_query($sql_a1);
$result_final_array=array();
	while ($row_a1 = mysql_fetch_assoc($result_sql_a1)) {
		$birthDate = explode("-", $row_a1['DOB']);
		$measureDate = explode("-", $row_a1['MeasurementDate']);
		$BOD_U=date('U', mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]) );
		$MD_U=date('U', mktime(0, 0, 0, $measureDate[1], $measureDate[2], $measureDate[0]) );
		$in_weeks=floor(($MD_U-$BOD_U)/(7*24*60*60));
		
		//find different in months
		$temp_date1=trim($birthDate[0])."-".trim($birthDate[1])."-".trim($birthDate[2]);
		$temp_date2=trim($measureDate[0])."-".trim($measureDate[1])."-".trim($measureDate[2]);
		
		$temp_age_1=diff_date($temp_date1, $temp_date2);
		
		//FOR HEAD PERCENTAGE
		
		$sql_a2="SELECT Per_3, Per_5, Per_10, Per_25, Per_50, Per_75, Per_90, Per_95, Per_97  FROM vivianpe_all4tot.HEAD where Gender='". $gender_temp."'and Age between '". ($temp_age_1.".0")."' and '".($temp_age_1.".9")."' limit 1"; //need to change
		$result_sql_a2=mysql_query($sql_a2);
		$row_a2 = mysql_fetch_assoc($result_sql_a2); //value form DB query
				
		$result_head_per="";
			if($row_a1['Head'] > $row_a2["Per_3"])
			$result_head_per='3%';
		if($row_a1['Head'] > $row_a2["Per_5"])
			$result_head_per='5%';
		if($row_a1['Head'] > $row_a2["Per_10"])
			$result_head_per='10%';
		$gap_a1=($row_a2["Per_25"]-$row_a2["Per_10"])/3;	
		if($row_a1['Head'] > $row_a2["Per_10"]+ ($gap_a1*1))
			$result_head_per='15%';
		if($row_a1['Head'] > $row_a2["Per_10"]+ ($gap_a1*2))
			$result_head_per='20%';
		if($row_a1['Head'] > $row_a2["Per_25"])
			$result_head_per='25%';
		$gap_a1=($row_a2["Per_50"]-$row_a2["Per_25"])/5;	
		if($row_a1['Head'] > $row_a2["Per_25"]+ ($gap_a1*1))
			$result_head_per='30%';		
		if($row_a1['Head'] > $row_a2["Per_25"]+ ($gap_a1*2))
			$result_head_per='35%';			
		if($row_a1['Head'] > $row_a2["Per_25"]+ ($gap_a1*3))
			$result_head_per='40%';				
		if($row_a1['Head'] > $row_a2["Per_25"]+ ($gap_a1*4))
			$result_head_per='45%';			
		if($row_a1['Head'] > $row_a2["Per_50"])
			$result_head_per='50%';
		$gap_a1=($row_a2["Per_75"]-$row_a2["Per_50"])/5;
		if($row_a1['Head'] > $row_a2["Per_50"]+ ($gap_a1*1))
			$result_head_per='55%';	
		if($row_a1['Head'] > $row_a2["Per_50"]+ ($gap_a1*2))
			$result_head_per='60%';		
		if($row_a1['Head'] > $row_a2["Per_50"]+ ($gap_a1*3))
			$result_head_per='65%';		
		if($row_a1['Head'] > $row_a2["Per_50"]+ ($gap_a1*4))
			$result_head_per='70%';		
		if($row_a1['Head'] > $row_a2["Per_75"])
			$result_head_per='75%';
		$gap_a1=($row_a2["Per_90"]-$row_a2["Per_75"])/3;	
		if($row_a1['Head'] > $row_a2["Per_75"]+ ($gap_a1*1))
			$result_head_per='80%';			
		if($row_a1['Head'] > $row_a2["Per_75"]+ ($gap_a1*2))
			$result_head_per='85%';			
		if($row_a1['Head'] > $row_a2["Per_90"])
			$result_head_per='90%';
		if($row_a1['Head'] > $row_a2["Per_95"])
			$result_head_per='95%';
		if($row_a1['Head'] > $row_a2["Per_97"])
			$result_head_per='97%';
			
		if($result_head_per=="")
		$result_head_per="N/A";
		
		$result_head_per.=" (".floor($row_a1['Head'])."CM)";
		
		//HEIGHT
		
		$sql_a2="SELECT Per_3, Per_5, Per_10, Per_25, Per_50, Per_75, Per_90, Per_95, Per_97  FROM vivianpe_all4tot.HEIGHT where Gender='". $gender_temp."'and Age between '". ($temp_age_1.".0")."' and '".($temp_age_1.".9")."' limit 1"; //need to change
		$result_sql_a2=mysql_query($sql_a2);
		$row_a2 = mysql_fetch_assoc($result_sql_a2); //value form DB query
				
		$result_height_per="";
			if($row_a1['Height'] > $row_a2["Per_3"])
			$result_height_per='3%';
		if($row_a1['Height'] > $row_a2["Per_5"])
			$result_height_per='5%';
		if($row_a1['Height'] > $row_a2["Per_10"])
			$result_height_per='10%';
		$gap_a1=($row_a2["Per_25"]-$row_a2["Per_10"])/3;	
		if($row_a1['Height'] > $row_a2["Per_10"]+ ($gap_a1*1))
			$result_height_per='15%';
		if($row_a1['Height'] > $row_a2["Per_10"]+ ($gap_a1*2))
			$result_height_per='20%';
		if($row_a1['Height'] > $row_a2["Per_25"])
			$result_height_per='25%';
		$gap_a1=($row_a2["Per_50"]-$row_a2["Per_25"])/5;	
		if($row_a1['Height'] > $row_a2["Per_25"]+ ($gap_a1*1))
			$result_height_per='30%';		
		if($row_a1['Height'] > $row_a2["Per_25"]+ ($gap_a1*2))
			$result_height_per='35%';			
		if($row_a1['Height'] > $row_a2["Per_25"]+ ($gap_a1*3))
			$result_height_per='40%';				
		if($row_a1['Height'] > $row_a2["Per_25"]+ ($gap_a1*4))
			$result_height_per='45%';			
		if($row_a1['Height'] > $row_a2["Per_50"])
			$result_height_per='50%';
		$gap_a1=($row_a2["Per_75"]-$row_a2["Per_50"])/5;
		if($row_a1['Height'] > $row_a2["Per_50"]+ ($gap_a1*1))
			$result_height_per='55%';	
		if($row_a1['Height'] > $row_a2["Per_50"]+ ($gap_a1*2))
			$result_height_per='60%';		
		if($row_a1['Height'] > $row_a2["Per_50"]+ ($gap_a1*3))
			$result_height_per='65%';		
		if($row_a1['Height'] > $row_a2["Per_50"]+ ($gap_a1*4))
			$result_height_per='70%';		
		if($row_a1['Height'] > $row_a2["Per_75"])
			$result_height_per='75%';
		$gap_a1=($row_a2["Per_90"]-$row_a2["Per_75"])/3;	
		if($row_a1['Height'] > $row_a2["Per_75"]+ ($gap_a1*1))
			$result_height_per='80%';			
		if($row_a1['Height'] > $row_a2["Per_75"]+ ($gap_a1*2))
			$result_height_per='85%';			
		if($row_a1['Height'] > $row_a2["Per_90"])
			$result_height_per='90%';
		if($row_a1['Height'] > $row_a2["Per_95"])
			$result_height_per='95%';
		if($row_a1['Height'] > $row_a2["Per_97"])
			$result_height_per='97%';
		
		if($result_height_per=="")
		$result_height_per="N/A";
		
		$result_height_per.=" (".floor($row_a1['Height'])."CM)";
		
		//WEIGHT
	
	
	$sql_a2="SELECT Per_3, Per_5, Per_10, Per_25, Per_50, Per_75, Per_90, Per_95, Per_97  FROM vivianpe_all4tot.WEIGHT where Gender='". $gender_temp."'and Age between '". ($temp_age_1.".0")."' and '".($temp_age_1.".9")."' limit 1"; //need to change
		$result_sql_a2=mysql_query($sql_a2);
		$row_a2 = mysql_fetch_assoc($result_sql_a2); //value form DB query
				
		$result_weight_per="";
		if($row_a1['Weight'] > $row_a2["Per_3"])
			$result_weight_per='3%';
		if($row_a1['Weight'] > $row_a2["Per_5"])
			$result_weight_per='5%';
		if($row_a1['Weight'] > $row_a2["Per_10"])
			$result_weight_per='10%';
		$gap_a1=($row_a2["Per_25"]-$row_a2["Per_10"])/3;	
		if($row_a1['Weight'] > $row_a2["Per_10"]+ ($gap_a1*1))
			$result_weight_per='15%';
		if($row_a1['Weight'] > $row_a2["Per_10"]+ ($gap_a1*2))
			$result_weight_per='20%';
		if($row_a1['Weight'] > $row_a2["Per_25"])
			$result_weight_per='25%';
		$gap_a1=($row_a2["Per_50"]-$row_a2["Per_25"])/5;	
		if($row_a1['Weight'] > $row_a2["Per_25"]+ ($gap_a1*1))
			$result_weight_per='30%';		
		if($row_a1['Weight'] > $row_a2["Per_25"]+ ($gap_a1*2))
			$result_weight_per='35%';			
		if($row_a1['Weight'] > $row_a2["Per_25"]+ ($gap_a1*3))
			$result_weight_per='40%';				
		if($row_a1['Weight'] > $row_a2["Per_25"]+ ($gap_a1*4))
			$result_weight_per='45%';			
		if($row_a1['Weight'] > $row_a2["Per_50"])
			$result_weight_per='50%';
		$gap_a1=($row_a2["Per_75"]-$row_a2["Per_50"])/5;
		if($row_a1['Weight'] > $row_a2["Per_50"]+ ($gap_a1*1))
			$result_weight_per='55%';	
		if($row_a1['Weight'] > $row_a2["Per_50"]+ ($gap_a1*2))
			$result_weight_per='60%';		
		if($row_a1['Weight'] > $row_a2["Per_50"]+ ($gap_a1*3))
			$result_weight_per='65%';		
		if($row_a1['Weight'] > $row_a2["Per_50"]+ ($gap_a1*4))
			$result_weight_per='70%';		
		if($row_a1['Weight'] > $row_a2["Per_75"])
			$result_weight_per='75%';
		$gap_a1=($row_a2["Per_90"]-$row_a2["Per_75"])/3;	
		if($row_a1['Weight'] > $row_a2["Per_75"]+ ($gap_a1*1))
			$result_weight_per='80%';			
		if($row_a1['Weight'] > $row_a2["Per_75"]+ ($gap_a1*2))
			$result_weight_per='85%';			
		if($row_a1['Weight'] > $row_a2["Per_90"])
			$result_weight_per='90%';
		if($row_a1['Weight'] > $row_a2["Per_95"])
			$result_weight_per='95%';
		if($row_a1['Weight'] > $row_a2["Per_97"])
			$result_weight_per='97%';
		
		if($result_weight_per=="")
		$result_weight_per="N/A";
		
		$result_weight_per.=" (".floor($row_a1['Weight'])."KG)";
	
	
	
	array_push($result_final_array, $row_a1['MeasurementDate']);
	array_push($result_final_array, $result_head_per);
	array_push($result_final_array, $result_height_per);
	array_push($result_final_array, $result_weight_per);
		
	}

	//print_r($result_final_array);


?>



<table width="720" height="280" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="250">
	<?
	if($gender_temp==1){
	$temp_image="boy1.png";
	}else{
	$temp_image="girl1.png";
	}
	
	?>
	<img src=".\images\<?echo $temp_image ?>" width="300" height="280" style="border-style: none" alt="Toddler Icon" />
	</td>
    <td width="877" colspan="2">   
    <table width="560" height="280" border="0" cellspacing="0" cellpadding="0">
      <tr height="65">
        <td width="200">&nbsp;</td>
		
<?

for($e=0;$e< (count($result_final_array)/4);$e++){
	echo "<td width=\"130\">". $result_final_array[($e*4)] ."</td>";
}
?>		
        </tr>
      <tr height="73">
        <td class="title" align="left">Head circumference</td>	
<?
for($e=0;$e<(count($result_final_array)/4);$e++){

	if($result_final_array[((($e+1)*4)+1)] > $result_final_array[(($e*4)+1)]){
		$arrow="down";
	}
	else{
		if(($e+1) ==(count($result_final_array)/4) ){
		$arrow="tigh";
		}else{
		$arrow="up";
		}
	}
	
	echo "<td>".$result_final_array[(($e*4)+1)]."&nbsp<img src='.\images\\".$arrow.".png' width=\"15\" height=\"15\" alt=\"Chart Data\" /></td>";
}
?>
        </tr>		
      <tr height="55">
        <td class="title" align="left">Height</td>

<?
for($e=0;$e< (count($result_final_array)/4);$e++){
if($result_final_array[((($e+1)*4)+2)] > $result_final_array[(($e*4)+2)]){
		$arrow="down";
	}
	else{
		
		if(($e+1) ==(count($result_final_array)/4) ){
		$arrow="tigh";
		}else{
		$arrow="up";
		}
	}
	echo "<td>".$result_final_array[(($e*4)+2)]."&nbsp<img src='.\images\\".$arrow.".png' width=\"15\" height=\"15\" alt=\"Chart Data\" /></td>";
}
?>		
        </tr>
      <tr height="90">
        <td class="title" align="left">Weight</td>
		
<?
for($e=0;$e< (count($result_final_array)/4);$e++){
	if($result_final_array[((($e+1)*4)+3)] > $result_final_array[(($e*4)+3)]){
		$arrow="down";
	}
	else{
		
		if(($e+1) ==(count($result_final_array)/4) ){
		$arrow="tigh";
		}else{
		$arrow="up";
		}
	}
	echo "<td>".$result_final_array[(($e*4)+3)]."&nbsp<img src='.\images\\".$arrow.".png' width=\"15\" height=\"15\" alt=\"Chart Data\" /></td>";
}
?>        
      </tr>
    </table>

   </td>
  </tr>
</table>
<p></p>
<p></p>
<p></p>
<h6>Please double check on growth measurement if no data or N/A displayed here.</h4>
<!--END APRIL-->
 </div>
  
<?

} //end of main loop

?>  
  
  
 
</div>

  </div>
    
   <div id="tabs-2">
    
   <div id="tabs_2">
  <ul>
   
	
<?   
  for($n=0;$n<count($_SESSION['tot_name_array']);$n++){
	echo "<li><a href=\"#tabs_2-".($n+1)."\">".$_SESSION['tot_name_array'][$n]." - ".$_SESSION['tot_dob_age'][$n]."</a></li>";
  }
?>
  </ul>
  
  
  <?


for($nt=0;$nt<count($TID=$_SESSION['tot_id']);$nt++){

	echo "<div id=\"tabs_2-".($nt+1)."\">";
	//$TID=$_SESSION['tot_id'][$nt];
	//$ainw=$_SESSION['tot_age_week'][$nt];
	$gender_temp=$_SESSION['tot_gender_array'][$nt];
  
 ?> 
  
    
<!--APRIL-->

<table width="938" height="232" border="0">
  <tr>
    <td width="254">
	<?
	if($gender_temp==1){
	$temp_image="boy2.png";
	}else{
	$temp_image="girl2.png";
	}
	
	?>
	<img src=".\images\<?echo $temp_image ?>" width="226" height="280" alt="Toddler Icon" /></td>
	
    <td width="559" colspan="2">
    <table width="614" height="245" border="0">
      <tr >
        <td width="152" height="54">Head Circumference</td>
        <td width="152">Height</td>
        <td width="147">Weight</td>
        <td width="147">Length-for-Weight</td>
        </tr>
      <tr>
        <td><a href=".\chartset\<?echo $_SESSION['Chart1'][$nt];?>" rel="prettyPhoto" title="Head circumference">    <img src=".\chartset\<?echo $_SESSION['Chart1'][$nt];?>" width="152" height="196" alt="Head Circumference"/></a></td>
		<td><a href=".\chartset\<?echo $_SESSION['Chart2'][$nt];?>" rel="prettyPhoto" title="Height">    <img src=".\chartset\<?echo $_SESSION['Chart2'][$nt];?>" width="152" height="196" alt="Height"/></a></td>
		<td><a href=".\chartset\<?echo $_SESSION['Chart3'][$nt];?>" rel="prettyPhoto" title="Weight">    <img src=".\chartset\<?echo $_SESSION['Chart3'][$nt];?>" width="152" height="196" alt="Weight"/></a></td>
		<td><a href=".\chartset\<?echo $_SESSION['Chart4'][$nt];?>" rel="prettyPhoto" title="Length-for-Weight">    <img src=".\chartset\<?echo $_SESSION['Chart3'][$nt];?>" width="152" height="196" alt="Length-for-Weight"/></a></td>		
        </tr>
      </table>

   </td>
  </tr>
</table>


<!--end of APRIL-->

	
	

  </div>

 <?
 
 }
 ?>
 
 
  
 </div> <!--end of inner tab 2-->
	</div> <!--end of outer tabs-2 -->

  
  
  
  
  
  
    <div id="tabs-3">
	<p>Measurement data can be add or edit in the <a href="..\profile\">profile function</a></p>
	</div>
</div>

</td></tr></table>

<iframe src="../footer.html" width="1000" height="75" scrolling="no" frameBorder="0" >
</iframe>

</center>
</body>

</html>