<?php
//pre-process
require_once("db_conn.php");
@session_start();
//$temp_userid='G-100';
$tot_name=array();
$tot_id=array();
$tot_gender=array();
$tot_dob=array();
$tot_age=array();
$tot_age_week=array();
$tot_age_month=array();
$chart1=array();
$chart2=array();
$chart3=array();
$chart4=array();



//FUNCTION
function diff_date($start_date, $end_date) {
  list($start_year, $start_month, $start_day) = explode('-', $start_date);
  list($end_year, $end_month, $end_day) = explode('-', $end_date);

  $month_diff = $end_month - $start_month;
  $day_diff   = $end_day - $start_day;

  $months = $month_diff + ($end_year - $start_year) * 12;
  $days = 0;

  if ($day_diff > 0) {
    $days = $day_diff;
  }
  else if ($day_diff < 0) {
    $days = $end_day;
    $months--;

    if ($month_diff > 0) {
      $days += 30 - $start_day;

      if (in_array($start_month, array(1, 3, 5, 7, 8, 10, 12))) {
        $days++;
      }
      else if ($start_month == 2) {
        if (($start_year % 4 == 0 && $start_year % 100 != 0) || $start_year % 400 == 0) {
          $days--;
        }
        else {
          $days -= 2;
        }
      }

      if (in_array($end_month - 1, array(1, 3, 5, 7, 8, 10, 12))) {
        $days++;
      }
      else if ($end_month - 1 == 2) {
        if (($end_year % 4 == 0 && $end_year % 100 != 0) || $end_year % 400 == 0) {
          $days--;
        }
        else {
          $days -= 2;
        }
      }
    }
  }

  return $months;
}
//END OF FUNCTION
$sql="select TOTID, Name, Gender, DOB, Chart1, Chart2, Chart3, Chart4 from vivianpe_all4tot.TB_TOTProfile where userID='".$temp_userid."'";


	$result_sql=mysql_query($sql);
	
	while ($row = mysql_fetch_assoc($result_sql)) {

	array_push($tot_name, $row['Name']);
	array_push($tot_gender, $row['Gender']);
	array_push($tot_dob, $row['DOB']);
	array_push($tot_id, $row['TOTID']);
	
	
	
	if($row['Chart1']==""){
		if($row['Gender']=='1'){
			array_push($chart1, "no_data_3.png");
		}else{
			array_push($chart1, "no_data_4.png");
		}
	}else{
		array_push($chart1, $row['Chart1']);
	}
	
	
	if($row['Chart2']==""){
		if($row['Gender']=='1'){
			array_push($chart2, "no_data_1.png");
		}else{
			array_push($chart2, "no_data_2.png");
		}
	}else{
		array_push($chart2, $row['Chart2']);
	}
	
	if($row['Chart3']==""){
		if($row['Gender']=='1'){
			array_push($chart3, "no_data_1.png");
		}else{
			array_push($chart3, "no_data_2.png");
		}
	}else{
		array_push($chart3, $row['Chart3']);
	}
	
		if($row['Chart4']==""){
		if($row['Gender']=='1'){
			array_push($chart4, "no_data_3.png");
		}else{
			array_push($chart4, "no_data_3.png");
		}
	}else{
		array_push($chart4, $row['Chart4']);
	}
	
	
	//array_push($chart2, $row['Chart2']);
	//array_push($chart3, $row['Chart3']);
	//array_push($chart4, $row['Chart4']);
	
	$birthDate = explode("-", $row['DOB']);
	$temp_date=trim($birthDate[0])."-".trim($birthDate[1])."-".trim($birthDate[2]);
	$today = new DateTime();
	$today_temp=date_format($today, 'Y-m-d');
	$temp_age=diff_date($temp_date, $today_temp);
	
	$temp_age_s= floor($temp_age/12)."Y".($temp_age-((floor($temp_age/12))*12))."M";   
	$temp_age_sw=(floor($temp_age/12))*52+(($temp_age-((floor($temp_age/12))*12))*4);
	
	array_push($tot_age, $temp_age_s);
	array_push($tot_age_month, $temp_age);
	
	array_push($tot_age_week, $temp_age_sw);
	
	
	
	}

$_SESSION['tot_name_array']=$tot_name;
$_SESSION['tot_gender_array']=$tot_gender;
$_SESSION['tot_dob_array']=$tot_dob;
$_SESSION['tot_dob_age']=$tot_age;
$_SESSION['tot_age_week']=$tot_age_week;
$_SESSION['tot_age_month']=$tot_age_month;
$_SESSION['tot_id']=$tot_id;




$_SESSION['Chart1']=$chart1;
$_SESSION['Chart2']=$chart2;
$_SESSION['Chart3']=$chart3;
$_SESSION['Chart4']=$chart4;

	
?>