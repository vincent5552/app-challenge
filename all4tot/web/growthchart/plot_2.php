<?
//FOR LENGTH AND WEIGHT
session_start();
require_once("pre_process_text.php");
$TID='92';
//$TID=$_SESSION['current_TOTID'];


function lookfor_mp($n){
	$file = fopen("G_WEIGHT_MONTH.txt", "r") or exit("Unable to open file!");
	$month_in_chart_pos=array();
	while(!feof($file)){
	  $line_of_text = fgets($file);
	  $data = explode(",", $line_of_text);
	  array_push($month_in_chart_pos, trim($data[1]));
	 }
	 if($n%3==0){
		$correct_pos=$month_in_chart_pos[$n/3]; 
	 }else{
		$low=floor($n/3);
		$high=ceil($n/3);
		$low_p=$month_in_chart_pos[$low];
		$high_p=$month_in_chart_pos[$high];
		$pos_gap=($high_p-$low_p)/3;
		$correct_pos=$low_p+($pos_gap*($n%3));		
	 }
	 
	 return floor($correct_pos);
}

function lookfor_wp($n){
	$file = fopen("G_WEIGHT_KG.txt", "r") or exit("Unable to open file!");
	$weight_in_chart_pos=array();
	while(!feof($file)){
	  $line_of_text = fgets($file);
	  $data = explode(",", $line_of_text);
	  array_push($weight_in_chart_pos, trim($data[1]));
	 }
	 if(is_int($n)){
		$correct_pos=$weight_in_chart_pos[$n-2];
	 }else{
		$low=floor($n);
		$high=ceil($n);
		$low_p=$weight_in_chart_pos[$low-2];
		$high_p=$weight_in_chart_pos[$high-2];
		$pos_gap=($high_p-$low_p)/100;
		
		$fraction = ($n - floor($n))*100; 
		$correct_pos=$low_p+($pos_gap*$fraction);
	 }
	 return floor($correct_pos);
}

//NEW
function lookfor_lp($n){

	$file = fopen("G_LENGTH_CM.txt", "r") or exit("Unable to open file!");
	$weight_in_chart_pos=array();
	while(!feof($file)){
	  $line_of_text = fgets($file);
	  $data = explode(",", $line_of_text);
	  array_push($weight_in_chart_pos, trim($data[1]));
	 }
	
	 if($n=="45"){
		$correct_pos=$weight_in_chart_pos[0];
	 }
	 if($n>45 && $n<105){
	 $low_p=$weight_in_chart_pos[0];
	 $high_p=$weight_in_chart_pos[12];
	 $pos_gap=($high_p-$low_p)/6000;
	 $fraction = ($n - 45)*100; 
	 $correct_pos=$low_p+($pos_gap*$fraction);
	 }
	 return ($correct_pos);
}

function lookfor_headp($n){
	$file = fopen("B_HEAD_CM_.HW.txt", "r") or exit("Unable to open file!");
	$weight_in_chart_pos=array();
	while(!feof($file)){
	  $line_of_text = fgets($file);
	  $data = explode(",", $line_of_text);
	  array_push($weight_in_chart_pos, trim($data[1]));
	 }
	 if(is_int($n)){
		$correct_pos=$weight_in_chart_pos[$n-32];
	 }else{
		$low=floor($n);
		$high=ceil($n);
		$low_p=$weight_in_chart_pos[$low-32];
		$high_p=$weight_in_chart_pos[$high-32];
		$pos_gap=($high_p-$low_p)/100;
		
		$fraction = ($n - floor($n))*100; 
		$correct_pos=$low_p+($pos_gap*$fraction);
	 }
	 return floor($correct_pos);
}

function lookfor_head_weightp_y($n){
	$file = fopen("B_WEIGHT_KG_.HW.txt", "r") or exit("Unable to open file!");
	$weight_in_chart_pos=array();
	while(!feof($file)){
	  $line_of_text = fgets($file);
	  $data = explode(",", $line_of_text);
	  array_push($weight_in_chart_pos, trim($data[1]));
	 }
	 if(is_int($n)){
		$correct_pos=$weight_in_chart_pos[$n-2];
	 }else{
		$low=floor($n);
		$high=ceil($n);
		$low_p=$weight_in_chart_pos[$low-2];
		$high_p=$weight_in_chart_pos[$high-2];
		$pos_gap=($high_p-$low_p)/100;
		
		$fraction = ($n - floor($n))*100; 
		$correct_pos=$low_p+($pos_gap*$fraction);
	 }
	 return floor($correct_pos);
}


function lookfor_head_weightp_x($n){

	$file = fopen("B_WEIGHT_KG_.HW.txt", "r") or exit("Unable to open file!");
	$weight_in_chart_pos=array();
	while(!feof($file)){
	  $line_of_text = fgets($file);
	  $data = explode(",", $line_of_text);
	  array_push($weight_in_chart_pos, trim($data[1]));
	 }
	 
	 $correct_pos=305+($n-45)*17.75;
	 
	 
	 return floor($correct_pos);
}



$draw_loc_x=array();
$draw_loc_y=array();
$draw_loc_y_2=array();

$draw_loc_y_head=array();
$draw_loc_y_head_weight=array();
$draw_loc_y_head_weight_x=array();
$kids_gender="";
$kids_name="";

$sql_a1="select A.Head, A.Height, A.Weight, A.MeasurementDate, B.DOB, B.Gender, B.Name from vivianpe_all4tot.TB_GrowthChart A, vivianpe_all4tot.TB_TOTProfile B where A.TOTID='".$TID."' and A.TOTID=B.TOTID order by A.MeasurementDate";	

//echo $sql_a1;
//die("_");

$result_sql_a1=mysql_query($sql_a1);
$result_final_array=array();

while ($row_a1 = mysql_fetch_assoc($result_sql_a1)) {
		
	$kids_gender=$row_a1["Gender"];
	$kids_name=	$row_a1["Name"];
		
	$birthDate = explode("-", $row_a1['DOB']);
	$temp_date=trim($birthDate[0])."-".trim($birthDate[1])."-".trim($birthDate[2]);
	$measureDate = explode("-", $row_a1['MeasurementDate']);
	$temp_date_measure=trim($measureDate[0])."-".trim($measureDate[1])."-".trim($measureDate[2]);
	$temp_age=diff_date($temp_date, $temp_date_measure);
	
	//print_r($row_a1);
	//echo "<br>";
	
	array_push($draw_loc_x, lookfor_mp($temp_age));
	array_push($draw_loc_y, lookfor_wp($row_a1["Weight"]));
	array_push($draw_loc_y_2, lookfor_lp($row_a1["Height"]));
	array_push($draw_loc_y_head, lookfor_headp($row_a1["Head"]));
	array_push($draw_loc_y_head_weight, lookfor_head_weightp_y($row_a1["Weight"]));
	array_push($draw_loc_y_head_weight_x, lookfor_head_weightp_x($row_a1["Height"]));
	
}


// print_r($draw_loc_x);
// echo "<br>";
// print_r($draw_loc_y);
// echo "<br>";
// print_r($draw_loc_y_2);
// echo "<br>";
// print_r($draw_loc_y_head);
// echo "<br>";
// print_r($draw_loc_y_head_weight);
// echo "<br>";
// print_r($draw_loc_y_head_weight_x);



if($kids_gender=="1"){
	$filename='cj41l017.png';
	$filename1='cj41l019.png';
}else{
	$filename='cj41l018.png';
	$filename1='cj41l020.png';
}

$image = imagecreatefrompng($filename);
$image1 = imagecreatefrompng($filename1);

$Dpink = imagecolorallocate($image, 242, 82, 165);
$Dpink = imagecolorallocate($image1, 242, 82, 165);
$red = imagecolorallocate($image, 255, 0, 0);
$red = imagecolorallocate($image1, 255, 0, 0);
$blue = imagecolorallocate($image, 0, 0, 255);
$blue = imagecolorallocate($image1, 0, 0, 255);


// draw the white ellipse
for($n=0;$n<count($draw_loc_x);$n++){
	imagefilledellipse($image, $draw_loc_x[$n], $draw_loc_y[$n], 20, 20, $red);
	imagefilledellipse($image, $draw_loc_x[$n], $draw_loc_y_2[$n], 20, 20, $blue);	
	imagefilledellipse($image1, $draw_loc_x[$n], $draw_loc_y_head[$n], 20, 20, $red);
	imagefilledellipse($image1, $draw_loc_y_head_weight_x[$n], $draw_loc_y_head_weight[$n], 20, 20, $blue);
	
	if($n>0){
		imagesetthickness($image, 7);
		imagesetthickness($image1, 7);
		imageline($image, $draw_loc_x[$n], $draw_loc_y[$n], $draw_loc_x[$n-1], $draw_loc_y[$n-1], $red);
		imageline($image, $draw_loc_x[$n], $draw_loc_y_2[$n], $draw_loc_x[$n-1], $draw_loc_y_2[$n-1], $blue);	
		imageline($image1, $draw_loc_x[$n], $draw_loc_y_head[$n], $draw_loc_x[$n-1], $draw_loc_y_head[$n-1], $red);
		imageline($image1, $draw_loc_y_head_weight_x[$n], $draw_loc_y_head_weight[$n], $draw_loc_y_head_weight_x[$n-1], $draw_loc_y_head_weight[$n-1], $blue);	
	}
}

$text = $kids_name;
$font = 'arial.ttf';
//NAME
imagettftext($image, 40, 0, 1200, 110, $Dpink, $font, $text);
imagettftext($image1, 40, 0, 1200, 110, $Dpink, $font, $text);

$percent=1;
list($width, $height) = getimagesize($filename);
$new_width = $width * $percent;
$new_height = $height * $percent;

list($width1, $height1) = getimagesize($filename1);
$new_width1 = $width1 * $percent;
$new_height1 = $height1 * $percent;

$image_p = imagecreatetruecolor($new_width, $new_height);
$image_p1 = imagecreatetruecolor($new_width1, $new_height1);

//TN
$image_TN = imagecreatetruecolor($new_width1*0.2, $new_height1*0.2);
$image_TN1 = imagecreatetruecolor($new_width1*0.2, $new_height1*0.2);


imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
imagecopyresampled($image_p1, $image1, 0, 0, 0, 0, $new_width1, $new_height1, $width1, $height1);

imagecopyresampled($image_TN, $image, 0, 0, 0, 0, $new_width1*0.2, $new_height1*0.2, $width, $height);
imagecopyresampled($image_TN1, $image1, 0, 0, 0, 0, $new_width1*0.2, $new_height1*0.2, $width1, $height1);

$temp_filename_2=$kids_name."_c2_".md5(microtime()).".png";
$temp_filename_1=$kids_name."_c1_".md5(microtime()).".png";

//CH3=CH2

// Output
imagepng($image_p,  "./chartset/".$temp_filename_2, 5);
imagepng($image_p1, "./chartset/".$temp_filename_1, 5);

imagepng($image_TN, 'LEN&WEIGHT_TN.png', 5);
imagepng($image_TN1, 'HEAD&WEIGHT&HEIGHT_TN.png', 5);

//update database
$sql_update="UPDATE vivianpe_all4tot.TB_TOTProfile SET  Chart1 = '". $temp_filename_1."', Chart2 = '".$temp_filename_2."', Chart3 = '".$temp_filename_2."' ,Chart4 = '".$temp_filename_1."' Where TOTID='".$TID."'";
$result_sql_update=mysql_query($sql_update);

//echo "<script type=\"text/javascript\">";
//echo "window.location = \"http://all4tot.org/web/".$_SESSION['growthChartRedirectURL']."\"";
//echo "</script>";

?>