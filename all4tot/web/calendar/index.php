<?
session_start();
$_SESSION['RedirectURL'] = "calendar/index.php";
if(empty($_SESSION['uid'])) {
	header('Location: ../login.php');
	exit;
}

?>
<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8" />
  <title>ALL4TOT - Profile</title>
  <link rel="shortcut icon" href="http://all4tot.org/web/favicon.ico" />
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css" />
  <link rel='stylesheet' href='style_w.css'>
  <script>
  <?
   if($_SESSION['Display_Error_Weather'])
	  echo "alert('We Currently Only Support US Cities, Please Re-Select. Thanks!!');";
  ?>
  </script>
<style type="text/css">
#wrapper {
	background-color: #FFFFFF;
}
#won	{
	font-size: 11px;
}
td	{
	font-size: 13px;
}
th {
	align: left;
	background-color: #81BEF7;
}
</style>
</head>
<body bgColor="#333333">

<center>
<iframe src="../homepage_header.html" width="1000" height="270" scrolling="no" frameBorder="0" style="height:270; width:1000px;margin: 0px 0px -5px 0px; " >
</iframe>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#FFFFFF" >
<tr><td height=20></td></tr>
<tr><td>
<div id="tabs">
  <ul>
<?
	
	$http = "http://all4tot.org/systemAPI/action.php?action=DisplayProfile&userID=".$_SESSION['uid'];
	$g = file_get_contents($http);
	$TOTS = json_decode($g,true);

	foreach($TOTS['DisplayProfile'] as $TOT => $value) {
		echo "<li><a href='#tabs_".$value["TOTID"]."'>".$value["Name"]."</a></li>";
	}
?>
  </ul>
<?
	$i = 0;
	foreach($TOTS['DisplayProfile'] as $TOT => $value) {
		//title
		echo "<div id='tabs_".$value["TOTID"]."'>";
		echo "	<table width=\"100%\">";
		echo 	"<tr><th width=\"200\">Name: </th><td align=\"left\">".$value["Name"]."</td></tr>";
		echo 	"<tr><th>Date of Birth: </th><td align=\"left\">".$value["DOB"]."</td></tr>";
		echo 	"<tr><th>Gender: </th><td align=\"left\">";
		if($value["Gender"]==1)
			echo "Male";
		else
			echo "Female";
		echo "	</td></tr>";
		echo "	</table>";
		echo "<br>";

		$http="http://all4tot.org/systemAPI/action.php?action=ShowVaccine&TOTID=".$value["TOTID"];
		$g = file_get_contents($http);
		$Vaccines = json_decode($g,true);
		//$diseases=array();

		echo "	<table width=\"100%\">";
		echo 	"	<tr><th width=\"200\">Disease</th><th width=\"200\">Vaccine</th><th width=\"200\">Begin Date</th><th width=\"200\">End Date</th><th width=\"200\">Remark</th></tr>";
		$i=0;
		foreach($Vaccines['ShowVaccine'] as $key => $Vaccine) {
			echo "<tr ";
			if($i%2==1)
				echo "bgcolor=\"#E0ECF8\"";
			echo ">";
			echo "	<td>".$Vaccine["Disease"]."</td>";
			echo "	<td>".$Vaccine["Vaccine"]."</td>";
			echo "	<td>".$Vaccine["StartDate"]."</td>";
			echo "	<td>".$Vaccine["EndDate"]."</td>";
			echo "	<td>".$Vaccine["Remark"]."</td>";
			echo "</tr>";
			$i++;
		}
		echo "</table>";
		
		echo "<br>";
		
		$http="http://all4tot.org/systemAPI/action.php?action=ShowOtherReminders&TOTID=".$value["TOTID"];
		$g = file_get_contents($http);
		$Vaccines = json_decode($g,true);
		//$diseases=array();

		echo "	<table width=\"100%\">";
		echo 	"	<tr><th width=\"200\">Category</th><th width=\"200\">Description</th><th width=\"200\">Date</th></tr>";
		$i=0;
		foreach($Vaccines['ShowOtherReminders'] as $key => $Vaccine) {
			echo "<tr ";
			if($i%2==1)
				echo "bgcolor=\"#E0ECF8\"";
			echo ">";
			echo "	<td>".$Vaccine["Category"]."</td>";
			echo "	<td>".$Vaccine["Description"]."</td>";
			echo "	<td>".$Vaccine["StartDate"]."</td>";
			echo "</tr>";
			$i++;
		}
		echo "</table>";
		
		echo "</div>";
	}
  
?>


</div>

<script>
	$( "#tabs" ).tabs();
</script>

</td></tr></table>
<iframe src="../footer.html" width="1000" height="75" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>
