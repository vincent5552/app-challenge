<?php
require_once("../db_conn.php");

$sql = "SELECT T.Name, Email, PhoneNO, MessageCategoryID, (DATEDIFF(NOW(), T.DOB) DIV 7) As Week, ";
$sql .= "CASE ";
$sql .= "WHEN F.FrequencyTypeID=1 THEN (DATEDIFF(NOW(), T.DOB) DIV 7) - (DATEDIFF(NOW(), NOW() - INTERVAL Intervals MONTH) DIV 7) ";
$sql .= "WHEN F.FrequencyTypeID=2 THEN (DATEDIFF(NOW(), T.DOB) DIV 7) - Intervals ";
$sql .= "END AS BeginWeek ";
$sql .= "FROM ";
$sql .= "TB_Subscribe S INNER JOIN TB_Frequency F ON S.FrequencyID = F.FrequencyID ";
$sql .= "INNER JOIN TB_TOTProfile T ON T.TOTID = S.TOTID ";
$sql .= "INNER JOIN TB_Carrier C ON S.CarrierID = C.CarrierID ";
$sql .= "WHERE S.Active = 1 AND (";
$sql .= "(F.FrequencyTypeID=1 AND DAYOFMONTH(NOW()) = 1 AND (MONTH(NOW()) - MONTH(T.DOB)) % Intervals = 0 ) OR "; // first day of the month
$sql .= "(F.FrequencyTypeID=2 AND DATEDIFF(NOW(), T.DOB) % (Intervals * 7) = 0 )";
$sql .= ")";

//echo $sql."<br>";


$result_sql=mysql_query($sql);

$prevType = "";
while ($row = mysql_fetch_assoc($result_sql)) {
	$msgarray = array();
	$temp = array();
	$sql = "SELECT TXT4TotsMessage, MessageType FROM TB_MessageCategory M INNER JOIN TXT4Tots T ON M.MessageCategory=T.MessageType WHERE MessageCategoryID IN (".trim($row["MessageCategoryID"]).") AND Age_In_Week > ".trim($row["BeginWeek"])." AND Age_In_Week <= ".trim($row["Week"])." Order By T.MessageType";
	$messages_sql = mysql_query($sql);
	while($msgrow = mysql_fetch_assoc($messages_sql)) {
		if(trim($msgrow["MessageType"])!=$prevType) {
			if(sizeof($temp)>0) {
				$msgarray[$prevType] = $temp;
				echo $prevType."<br>";
			}
			$temp = array();
		}
		array_push($temp, trim($msgrow["TXT4TotsMessage"]));
		$prevType = trim($msgrow["MessageType"]);
	}
	if(sizeof(temp)>0) {
		$msgarray[$prevType] = $temp;
		echo $prevType."<br>";
	}
	
	$to = trim($row["PhoneNO"]).trim($row["Email"]);
	echo $to."<br>";
	$subject = "Message from TXT4Tots";
	$headers = 'From:  text@all4tot.org' . "\r\n" .
    'Reply-To:  text@all4tot.org' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
	
	foreach($msgarray as $messages) {
		$body = "Dear ".trim($row["Name"])."\n\n";
		foreach($messages as $message) {
			$body .= $message."\n\n";
		}
		echo $body."<br>";
		if(mail($to, $subject, $body, $headers)) {
			echo "good<br>";
		} else {
			echo "bad<br>";
		}
	}
	
	
}



?>