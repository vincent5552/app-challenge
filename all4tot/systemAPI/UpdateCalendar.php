<?php
session_start();
require_once("../db_conn.php");
require_once 'Zend/Loader.php';

Zend_Loader::loadClass('Zend_Gdata');
Zend_Loader::loadClass('Zend_Gdata_AuthSub');
Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
Zend_Loader::loadClass('Zend_Gdata_HttpClient');
Zend_Loader::loadClass('Zend_Gdata_Calendar');

	$TOTID =$_GET["TOTID"];
	$ImmunizationChecked=$_GET["ImmunizationChecked"];
	$BDReminderChecked=$_GET["BDReminderChecked"];
	$CheckUpsChecked=$_GET["CheckUpsChecked"];
	$access_token =$_GET["access_token"];

	$client = Zend_Gdata_AuthSub::getHttpClient($access_token); 
	// Create a Gdata object using the authenticated Http Client
	$gdataCal = new Zend_Gdata_Calendar($client);	
	//create calendar
	$calFeed = $gdataCal->getCalendarListFeed();
	$noAppCal = true;
	//Loop through calendars and check name which is ->title->text
	foreach ($calFeed as $calendar) {
		if($calendar->title->text == "All4Tot") {
			$noAppCal = false;
		}
	}
	if($noAppCal) {
		$appCal = $gdataCal -> newListEntry();
		$appCal->title= $gdataCal->newTitle("All4Tot"); 

		$own_cal = "http://www.google.com/calendar/feeds/default/owncalendars/full";
		$gdataCal->insertEvent($appCal, $own_cal);
	}

	$calFeed = $gdataCal->getCalendarListFeed();
	foreach ($calFeed as $calendar) {
		if($calendar->title->text == "All4Tot")
			$appCalUrl = $calendar->content->src;
	}
	
	$sql = "SELECT DOB, Name FROM vivianpe_all4tot.TB_TOTProfile WHERE TOTID=".$TOTID;
	$result_sql=mysql_query($sql);
	$result_json=array();
	if ($row = mysql_fetch_assoc($result_sql)) {
		$DOB = trim($row["DOB"]);
		$Name = trim($row["Name"]);
	} else {
		$temp = array();
		$temp["result"]="TOTID does not exist";
		$result_json=array();
		array_push($result_json, $temp);
		$result_json=array("Calendar"=>$result_json);
		echo json_encode($result_json_out);
		exit;
	}	

	//Vaccine
	if($ImmunizationChecked==1) {
		$imm_results=array();
		$sql = "SELECT Disease, Vaccine, DATE_ADD('".$DOB."', INTERVAL StartMonth Month) As StartDate, DATE_ADD('".$DOB."', INTERVAL EndMonth Month) As EndDate, Remark FROM vivianpe_all4tot.TB_Immunization ORDER BY DATE_ADD('".$DOB."', INTERVAL StartMonth Month)";
		$result_sql=mysql_query($sql);
		while ($row = mysql_fetch_assoc($result_sql)) {
			$temp=array();
			$temp["Disease"]=trim($row["Disease"]);
			$temp["Vaccine"]=trim($row["Vaccine"]);
			$temp["StartDate"]=trim($row["StartDate"]);
			if($DOB==trim($row["EndDate"]))
				$temp["EndDate"]="";
			else
				$temp["EndDate"]=trim($row["EndDate"]);
			$temp["Remark"]=trim($row["Remark"]);
			array_push($imm_results, $temp);
		}

		$imm_results=array_values($imm_results);

		foreach($imm_results as $key=>$result) {
			$temp=array();
		
			$event = $gdataCal->newEventEntry();
			
			$event->title = $gdataCal->newTitle("Vaccination notification for ".$Name);
			//$event->where = array($gdataCal->newWhere("Mountain View, California"));
			//$event->content =
			//$gdataCal->newContent(" This is my awesome event. RSVP required.");
			$content = $Name." is required to take ".$result["Vaccine"]." for protecting from ".$result["Disease"];
			if(strlen($result["EndDate"])==0)
				$content .= ". Suggestion time for taking is on ".$result["StartDate"].". ";
			else
				$content .= ". Suggestion time for taking is between ".$result["StartDate"]." and ".$result["EndDate"].". ";
			$content .= "Please consult your doctor for more information. ";
			if(strlen($result["Remark"])>0)
				$content .= "Remark: ".$result["Remark"]." .";
				
			$content .= "Reference: CDC's immunization schedules. For more informaiton, please visit http://www.cdc.gov/vaccines/schedules/index.html";

			$event->content =
				$gdataCal->newContent($content);
				
	 
			// Set the date using RFC 3339 format.
			$startDate = $result["StartDate"];
			$startTime = "09:00";
			$endDate = $result["StartDate"];
			$endTime = "16:00";
			$tzOffset = "-06";
	 
			$when = $gdataCal->newWhen();
			//$when->startTime = "{$startDate}T{$startTime}:00.000{$tzOffset}:00";
			//$when->endTime = "{$endDate}T{$endTime}:00.000{$tzOffset}:00";
			$when->startTime = "{$startDate}";
			$when->endTime = "{$endDate}";
			$event->when = array($when);
			
			$newEvent = $gdataCal->insertEvent($event, $appCalUrl);
			
			$temp["content"]=$content;
			array_push($result_json, $temp);
		}
	}
	
	if($CheckUpsChecked==1) {
		$chk_results=array();
		$sql = "SELECT Description, Category, DATE_ADD('".$DOB."', INTERVAL ElapsedTime Month) As StartDate FROM vivianpe_all4tot.TB_OtherReminders ";
		$sql .= "WHERE Category='Recommend Check ups' ";
		$sql .= "ORDER BY ElapsedTime";
		$result_sql=mysql_query($sql);
		while ($row = mysql_fetch_assoc($result_sql)) {
			$temp=array();
			$temp["Description"]=trim($row["Description"]);
			$temp["Category"]=trim($row["Category"]);
			$temp["StartDate"]=trim($row["StartDate"]);
			array_push($chk_results, $temp);
		}

		$chk_results=array_values($chk_results);

		foreach($chk_results as $key=>$result) {
			$temp=array();
		
			$event = $gdataCal->newEventEntry();
			
			$event->title = $gdataCal->newTitle($result["Category"]." for ".$Name);
			$content = $Name." is required to take ".$result["Description"].". ";
			$content .= ". Suggestion time for taking is on ".$result["StartDate"].". ";
			$content .= "Please consult your doctor for more information. ";
			$event->content =
				$gdataCal->newContent($content);
	 
			// Set the date using RFC 3339 format.
			$startDate = $result["StartDate"];
			$endDate = $result["StartDate"];
	 
			$when = $gdataCal->newWhen();
			$when->startTime = "{$startDate}";
			$when->endTime = "{$endDate}";
			$event->when = array($when);
			
			$newEvent = $gdataCal->insertEvent($event, $appCalUrl);
			
			$temp["content"]=$content;
			array_push($result_json, $temp);
		}
	}
	if($BDReminderChecked==1) {
		$chk_results=array();
		$sql = "SELECT Description, Category, DATE_ADD('".$DOB."', INTERVAL ElapsedTime Month) As StartDate FROM vivianpe_all4tot.TB_OtherReminders ";
		$sql .= "WHERE Category='Birthday Reminder' ";
		$sql .= "ORDER BY ElapsedTime";
		$result_sql=mysql_query($sql);
		while ($row = mysql_fetch_assoc($result_sql)) {
			$temp=array();
			$temp["Description"]=trim($row["Description"]);
			$temp["Category"]=trim($row["Category"]);
			$temp["StartDate"]=trim($row["StartDate"]);
			array_push($chk_results, $temp);
		}

		$chk_results=array_values($chk_results);

		foreach($chk_results as $key=>$result) {
			$temp=array();
		
			$event = $gdataCal->newEventEntry();
			
			$event->title = $gdataCal->newTitle($result["Category"]." for ".$Name);
			$content = $Name."'s ".$result["Description"]." is coming. ";
			$content .= " It is on ".$result["StartDate"].". ";
			$event->content =
				$gdataCal->newContent($content);
	 
			// Set the date using RFC 3339 format.
			$startDate = $result["StartDate"];
			$endDate = $result["StartDate"];
	 
			$when = $gdataCal->newWhen();
			$when->startTime = "{$startDate}";
			$when->endTime = "{$endDate}";
			$event->when = array($when);
			
			$newEvent = $gdataCal->insertEvent($event, $appCalUrl);
			
			$temp["content"]=$content;
			array_push($result_json, $temp);
		}
	}
	
	
	$result_json=array_values($result_json);
	$result_json_out=array("Calendar"=>$result_json);
	echo json_encode($result_json_out);

?>