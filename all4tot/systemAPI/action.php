<?php
require_once("../db_conn.php");

//Create a profile for a given userID: userID => google id, Name => TOT's name, Gender => TOT's Gender, DOB => Date of Birth
if($_GET["action"]=="AddProfile") {
	$userID =$_GET["userID"];
	$Name =$_GET["Name"];
	$Gender = $_GET["Gender"];
	$DOB = $_GET["DOB"];
	
	$sql = "INSERT INTO vivianpe_all4tot.TB_TOTProfile (userID, Name, Gender, DOB, Active) VALUES (";
	$sql .= "'".$userID."',";
	$sql .= "'".$Name."',";
	$sql .= $Gender.",";
	$sql .= "STR_TO_DATE('".$DOB."','%m-%d-%Y'),";
	$sql .= "1)";
	
	//echo $sql."<br>";
	
	//$con=mysqli_connect($dbhost, $dbuser, $dbpasswd, $db) or die("Unable to connect to SQL server for insertion");
	//if (!mysqli_query($con,$sql)) {
	if (!mysql_query($sql)) {
		die('Error: ' . mysqli_error());
	} else {
		$temp = array();
		//$temp["result"]="1 record added";
		$temp["TOTID"]=mysql_insert_id();
		$result_json=array();
		array_push($result_json, $temp);
		$result_json=array("AddProfile"=>$result_json);
		echo json_encode($result_json);
	}
	//echo "1 record added";	
	mysqli_close($con);
	
//Update a profile for a given TOTID: userID => google id, Name => TOT's name, Gender => TOT's Gender, DOB => Date of Birth, TOTID
} else if($_GET["action"]=="UpdateProfile") {
	$userID =$_GET["userID"];
	$Name =$_GET["Name"];
	$Gender = $_GET["Gender"];
	$DOB = $_GET["DOB"];
	$TOTID = $_GET["TOTID"];
	
	$sql = "UPDATE vivianpe_all4tot.TB_TOTProfile SET ";
	$sql .= "userID = '".$userID."',";
	$sql .= "Name = '".$Name."',";
	$sql .= "Gender = ".$Gender.",";
	$sql .= "DOB = STR_TO_DATE('".$DOB."','%m-%d-%Y'),";
	$sql .= "Active = 1";
	$sql .= " WHERE TOTID =".$TOTID;
	
	//echo $sql."<br>";
	
	$con=mysqli_connect($dbhost, $dbuser, $dbpasswd, $db) or die("Unable to connect to SQL server for insertion");
	// Check connection
	if (!mysqli_query($con,$sql)) {
		die('Error: ' . mysqli_error());
	} else {
		$temp = array();
		$temp["result"]="1 record updated";
		$result_json=array();
		array_push($result_json, $temp);
		$result_json=array("UpdateProfile"=>$result_json);
		echo json_encode($result_json);
	}
	//echo "1 record updated";	
	mysqli_close($con);	

//Delete a profile for a given TOTID
} else if($_GET["action"]=="RemoveProfile") {
	$TOTID = $_GET["TOTID"];
	
	$sql = "DELETE FROM vivianpe_all4tot.TB_TOTProfile WHERE TOTID=".$TOTID;

	//echo $sql."<br>";
	
	$con=mysqli_connect($dbhost, $dbuser, $dbpasswd, $db) or die("Unable to connect to SQL server for insertion");
	// Check connection
	if (!mysqli_query($con,$sql)) {
		die('Error: ' . mysqli_error());
	} else {
		$temp = array();
		$temp["result"]="1 record deleted";
		$result_json=array();
		array_push($result_json, $temp);
		$result_json=array("RemoveProfile"=>$result_json);
		echo json_encode($result_json);
	}
	//echo "1 record delete";	
	mysqli_close($con);
	
//Display TOT's profiles for a given user: userID => google id
} else if($_GET["action"]=="DisplayProfile") {
	$userID =$_GET["userID"];
	$sql = "SELECT TOTID, Name, Gender, DATE_FORMAT(DOB, '%m-%d-%Y') as DOB FROM vivianpe_all4tot.TB_TOTProfile WHERE userID='".$userID."' AND Active = 1";
	$result_sql=mysql_query($sql);
	$result_json=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		$temp=array();
		$temp["TOTID"]=trim($row["TOTID"]);
		$temp["Name"]=trim($row["Name"]);
		$temp["Gender"]=trim($row["Gender"]);
		$temp["DOB"]=trim($row["DOB"]);
		array_push($result_json, $temp);
	}
	$result_json=array_values($result_json);		
	$result_json_out=array("DisplayProfile"=>$result_json);		
	echo json_encode($result_json_out);
// delete growth chart record
} else if($_GET["action"]=="RemoveGrowthChart") {
	$TOTID =$_GET["TOTID"];
	$MeasurementDate = $_GET["MeasurementDate"];
	
	$sql = "DELETE FROM vivianpe_all4tot.TB_GrowthChart WHERE MeasurementDate = STR_TO_DATE('".$MeasurementDate."','%m-%d-%Y') AND TOTID=".$TOTID;
	$con=mysqli_connect($dbhost, $dbuser, $dbpasswd, $db) or die("Unable to connect to SQL server for insertion");
	// Check connection
	if (!mysqli_query($con,$sql)) {
		die('Error: ' . mysqli_error());
	} else {
		$temp = array();
		$temp["result"]="1 record deleted";
		$result_json=array();
		array_push($result_json, $temp);
		$result_json=array("RemoveGrowthChart"=>$result_json);
		echo json_encode($result_json);
	}
	mysqli_close($con);

// insert or update growth chart for a given TOTID: TOTID, Height (cm), Weight (lb), Head (cm) and MeasurementDate
} else if($_GET["action"]=="UpdateGrowthChart") {
	$TOTID =$_GET["TOTID"];
	$Height =$_GET["Height"];
	$Weight = $_GET["Weight"];
	$Head = $_GET["Head"];
	$MeasurementDate = $_GET["MeasurementDate"];
	
	$sql = "SELECT * FROM vivianpe_all4tot.TB_GrowthChart WHERE MeasurementDate = STR_TO_DATE('".$MeasurementDate."','%m-%d-%Y') AND TOTID=".$TOTID;
	//echo $sql."<br>";
	$result_sql=mysql_query($sql);
	if (!$row = mysql_fetch_assoc($result_sql)) {
		$sql = "INSERT INTO vivianpe_all4tot.TB_GrowthChart (TOTID, Height, Weight, Head, MeasurementDate, Active) VALUES (";
		$sql .= $TOTID.",";
		$sql .= "'".$Height."',";
		$sql .= "'".$Weight."',";
		$sql .= "'".$Head."',";
		$sql .= "STR_TO_DATE('".$MeasurementDate."','%m-%d-%Y'),";
		$sql .= "1)";
	} else {
		$sql = "UPDATE vivianpe_all4tot.TB_GrowthChart SET ";
		$sql .= "Height = '".$Height."',";
		$sql .= "Weight = '".$Weight."',";
		$sql .= "Head = '".$Head."',";
		$sql .= "Active = 1";
		$sql .= " WHERE MeasurementDate = STR_TO_DATE('".$MeasurementDate."','%m-%d-%Y') AND TOTID=".$TOTID;
	}
	
	//echo $sql."<br>";
	
	$con=mysqli_connect($dbhost, $dbuser, $dbpasswd, $db) or die("Unable to connect to SQL server for insertion");
	// Check connection
	if (!mysqli_query($con,$sql)) {
		die('Error: ' . mysqli_error());
	} else {
		$sql = "UPDATE vivianpe_all4tot.TB_TOTProfile SET Plot=1 WHERE TOTID=".$TOTID;
		if (!mysqli_query($con,$sql)) {
			die('Error: ' . mysqli_error());
		} else {
			$temp = array();
			$temp["result"]="1 record changed"; //inserted or updated
			$result_json=array();
			array_push($result_json, $temp);
			$result_json=array("UpdateGrowthChart"=>$result_json);
			echo json_encode($result_json);
		}
	}
	//echo "1 record changed";	
	mysqli_close($con);	

// Display growth chart for a given TOTID: TOTID
} else if($_GET["action"]=="DisplayGrowthChart") {
	$TOTID =$_GET["TOTID"];

	
	$sql = "SELECT DOB, Gender FROM vivianpe_all4tot.TB_TOTProfile WHERE TOTID=".$TOTID;
	$result_sql=mysql_query($sql);
	if ($row = mysql_fetch_assoc($result_sql)) {
		$DOB = trim($row["DOB"]);
		$Gender = trim($row["Gender"]);
	} else {
		die('Error: bad TOTID');
		exit;
	}

	$heights=array();
	$weights=array();
	$heads=array();

	//height
	$sql = "SELECT Age, Per_3, Per_5, Per_10, Per_25, Per_50, Per_75, Per_90, Per_95, Per_97 FROM vivianpe_all4tot.HEIGHT WHERE Gender=".$Gender." ORDER BY Age Asc";
	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
		$temp=array();
		$temp["3"]=$row["Per_3"];
		$temp["5"]=$row["Per_5"];
		$temp["10"]=$row["Per_10"];
		$temp["25"]=$row["Per_25"];
		$temp["50"]=$row["Per_50"];
		$temp["75"]=$row["Per_75"];
		$temp["90"]=$row["Per_90"];
		$temp["95"]=$row["Per_95"];
		$temp["97"]=$row["Per_97"];
		$heights[$row["Age"]]=$temp;
	}
	//weight
	$sql = "SELECT Age, Per_3, Per_5, Per_10, Per_25, Per_50, Per_75, Per_90, Per_95, Per_97 FROM vivianpe_all4tot.WEIGHT WHERE Gender=".$Gender." ORDER BY Age Asc";
	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
		$temp=array();
		$temp["3"]=$row["Per_3"];
		$temp["5"]=$row["Per_5"];
		$temp["10"]=$row["Per_10"];
		$temp["25"]=$row["Per_25"];
		$temp["50"]=$row["Per_50"];
		$temp["75"]=$row["Per_75"];
		$temp["90"]=$row["Per_90"];
		$temp["95"]=$row["Per_95"];
		$temp["97"]=$row["Per_97"];
		$weights[$row["Age"]]=$temp;
	}
	//head
	$sql = "SELECT Age, Per_3, Per_5, Per_10, Per_25, Per_50, Per_75, Per_90, Per_95, Per_97 FROM vivianpe_all4tot.HEAD WHERE Gender=".$Gender." ORDER BY Age Asc";
	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
		$temp=array();
		$temp["3"]=$row["Per_3"];
		$temp["5"]=$row["Per_5"];
		$temp["10"]=$row["Per_10"];
		$temp["25"]=$row["Per_25"];
		$temp["50"]=$row["Per_50"];
		$temp["75"]=$row["Per_75"];
		$temp["90"]=$row["Per_90"];
		$temp["95"]=$row["Per_95"];
		$temp["97"]=$row["Per_97"];
		$heads[(string)$row["Age"]]=$temp;
	}

	$sql = "SELECT TOTID, Head, Height, Weight, DATE_FORMAT(MeasurementDate, '%m-%d-%Y') As MeasurementDate, DATE_FORMAT(MeasurementDate, '%Y-%m-%d') As SystemDate FROM vivianpe_all4tot.TB_GrowthChart WHERE TOTID=".$TOTID." AND Active = 1";
	$result_sql=mysql_query($sql);
	$result_json=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		$temp=array();
		$temp["TOTID"]=trim($row["TOTID"]);
		$temp["Head"]=trim($row["Head"]);
		$temp["Height"]=trim($row["Height"]);
		$temp["Weight"]=trim($row["Weight"]);
		$temp["MeasurementDate"]=trim($row["MeasurementDate"]);
		
		$month = (date("Y", time()) - date("Y", strtotime($row["SystemDate"]))) * 12 + (date("m", time()) - date("m", strtotime($row["SystemDate"])));
		$month += (date("d", time()) - date("d", strtotime($row["SystemDate"])))/31;

		$headRatio = checkRatio($heads, $month, $temp["Head"]);
		$heightRatio = checkRatio($heights, $month, $temp["Height"]);
		$weightRatio = checkRatio($weights, $month, $temp["Weight"]);
		
		$temp["HeadRatio"] = $headRatio;
		$temp["HeightRatio"] = $heightRatio;
		$temp["WeightRatio"] = $weightRatio;
		
		array_push($result_json, $temp);
	}
	$result_json=array_values($result_json);		
	$result_json_out=array("DisplayGrowthChart"=>$result_json);		
	echo json_encode($result_json_out);

// Get all message categories
} else if($_GET["action"]=="GetMessageCategories") {
	$sql = "SELECT MessageCategoryID, MessageCategory FROM vivianpe_all4tot.TB_MessageCategory WHERE Active = 1";
	$result_sql=mysql_query($sql);
	$result_json=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		$temp=array();
		$temp["MessageCategoryID"]=trim($row["MessageCategoryID"]);
		$temp["MessageCategory"]=trim($row["MessageCategory"]);
		array_push($result_json, $temp);
	}
	$result_json=array_values($result_json);		
	$result_json_out=array("GetMessageCategories"=>$result_json);		
	echo json_encode($result_json_out);
	
// Get a random message by a given message category id
} else if($_GET["action"]=="GetRandomMessage") {
	$MessageCategoryID=$_GET["MessageCategoryID"];
	$TOTID=$_GET["TOTID"];
	$Size=(int)$_GET["Size"];
	
	$sql = "SELECT DATE_FORMAT(DOB,'%m-%d-%Y') As DOB FROM vivianpe_all4tot.TB_TOTProfile WHERE TOTID=".$TOTID." AND Active=1";
	$tot_result_sql=mysql_query($sql);
	$result_json=array();
	if($row = mysql_fetch_assoc($tot_result_sql)) {
		$DOB = trim($row["DOB"]);

		$sql = "SELECT MessageID, Sequence, Age_In_Week, MessageType, TXT4TotsMessage, CharacterCount FROM vivianpe_all4tot.TXT4Tots WHERE MessageType IN (";
		$sql .= "SELECT MessageCategory FROM vivianpe_all4tot.TB_MessageCategory WHERE MessageCategoryID='".$MessageCategoryID."' AND Active = 1) AND ";
		$sql .= "((DATEDIFF(NOW(), STR_TO_DATE('".$DOB."', '%m-%d-%Y')))/7 + 10 > Age_IN_Week AND (DATEDIFF(NOW(), STR_TO_DATE('".$DOB."', '%m-%d-%Y')))/7 - 10 < Age_IN_Week)";
		//echo $sql."<br>";
		
		$result_sql=mysql_query($sql);
		$whole_result = array();
		while ($row = mysql_fetch_assoc($result_sql)) {
			$temp=array();
			$temp["MessageID"]=trim($row["MessageID"]);
			$temp["Sequence"]=trim($row["Sequence"]);
			$temp["Age_In_Week"]=trim($row["Age_In_Week"]);
			$temp["MessageType"]=trim($row["MessageType"]);
			$temp["TXT4TotsMessage"]=trim($row["TXT4TotsMessage"]);
			$temp["CharacterCount"]=trim($row["CharacterCount"]);
			array_push($whole_result, $temp);
		}
		while($Size>0&&sizeof($whole_result)>0) {
			$rnd = rand(0,sizeof($whole_result)-1);
			array_push($result_json, $whole_result[$rnd]);
			unset($whole_result[$rnd]);
			$Size--;
		}
	}
	$result_json=array_values($result_json);
	$result_json_out=array("GetRandomMessage"=>$result_json);
	echo json_encode($result_json_out);
// Get available carrier
} else if($_GET["action"]=="GetCarriers") {
	$sql = "SELECT CarrierID, Name FROM vivianpe_all4tot.TB_Carrier WHERE Active=1";
	
	$result_sql=mysql_query($sql);
	$result_json=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		$temp=array();
		$temp["CarrierID"]=trim($row["CarrierID"]);
		$temp["Name"]=trim($row["Name"]);
		array_push($result_json, $temp);
	}
	$result_json=array_values($result_json);
	$result_json_out=array("GetCarriers"=>$result_json);		
	echo json_encode($result_json_out);
// Get available sending interval
} else if($_GET["action"]=="GetIntervals") {
	$sql = "SELECT F.FrequencyID, FT.Type, F.Intervals FROM vivianpe_all4tot.TB_Frequency F";
	$sql .= " INNER JOIN vivianpe_all4tot.TB_FrequencyType FT ON F.FrequencyTypeID = FT.FrequencyTypeID WHERE F.Active=1 ORDER BY F.FrequencyTypeID DESC, Intervals";
	
	//echo $sql."<br>";
	
	$result_sql=mysql_query($sql);
	$result_json=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		$temp=array();
		$temp["FrequencyID"]=trim($row["FrequencyID"]);
		$temp["Type"]=trim($row["Type"]);
		$temp["Intervals"]=trim($row["Intervals"]);
		array_push($result_json, $temp);
	}
	$result_json=array_values($result_json);
	$result_json_out=array("GetIntervals"=>$result_json);		
	echo json_encode($result_json_out);

// subscribe message for a given TOTID: TOTID, MessageCategoryID, FrequencyID, PhoneNo, CarrierID, Immunization, SubscribeDate
} else if($_GET["action"]=="SubscribeMessage") {
	$TOTID =$_GET["TOTID"];
	$MessageCategoryID =$_GET["MessageCategoryID"];
	$FrequencyID = $_GET["FrequencyID"];
	$PhoneNo = $_GET["PhoneNo"];
	$CarrierID = $_GET["CarrierID"];
	$Immunization = $_GET["Immunization"];
	$SubscribeDate = $_GET["SubscribeDate"];
	
	$sql = "SELECT * FROM vivianpe_all4tot.TB_Subscribe WHERE TOTID=".$TOTID;
	//echo $sql."<br>";
	$result_sql=mysql_query($sql);
	if (!$row = mysql_fetch_assoc($result_sql)) {
		$sql = "INSERT INTO vivianpe_all4tot.TB_Subscribe (TOTID, MessageCategoryID, FrequencyID, PhoneNo, CarrierID, Immunization, SubscribeDate, Active) VALUES (";
		$sql .= $TOTID.",";
		$sql .= "'".$MessageCategoryID."',";
		$sql .= $FrequencyID.",";
		$sql .= "'".$PhoneNo."',";
		$sql .= $CarrierID.",";
		$sql .= $Immunization.",";
		$sql .= "Date(UTC_TIMESTAMP()),";
		$sql .= "1)";
	} else {
		$sql = "UPDATE vivianpe_all4tot.TB_Subscribe SET ";
		$sql .= "MessageCategoryID = '".$MessageCategoryID."',";
		$sql .= "FrequencyID = ".$FrequencyID.",";
		$sql .= "PhoneNo = '".$PhoneNo."',";
		$sql .= "CarrierID = ".$CarrierID.",";
		$sql .= "Immunization = ".$Immunization.",";
		$sql .= "SubscribeDate = Date(UTC_TIMESTAMP()),";
		$sql .= "Active = 1";
		$sql .= " WHERE TOTID=".$TOTID;
	}
	
	//echo $sql."<br>";
	
	$con=mysqli_connect($dbhost, $dbuser, $dbpasswd, $db) or die("Unable to connect to SQL server for insertion");
	// Check connection
	if (!mysqli_query($con,$sql)) {
		die('Error: ' . mysqli_error());
	} else {
		$temp = array();
		$temp["result"]="1 record changed"; //inserted or updated
		$result_json=array();
		array_push($result_json, $temp);
		$result_json=array("SubscribeMessage"=>$result_json);
		echo json_encode($result_json);
	}
	//echo "1 record changed";	
	mysqli_close($con);	
// show subscription information for a given TOTID
} else if($_GET["action"]=="ShowSubscription") {
	$TOTID =$_GET["TOTID"];
	
	$sql = "SELECT MessageCategoryID, F.Intervals, FT.Type, PhoneNo, C.Name, S.Immunization,  DATE_FORMAT(S.SubscribeDate,'%m-%d-%Y') As SubscribeDate FROM vivianpe_all4tot.TB_Subscribe S ";
	$sql .= "INNER JOIN TB_Frequency F ON S.FrequencyID = F.FrequencyID ";
	$sql .= "INNER JOIN TB_FrequencyType FT ON F.FrequencyTypeID = FT.FrequencyTypeID ";
	$sql .= "INNER JOIN TB_Carrier C ON S.CarrierID = C.CarrierID ";
	$sql .= "WHERE S.TOTID=".$TOTID." AND S.Active=1";
	
	//echo $sql."<br>";
	
	$result_sql=mysql_query($sql);
	$result_json=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		$temp=array();
		$MessageCategoryID=trim($row["MessageCategoryID"]);
		$MessageCategories=array();
		if(strlen($MessageCategoryID)>0) {
			$sql = "SELECT MessageCategory FROM vivianpe_all4tot.TB_MessageCategory WHERE MessageCategoryID IN (".$MessageCategoryID.") AND Active=1";
			$inner_result_sql=mysql_query($sql);
			while ($inner_row = mysql_fetch_assoc($inner_result_sql)) {
				array_push($MessageCategories, trim($inner_row["MessageCategory"]));
			}
			//echo $sql."<br>";
		}
		$temp["MessageCategories"]=$MessageCategories;
		$temp["Intervals"]=trim($row["Intervals"]);
		$temp["Type"]=trim($row["Type"]);
		$temp["PhoneNo"]=trim($row["PhoneNo"]);
		$temp["Name"]=trim($row["Name"]);
		$temp["Immunization"]=trim($row["Immunization"]);
		$temp["SubscribeDate"]=trim($row["SubscribeDate"]);
		array_push($result_json, $temp);
	}
	$result_json=array_values($result_json);
	$result_json_out=array("ShowSubscription"=>$result_json);		
	echo json_encode($result_json_out);

//delete a subscription
} else if($_GET["action"]=="DeleteSubscribeMessage") {
	$TOTID =$_GET["TOTID"];
	
	$sql = "DELETE FROM vivianpe_all4tot.TB_Subscribe WHERE TOTID=".$TOTID;
	$con=mysqli_connect($dbhost, $dbuser, $dbpasswd, $db) or die("Unable to connect to SQL server for insertion");
	// Check connection
	if (!mysqli_query($con,$sql)) {
		die('Error: ' . mysqli_error());
	} else {
		$temp = array();
		$temp["result"]="1 record deleted";
		$result_json=array();
		array_push($result_json, $temp);
		$result_json=array("DeleteSubscribeMessage"=>$result_json);
		echo json_encode($result_json);
	}
	mysqli_close($con);
	
// show vaccine for a TOT
} else if($_GET["action"]=="ShowVaccine") {
	$TOTID =$_GET["TOTID"];
	
	$sql = "SELECT DOB FROM vivianpe_all4tot.TB_TOTProfile WHERE TOTID=".$TOTID;
	$result_sql=mysql_query($sql);
	$result_json=array();
	if ($row = mysql_fetch_assoc($result_sql)) {
		$DOB = trim($row["DOB"]);
		$sql = "SELECT Disease, Vaccine, DATE_FORMAT(DATE_ADD('".$DOB."', INTERVAL StartMonth Month), '%m-%d-%Y') As StartDate, DATE_FORMAT(DATE_ADD('".$DOB."', INTERVAL EndMonth Month), '%m-%d-%Y') As EndDate, Remark FROM vivianpe_all4tot.TB_Immunization ORDER BY DATE_ADD('".$DOB."', INTERVAL StartMonth Month)";
		$result_sql=mysql_query($sql);
		while ($row = mysql_fetch_assoc($result_sql)) {
			$temp=array();
			$temp["Disease"]=trim($row["Disease"]);
			$temp["Vaccine"]=trim($row["Vaccine"]);
			$temp["StartDate"]=trim($row["StartDate"]);
			if($DOB==trim($row["EndDate"]))
				$temp["EndDate"]="";
			else
				$temp["EndDate"]=trim($row["EndDate"]);
			$temp["Remark"]=trim($row["Remark"]);
			array_push($result_json, $temp);
		}
	}
	$result_json=array_values($result_json);
	$result_json_out=array("ShowVaccine"=>$result_json);
	echo json_encode($result_json_out);

// show other reminders for a TOT
} else if($_GET["action"]=="ShowOtherReminders") {
	$TOTID =$_GET["TOTID"];
	
	$sql = "SELECT DOB FROM vivianpe_all4tot.TB_TOTProfile WHERE TOTID=".$TOTID;
	$result_sql=mysql_query($sql);
	$result_json=array();
	if ($row = mysql_fetch_assoc($result_sql)) {
		$DOB = trim($row["DOB"]);
		$sql = "SELECT Description, Category, DATE_FORMAT(DATE_ADD('".$DOB."', INTERVAL ElapsedTime Month), '%m-%d-%Y') As StartDate FROM vivianpe_all4tot.TB_OtherReminders ORDER BY DATE_ADD('".$DOB."', INTERVAL ElapsedTime Month)";
		$result_sql=mysql_query($sql);
		while ($row = mysql_fetch_assoc($result_sql)) {
			$temp=array();
			$temp["Description"]=trim($row["Description"]);
			$temp["Category"]=trim($row["Category"]);
			$temp["StartDate"]=trim($row["StartDate"]);
			array_push($result_json, $temp);
		}
	}
	$result_json=array_values($result_json);
	$result_json_out=array("ShowOtherReminders"=>$result_json);
	echo json_encode($result_json_out);
	
} else if($_GET["action"]=="GetRange") {
	$DOB = $_GET["DOB"];
	$Gender = $_GET["Gender"];

	$maxHeight="";
	$maxHead="";
	$maxWeight="";
	$minHeight="";
	$minHead="";
	$minWeight="";

	$sql = "SELECT Per_3 As MinV, Per_97 As MaxV FROM vivianpe_all4tot.HEIGHT WHERE Gender=".$_GET["Gender"];
	$sql .=" AND ((Year(NOW()) - Year(STR_TO_DATE('".$DOB."','%m-%d-%Y'))) * 12 + (Month(NOW()) - Month(STR_TO_DATE('".$DOB."','%m-%d-%Y'))) + ROUND((Day(NOW()) - Day(STR_TO_DATE('".$DOB."','%m-%d-%Y')))/31, 1)) >= Age ORDER BY Age DESC LIMIT 1";
	$result_sql=mysql_query($sql);
	$result_json=array();
	if ($row = mysql_fetch_assoc($result_sql)) {
		$maxHeight = trim($row["MaxV"]);
		$minHeight = trim($row["MinV"]);
	}
	
	$sql = "SELECT Per_3 As MinV, Per_97 As MaxV FROM vivianpe_all4tot.HEAD WHERE Gender=".$_GET["Gender"];
	$sql .=" AND ((Year(NOW()) - Year(STR_TO_DATE('".$DOB."','%m-%d-%Y'))) * 12 + (Month(NOW()) - Month(STR_TO_DATE('".$DOB."','%m-%d-%Y'))) + ROUND((Day(NOW()) - Day(STR_TO_DATE('".$DOB."','%m-%d-%Y')))/31, 1)) >= Age ORDER BY Age DESC LIMIT 1";
	$result_sql=mysql_query($sql);
	$result_json=array();
	if ($row = mysql_fetch_assoc($result_sql)) {
		$maxHead = trim($row["MaxV"]);
		$minHead = trim($row["MinV"]);
	}

	$sql = "SELECT Per_3 As MinV, Per_97 As MaxV FROM vivianpe_all4tot.WEIGHT WHERE Gender=".$_GET["Gender"];
	$sql .=" AND ((Year(NOW()) - Year(STR_TO_DATE('".$DOB."','%m-%d-%Y'))) * 12 + (Month(NOW()) - Month(STR_TO_DATE('".$DOB."','%m-%d-%Y'))) + ROUND((Day(NOW()) - Day(STR_TO_DATE('".$DOB."','%m-%d-%Y')))/31, 1)) >= Age ORDER BY Age DESC LIMIT 1";
	$result_sql=mysql_query($sql);
	$result_json=array();
	if ($row = mysql_fetch_assoc($result_sql)) {
		$maxWeight = trim($row["MaxV"]);
		$minWeight = trim($row["MinV"]);
	}
	$temp = array();
	$temp["maxHeight"]=$maxHeight;
	$temp["maxWeight"]=$maxWeight;
	$temp["maxHead"]=$maxHead;
	$temp["minHeight"]=$minHeight;
	$temp["minWeight"]=$minWeight;
	$temp["minHead"]=$minHead;

	array_push($result_json, $temp);

	$result_json=array_values($result_json);
	$result_json_out=array("GetRange"=>$result_json);
	echo json_encode($result_json_out);
}

function checkRatio($rArray, $month, $value) {

	$isLast = true;
	
	$currentMonth="0.0";
	$prevMonth="0.0";
	foreach($rArray as $key => $rValue) {
		$prevMonth = $currentMonth;
		$currentMonth = $key;

		if((int)$month<=(int)$key) {
			$isLast=false;
			break;
		}
	}
	$checkingMonth = $prevMonth;
	if($isLast)
		$checkingMonth = $currentMonth;
	
	$dataArray = $rArray[$checkingMonth];
	$prevKey="";
	$currentKey="";
	$isLast = true;

	foreach($dataArray as $key => $rValue) {
		$prevKey = $currentKey;
		$currentKey = $key;
		if($rValue>$value) {
			$isLast = false;
			break;
		}
	}
	$retval=0;
	//3% or 97%
	if($prevKey==""||$isLast) {
		$retval = $currentKey;
	} else {
		$retval = (int)$prevKey + ((int)currentKey-(int)prevKey)*((double)$value-(double)$rArray[$prevKey])/((double)$rArray[$currentKey]-(double)$rArray[$prevKey]);
		$retval = round($retval/5);
		$retval = (int)$retval*5;
		
	}
	return $retval;
}

?>