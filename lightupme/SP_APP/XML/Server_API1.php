<?php
require_once("db_conn.php");

function distance($lat1, $lng1, $lat2, $lng2, $miles = true)
{
	$pi80 = M_PI / 180;
	$lat1 *= $pi80;
	$lng1 *= $pi80;
	$lat2 *= $pi80;
	$lng2 *= $pi80;

	$r = 6372.797; 
	$dlat = $lat2 - $lat1;
	$dlng = $lng2 - $lng1;
	$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
	$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
	$km = $r * $c;

	return $km * 0.621371192;
}	
	

//END OF FUNCTION

//START SERVER API CALL
	
	
//getNearbyCenter
if($_GET["action"]=="getNearbyCenter"){
	
	$city =$_GET["city"];
	$state =$_GET["state"];
	$range =$_GET["range"];
	
	if(trim($range)=='')
	$range=30;
	
	$sql="SELECT  Latitude, Longitude FROM vivianpe_SP_DEMO.TB_ZIPCODE where State='".$state."' and City = '".$city."' limit 1";
	$row = mysql_fetch_assoc(mysql_query($sql)); //Get source LAT/LON
	
	//Query2	
	$sql="SELECT DISTINCT City, Latitude,Longitude FROM vivianpe_SP_DEMO.TB_ZIPCODE where State='".$state."'";
	$result_sql=mysql_query($sql);
	$result_json=array();
	$city_list="";
	while ($row1 = mysql_fetch_assoc($result_sql)) {
		if(distance($row["Latitude"], $row["Longitude"], $row1["Latitude"], $row1["Longitude"]) <=$range){
			$city_list.="'".$row1["City"]."',";
		}			
	}
	$city_list = substr($city_list, 0, -1);
	
	$sql="SELECT name1, name2, location_street1, location_street2, location_city, location_state, location_zip, phone, latlon FROM vivianpe_SP_DEMO.TB_SAMHSA_LOC where  
	location_state='".$state."' and location_city in (".$city_list.")";
	
	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
			
				$temp = array();
				$temp["Name1"]=$row["name1"];
				$temp["Name2"]=$row["name2"];			
				$temp["Phone"]=$row["phone"];			
				$temp["Street1"]=$row["location_street1"];
				$temp["Street2"]=$row["location_street2"];
				$temp["City"]=$row["location_city"];
				$temp["State"]=$row["location_state"];
				$temp["latlon"]=$row["latlon"];
				
				array_push($result_json, $temp);
	
	}
	
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
	
}	
	




//Login
if($_GET["action"]=="Login"){ //check user login and authentication
	$result_json=array();
	$username =mysql_real_escape_string($_GET["username"]);
	$password =mysql_real_escape_string($_GET["password"]);
	$accounttype =mysql_real_escape_string($_GET["accounttype"]);
	//1=G, 2=F, 3=System
	$iosdeviceid =mysql_real_escape_string($_GET["iosdeviceid"]);
	
	
	$status="";
	$userID="";
	
	if($accounttype=='1' || $accounttype=='2'){
		$sql="SELECT  UserID, LightUpMeID FROM vivianpe_SP_DEMO.TB_USER where UserName='".$username."' and AccountTypeID = '".$accounttype."'";
		$num_rows = mysql_num_rows(mysql_query($sql));
		if($num_rows==0)
		$status="ERROR01 - Account does not exist";
		if($num_rows==1){
			$row = mysql_fetch_assoc(mysql_query($sql));
			$userID=$row["UserID"];
			$LightUpMeID=$row["LightUpMeID"];
			$status="Login OKAY";
			
			$sql_iosid="UPDATE vivianpe_SP_DEMO.TB_USER SET iOSDeviceID = '".$iosdeviceid."' WHERE UserID='".$userID."'";
			$result_sql_iosid=mysql_query($sql_iosid);
			
			
		}
		
	}else if($accounttype=='3') {
		$sql="SELECT LightUpMeID FROM vivianpe_SP_DEMO.TB_USER where UserName='".$username."' and AccountTypeID = '".$accounttype."' and Password= '".$password."'";
		$num_rows = mysql_num_rows(mysql_query($sql));
		if($num_rows==0)
		$status="ERROR02 - Please check on account name and password";
		if($num_rows==1){
			$row = mysql_fetch_assoc(mysql_query($sql));
			$userID=$row["UserID"];
			$LightUpMeID=$row["LightUpMeID"];
			$status="Login OKAY";
			
			$sql_iosid="UPDATE vivianpe_SP_DEMO.TB_USER SET iOSDeviceID = '".$iosdeviceid."' WHERE UserID='".$userID."'";
			$result_sql_iosid=mysql_query($sql_iosid);
		}	
	}else{
		$status="ERROR03 - Account type ID not correct";
	}
	
	array_push($result_json, $status);
	array_push($result_json, $LightUpMeID);
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
}
	

	
//CreateProfile	
if($_GET["action"]=="CreateProfile"){
	$result_json=array();
	$username =mysql_real_escape_string($_GET["username"]);
	$password =mysql_real_escape_string($_GET["password"]);
	$accounttype =mysql_real_escape_string($_GET["accounttype"]);
	$usertype =mysql_real_escape_string($_GET["usertype"]); //1=P. 2=D, 3=Other (F/F)
	$name =mysql_real_escape_string($_GET["name"]);
	//$lastname =mysql_real_escape_string($_GET["lastname"]);
	$email =mysql_real_escape_string($_GET["email"]);
	$phone =mysql_real_escape_string($_GET["phone"]);
	$lightupmeid =mysql_real_escape_string($_GET["lightupmeid"]);
	$lightupmeid_random=rand(10000000, 99999999);
	$gender=mysql_real_escape_string($_GET["gender"]); //1-Male, 2=Female, 3-Other
	$DOB=mysql_real_escape_string($_GET["DOB"]); //format?
	$occ=mysql_real_escape_string($_GET["occupation"]);
	$fflumid=mysql_real_escape_string($_GET["FofoplumID"]);
	
	$status="";
	$user_exist=false;
	$lightmeupid_exist=false;
	$FofoplumID_exist=false;
	
	//http://127.0.0.1/testAAgg88/APP3/Server_API1.php?action=CreateProfile&username=abc&password=abc&accounttype=1&usertype=1&firstname=myfirst&lastname=mylast&email=aaa@yahoo.com&phone=888555212
	
	if(trim($username)=="" || trim($password)=="" || trim($accounttype)=="" || trim($usertype)=="" || trim($name)=="" || trim($email)==""
	|| trim($phone)==""  ){
		$status="ERROR04 - Information is not completed, please check";
	}else{
		while(true){
			$sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where LightUpMeID='".$lightupmeid_random."'";
			$num_rows = mysql_num_rows(mysql_query($sql));
			if ($num_rows==0)
				break;
			else
				$lightupmeid_random=rand(10000000, 99999999);
		}	
		$sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where UserName='".$username."'";
		$num_rows = mysql_num_rows(mysql_query($sql));
		if ($num_rows>0){
				$user_exist=true;
				$status="ERROR05 - Username Exist";
		}
		$sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where LightUpMeID='".$lightupmeid."'";
		$num_rows = mysql_num_rows(mysql_query($sql));
		if ($num_rows>0){
			$lightmeupid_exist=true;
			$status="ERROR06 - Light-up-me ID Exist";
		}
		
		
		if(trim($fflumid)==""){ // NO reference !! Just setup as simple user
		
			if($user_exist==false && $lightmeupid_exist==false){
			if(trim($lightupmeid)==""){ //use random ID
				$sql_cp="INSERT INTO vivianpe_SP_DEMO.TB_USER (UserName,  Password, AccountTypeID, UserTypeID,  Active, Name,  Email, Phone, LightUpMeID, Gender, DOB, Occupation) VALUES 
				('".$username."' , '".$password."', '". $accounttype."', '".$usertype."','1', '".$name."', '".$email."', '".$phone."', '".$lightupmeid_random."', '".$gender."','". $DOB."','". $occ."')";
			}else{ //use user specific ID
				$sql_cp="INSERT INTO vivianpe_SP_DEMO.TB_USER (UserName,  Password, AccountTypeID, UserTypeID,  Active, Name,  Email, Phone, LightUpMeID, Gender, DOB, Occupation) VALUES 
				('".$username."' , '".$password."', '". $accounttype."', '".$usertype."','1', '".$name."', '".$email."', '".$phone."', '".$lightupmeid."', '".$gender."','". $DOB."','". $occ."')";
			}
			$result_sql=mysql_query($sql_cp);
			if (!$result_sql) {
				$status="ERROR07 - SQL ERROR";
			}else{
				if(trim($lightupmeid)==""){
					//$status="Create Okay - ".$lightupmeid_random;
					$status="Create Okay";
					CreateXMPPUser($name,  $email, $usertype);
				}else{
					//$status="Create Okay - ".$lightupmeid;
					$status="Create Okay";
					CreateXMPPUser($name, $email, $usertype);
				}
			}
			}
		
		
		}else{ //user enter reference ID
			$sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where LightUpMeID='".$fflumid."' and UserTypeID='1'";
			$num_rows = mysql_num_rows(mysql_query($sql));
			echo $sql;
			echo "<br>";
			echo $num_rows;
			
			
			if ($num_rows==1){
					$FofoplumID_exist=true;
					
					if($user_exist==false && $lightmeupid_exist==false){
						if(trim($lightupmeid)==""){ //use random ID
							$sql_cp="INSERT INTO vivianpe_SP_DEMO.TB_USER (UserName,  Password, AccountTypeID, UserTypeID,  Active, Name,  Email, Phone, LightUpMeID, Gender, DOB, Occupation, FofoplumID) VALUES 
							('".$username."' , '".$password."', '". $accounttype."', '".$usertype."','1', '".$name."', '".$email."', '".$phone."', '".$lightupmeid_random."', '".$gender."','". $DOB."','". $occ."','".$fflumid."')";
						}else{ //use user specific ID
							$sql_cp="INSERT INTO vivianpe_SP_DEMO.TB_USER (UserName,  Password, AccountTypeID, UserTypeID,  Active, Name,  Email, Phone, LightUpMeID, Gender, DOB, Occupation, FofoplumID) VALUES 
							('".$username."' , '".$password."', '". $accounttype."', '".$usertype."','1', '".$name."', '".$email."', '".$phone."', '".$lightupmeid."', '".$gender."','". $DOB."','". $occ."','".$fflumid."')";
						}
						$result_sql=mysql_query($sql_cp);
					if (!$result_sql) {
						$status="ERROR07 - SQL ERROR";
					}else{
						if(trim($lightupmeid)==""){ //Reference ID = YES, Light-Up-Me ID = NO   (most case - Family or Friend)
							//$status="Create Okay - ".$lightupmeid_random;
							$status="Create Okay";
							CreateXMPPUser($name, $email, $usertype);
							
							
							$sql_relation="INSERT INTO vivianpe_SP_DEMO.TB_RELATIONSHIP (Patiend_LUMID, FF_LUMID) VALUES ('".$fflumid."','".$lightupmeid_random."')";
							$result_sql_relation=mysql_query($sql_relation);
							
							
							//echo $sql_relation;
							
							
							
						}else{ //Reference ID = YES, Light-Up-Me ID = Yes   (most case - Re-Rgister on other Drvices)
							//$status="Create Okay - ".$lightupmeid;
							$status="Create Okay";
							CreateXMPPUser($name, $email, $usertype);
							
							$sql_relation="INSERT INTO vivianpe_SP_DEMO.TB_RELATIONSHIP (Patiend_LUMID, FF_LUMID) VALUES ('".$fflumid."','".$lightupmeid."')";
							$result_sql_relation=mysql_query($sql_relation);
							
							
							//echo $sql_relation;
							
						}
					}
				}
					
			}else{
				$FofoplumID_exist=false;
				$status="ERROR07A - Reference Light Up Me ID not Exist or More than one in system";
			}
		}
	}
	array_push($result_json, $status);
	
	if($status=="Create Okay"){
		if(trim($lightupmeid)=="")
			array_push($result_json, $lightupmeid_random);
		else
			array_push($result_json, $lightupmeid);
	}
	
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);	
}




//DisplayProfilebylightupmeid
if($_GET["action"]=="DisplayProfilebylightupmeid"){

	$status="";
	$result_json=array();
	$lightupmeid =mysql_real_escape_string($_GET["lightupmeid"]);
	
	if(trim($lightupmeid)==""){
		$status="ERROR08 - Light Up Me ID empty";
	
	}else{
	
		$sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where LightUpMeID='".$lightupmeid."'";
		$num_rows = mysql_num_rows(mysql_query($sql));
		if($num_rows==0)
		$status="ERROR09 - Light Up Me ID no match";
		if($num_rows==1){
			
		$sql_1=	"SELECT UserID, UserName, AccountTypeID, UserTypeID, Name, Email, Phone, LightUpMeID,Gender,DOB, Occupation   FROM vivianpe_SP_DEMO.TB_USER where LightUpMeID='".$lightupmeid."'";	
		$result_sql_1=mysql_query($sql_1);
		$row_1 = mysql_fetch_assoc($result_sql_1);	
		if (!$result_sql_1) {
				$status="ERROR11 - SQL ERROR";
			}else{
				$status="Create Okay";
			}	
				
		}else if($num_rows>1){
			$status="ERROR10 - Light Up Me ID duplicate";
		}
	}
	array_push($result_json, $status);
	if($status=="Create Okay")
	array_push($result_json, $row_1);
	
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
	
}



//Flagbylightupmeid
if($_GET["action"]=="Flagbylightupmeid"){
	$status="";
	$result_json=array();
	$flagwho =mysql_real_escape_string($_GET["flagwho"]);
	$whoflag =mysql_real_escape_string($_GET["whoflag"]);
	$comment =mysql_real_escape_string($_GET["comment"]);
	$flagit1=false;
	$flagit2=false;
	
	$sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where LightUpMeID='".$flagwho."'";
	$num_rows = mysql_num_rows(mysql_query($sql));
	
	if($num_rows==0)
	$status="ERROR12 - Light Up Me ID no match (flagwho)";
	else
	$flagit1=true;
	
	
	$sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where LightUpMeID='".$whoflag."'";
	$num_rows = mysql_num_rows(mysql_query($sql));
	if($num_rows==0)
	$status="ERROR13 - Light Up Me ID no match (whoflag)";
	else
	$flagit2=true;
	

	if($flagit1 && $flagit2){
		$sql_flag="INSERT INTO vivianpe_SP_DEMO.TB_FLAG (FlagWho, WhoFLag, Comment) VALUES ('".$flagwho."','".$whoflag."','".$comment."')";
		$result_sql_flag=mysql_query($sql_flag);
					if (!$result_sql_flag) {
						$status="ERROR14 - SQL ERROR";
					}else{
							$status="Flag Okay";
					}
	
	
	}	
	array_push($result_json, $status);
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
}

if($_GET["action"]=="DeFlagbylightupmeid"){

	$status="";
	$result_json=array();
	$deflagwho =mysql_real_escape_string($_GET["deflagwho"]);
	
	
	$sql_deflag="UPDATE vivianpe_SP_DEMO.TB_FLAG SET Active = '0' WHERE FlagWho='".$deflagwho."'";
	$result_sql_deflag=mysql_query($sql_deflag);
	if (!$result_sql_deflag) {
		$status="ERROR15 - SQL ERROR";
	}else{
		$status="DeFlag Okay";
	}
	array_push($result_json, $status);
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
}




//AddPatientbyPro
if($_GET["action"]=="AddPatientbyPro"){

	$result_json=array();
	$name =mysql_real_escape_string($_GET["name"]);
	//$lastname =mysql_real_escape_string($_GET["lastname"]);
	$email =mysql_real_escape_string($_GET["email"]);
	$phone =mysql_real_escape_string($_GET["phone"]);
	$lightupmeid =mysql_real_escape_string($_GET["lightupmeid"]);
	$lightupmeid_random=rand(10000000, 99999999);
	$gender=mysql_real_escape_string($_GET["gender"]); //1-Male, 2=Female, 3-Other
	$DOB=mysql_real_escape_string($_GET["DOB"]); //format?
	$occ=mysql_real_escape_string($_GET["occupation"]);
	$rank=mysql_real_escape_string($_GET["rank"]);
	$doctorid=mysql_real_escape_string($_GET["doctorid"]);
		
	$status="";
	$user_exist=false;
	$lightmeupid_exist=false;
	
	if(trim($Name)=="" || trim($email)==""
	|| trim($phone)==""  ||  trim($gender)==""  ||  trim($DOB)==""  || trim($occ)==""){
		$status="ERROR20 - Information is not completed, please check";
	}else{
		while(true){
			$sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where LightUpMeID='".$lightupmeid_random."'";
			$num_rows = mysql_num_rows(mysql_query($sql));
			if ($num_rows==0)
				break;
			else
				$lightupmeid_random=rand(10000000, 99999999);
		}
		
		if(trim($lightupmeid)==""){ // no id, use random !!
		
			$sql_add_patient="INSERT INTO vivianpe_SP_DEMO.TB_PATIENT (Name, Email, Phone, Gender, LightUpMeID, DOB, Occupation, Rank, DoctorID ) 
			VALUES ('".$name."', '".$email."', '".$phone."', '".$gender."', '".$lightupmeid_random."', '".$DOB."', '".$occ."','".$rank."','".$doctorid."')";
			
		}else{  //with id, use existing
			
			$sql_add_patient="INSERT INTO vivianpe_SP_DEMO.TB_PATIENT (Name,  Email, Phone, Gender, LightUpMeID, DOB, Occupation, Rank, DoctorID ) 
			VALUES ('".$name."', '".$email."', '".$phone."', '".$gender."', '".$lightupmeid."', '".$DOB."', '".$occ."','".$rank."',".$doctorid."')";
		}
		
		$result_sql_add_patient=mysql_query($sql_add_patient);
		
		if (!$result_sql_add_patient) {
			$status="ERROR21 - SQL ERROR";
		}else{
		
			if(trim($lightupmeid)==""){
				$status="Create Okay - ".$lightupmeid_random;
			}else{
				$status="Create Okay - ".$lightupmeid;
			}	
		}	
	}
	array_push($result_json, $status);
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
}




//DisplayPatientProfilebylightupmeid
if($_GET["action"]=="DisplayPatientProfilebylightupmeid"){

	$status="";
	$result_json=array();
	$lightupmeid =mysql_real_escape_string($_GET["lightupmeid"]);
	
	if(trim($lightupmeid)==""){
		$status="ERROR30 - Light Up Me ID empty";
	
	}else{
	
		$sql="SELECT PatientID FROM vivianpe_SP_DEMO.TB_PATIENT where LightUpMeID='".$lightupmeid."'";
		$num_rows = mysql_num_rows(mysql_query($sql));
		if($num_rows==0)
		$status="ERROR31 - Light Up Me ID no match";
		if($num_rows==1){
			
		$sql_1=	"SELECT Name, Email, Phone, Gender, LightUpMeID,DOB, Occupation, Rank, DoctorID   FROM vivianpe_SP_DEMO.TB_PATIENT where LightUpMeID='".$lightupmeid."'";	
		$result_sql_1=mysql_query($sql_1);
		$row_1 = mysql_fetch_assoc($result_sql_1);	
		if (!$result_sql_1) {
				$status="ERROR32 - SQL ERROR";
			}else{
				$status="SQL Okay";
			}	
				
		}else if($num_rows>1){
			$status="ERROR33 - Light Up Me ID duplicate";
		}
	}
	array_push($result_json, $status);
	if($status=="SQL Okay")
	array_push($result_json, $row_1);
	
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
}



//PatientListbyDoctorID
if($_GET["action"]=="PatientListbyDoctorID"){

		$status="";
		$result_json=array();
		$DoctorID =mysql_real_escape_string($_GET["DoctorID"]);
		$sql_1=	"SELECT Name Email, Phone, Gender, LightUpMeID,DOB, Occupation, Rank, DoctorID   FROM vivianpe_SP_DEMO.TB_PATIENT where DoctorID='".$DoctorID."'";	
		$result_sql_1=mysql_query($sql_1);
		
	while ($row = mysql_fetch_assoc($result_sql_1)) {
			
				$temp = array();
				$temp["Name"]=$row["Name"];
				$temp["Email"]=$row["Email"];			
				$temp["Phone"]=$row["Phone"];
				$temp["Gender"]=$row["Gender"];
				$temp["LightUpMeID"]=$row["LightUpMeID"];
				$temp["DOB"]=$row["DOB"];
				$temp["Occupation"]=$row["Occupation"];
				$temp["Rank"]=$row["Rank"];
				
				array_push($result_json, $temp);
	}
	
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
		
		// $row_1 = mysql_fetch_assoc($result_sql_1);	
		// if (!$result_sql_1) {
				// $status="ERROR40 - SQL ERROR";
			// }else{
				// $status="SQL Okay";
			// }	

}



//AddPatientbyFF
if($_GET["action"]=="AddPatientbyFF"){

	$status="";
	$result_json=array();
	$patient_lightupmeid =mysql_real_escape_string($_GET["patient_lightupmeid"]);
	$friendfamily_lightupmeid =mysql_real_escape_string($_GET["friendfamily_lightupmeid"]);
	
	$sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where UserTypeID='1' and LightUpMeID='".$patient_lightupmeid."'";
	$num_rows = mysql_num_rows(mysql_query($sql));
	if($num_rows==0 or $num_rows>1)
		$status="ERROR50 - Patient lightupmeid not exist or duplicate";
	else{
	
		$sql_2="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where UserTypeID='3' and LightUpMeID='".$friendfamily_lightupmeid."'";
		$num_rows_2 = mysql_num_rows(mysql_query($sql_2));
		if($num_rows_2==0 or $num_rows_2>1)
			$status="ERROR51 - SQL Friend or Family lightupmeid not exist or duplicate";
		else{
		
			$sql_relation="INSERT INTO vivianpe_SP_DEMO.TB_RELATIONSHIP (Patiend_LUMID, FF_LUMID) VALUES ('".$patient_lightupmeid."','".$friendfamily_lightupmeid."')";
			$result_sql_relation=mysql_query($sql_relation);
		
			if (!$result_sql_relation) {
				$status="ERROR52 - SQL ERROR";
			}else{
				$status="Relationship Added Okay";
			}	
		}
	}
	
	array_push($result_json, $status);
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
}



//ListFriend
if($_GET["action"]=="ListFriend"){ //for Patient to list all his/her friends
	$status="";
	$result_json=array();
	$patient_lightupmeid =mysql_real_escape_string($_GET["patient_lightupmeid"]);
	$sql="SELECT FF_LUMID FROM vivianpe_SP_DEMO.TB_RELATIONSHIP where Patiend_LUMID='".$patient_lightupmeid."'";
	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
				$temp = array();
				$temp["FF_LUMID"]=$row["FF_LUMID"];
				array_push($result_json, $temp);
	}
	
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
}

//ListPatient
if($_GET["action"]=="ListPatient"){ //for friend/family to list all patient friend
	$status="";
	$result_json=array();
	$friendfamily_lightupmeid =mysql_real_escape_string($_GET["friendfamily_lightupmeid"]);
	
	$sql="SELECT Patiend_LUMID FROM vivianpe_SP_DEMO.TB_RELATIONSHIP where FF_LUMID='".$friendfamily_lightupmeid."'";
	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
				$temp = array();
				$temp["Patiend_LUMID"]=$row["Patiend_LUMID"];
				array_push($result_json, $temp);
	}
	
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);


}



// if($_GET["action"]=="UpdateProfilebylightupmeid"){
	// $result_json=array();
	// $username =mysql_real_escape_string($_GET["username"]);
	// $password =mysql_real_escape_string($_GET["password"]);
	// $accounttype =mysql_real_escape_string($_GET["accounttype"]);
	// $usertype =mysql_real_escape_string($_GET["usertype"]);
	// $firstname =mysql_real_escape_string($_GET["firstname"]);
	// $lastname =mysql_real_escape_string($_GET["lastname"]);
	// $email =mysql_real_escape_string($_GET["email"]);
	// $phone =mysql_real_escape_string($_GET["phone"]);
	// $lightupmeid =mysql_real_escape_string($_GET["lightupmeid"]);
	
	
	
	
	// if(trim($lightupmeid)==""){
		// $status="ERROR08 - Light Up Me ID empty";
	
	// }else{
	
		// $sql="SELECT UserID FROM vivianpe_SP_DEMO.TB_USER where LightUpMeID='".$lightupmeid."'";
		// $num_rows = mysql_num_rows(mysql_query($sql));
		// if($num_rows==0)
		// $status="ERROR09 - Light Up Me ID no match";
		// if($num_rows==1){
			
			
			
			
			
			
		// }else if($num_rows>1){
			// $status="ERROR10 - Light Up Me ID duplicate";
		// }
		
	
	
	
	// }
	
// }


//CallTTS
if($_GET["action"]=="CallTTS"){

	$status="";
	$result_json=array();
	$phone =mysql_real_escape_string($_GET["phone"]);
	$message =mysql_real_escape_string($_GET["message"]);
	if (is_numeric ($phone)){
		if(strlen($phone)==10){	
			$myFile = "XML/".$phone.".xml";
			$fh = fopen($myFile, 'w') or die("can't open file");
			$stringData = "<Response>
			<Say voice='woman' language='en'>".$message."</Say>
			<Play>http://demo.twilio.com/docs/classic.mp3</Play>
			</Response>";
			fwrite($fh, $stringData);
			fclose($fh);
			
			require "twilio/Services/Twilio.php";
			$AccountSid = "AC83804311786402cadc96419ec1518ce1";
			$AuthToken = "234de42b42e37785e044efeaf160d6a8";
			$client = new Services_Twilio($AccountSid, $AuthToken);
			$xml_location="http://vivianpeter.com/SP_APP/XML/".$phone.".xml";
			echo $xml_location;
			$call = $client->account->calls->create("9793536660", $phone, $xml_location, array());
			echo $call->sid;
			$status="Call Sent";
		}else{
			$status="ERROR60 - Phone Number Format is not correct";
		}
	}else{
		$status="ERROR61 - Phone Number Format is not correct";
	}
	array_push($result_json, $status);
	$result_json=array("Result"=>$result_json);
	echo json_encode($result_json);
}


function CreateXMPPUser(){


}

	
//echo hash('sha256', "EOOO4 is the best");


?>