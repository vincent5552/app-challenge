<?php
	/* Send an SMS using Twilio. You can run this file 3 different ways:
	 *
	 * - Save it as sendnotifications.php and at the command line, run 
	 *        php sendnotifications.php
	 *
	 * - Upload it to a web host and load mywebhost.com/sendnotifications.php 
	 *   in a web browser.
	 * - Download a local server like WAMP, MAMP or XAMPP. Point the web root 
	 *   directory to the folder containing this file, and load 
	 *   localhost:8888/sendnotifications.php in a web browser.
	 */
	// Include the PHP Twilio library. You need to download the library from 
	// twilio.com/docs/libraries, and move it into the folder containing this 
	// file.
	require "twilio/Services/Twilio.php";
	
	// Set our AccountSid and AuthToken from twilio.com/user/account
	$AccountSid = "AC83804311786402cadc96419ec1518ce1";
	$AuthToken = "234de42b42e37785e044efeaf160d6a8";

	// Instantiate a new Twilio Rest Client
	$client = new Services_Twilio($AccountSid, $AuthToken);

	/* Your Twilio Number or Outgoing Caller ID */
	$from = '9793536660';

	// make an associative array of server admins. Feel free to change/add your 
	// own phone number and name here.
	$people = array(
		"5202611658" => "Vincent",
		"6266757884" => "BJ",
		"8056378152" => "SJ",
		"9797391129" => "Grant",
		"9799974735" => "SV",
		"9797039118" => "MR",
		"8322025716" => "Peter",
	);

	// Iterate over all admins in the $people array. $to is the phone number, 
	// $name is the user's name
	foreach ($people as $to => $name) {
		// Send a new outgoing SMS */
		//$body = "Yo! Check this out. Message by light-up-me. We are # 1";
		//$client->account->sms_messages->create($from, $to, $body);
		//echo "Sent message to $name";
	}
	
	$call = $client->account->calls->create("9793536660", "9794360403", "http://vivianpeter.com/SP_APP/8322025716.xml", array());
	echo $call->sid;
	
?>
