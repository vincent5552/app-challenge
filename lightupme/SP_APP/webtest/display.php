
	<script>
	$(function() {
		$( "#tabs" ).tabs();
		$( "#protabs" ).tabs();
		$( ".datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
		var arr=new Array();
		$('#sbsed').click(function(){
			arr[0]=<?echo $_POST['data'];?>;
			arr[1]=document.getElementById('sed').value;
			arr[2]=document.getElementById('note').value;
			arr[3]=document.getElementById('clinic').value;
			
			$.ajax({
				url: 'schedule.php',
				type: "POST",
				data:  {data: arr},
				success : function(data) {
					alert(data);
				}
			});
		});
	});
	</script>
<div id="tabs">
	<ul>
	<li><a href="#tabs-1">Profile</a></li>
	<li><a href="#tabs-2">Schedule</a></li>
	<li><a href="#tabs-3">Connect</a></li>
	<li><a href="#tabs-4">Safety Plan</a></li>
	<li><a href="#tabs-5">Evaluation</a></li>
	</ul>
					
	<div id="tabs-1">
		<div id="protabs">
		<ul>
			<li><a href="#protabs-1">Display</a></li>
			<li><a href="#protabs-2">Edit</a></li>
			<li><a href="#protabs-3">Remove</a></li>
		</ul>
		<div id="protabs-1">
		<h3>Display Profile</h3>
		<table border='1' width='100%'>
		<?
			$http = "http://vivianpeter.com/SP_APP/Server_API1.php?action=DisplayPatientProfilebylightupmeid&lightupmeid=".$_POST['data'];
			$g = file_get_contents($http);
			$dis = json_decode($g,true);
			$name;
			$email;
			$phone;
			$dob;
			$me;
			$job;
			$rank;
			$doctor;
			foreach($dis['Result']['1'] as $key => $value) {
				if($key == 'Name') {
					$name = $value;
				}
				else if($key == 'Email') {
					$email = $value;
				}
				else if($key == 'Phone') {
					$phone = $value;
				}
				else if($key == 'Gender') {
					$gender = $value;
				}
				else if($key == 'LightUpMeID') {
					$me = $value;
				}
				else if($key == 'DOB') {
					$dob = $value;
				}
				else if($key == 'Occupation') {
					$job = $value;
				}
				else if($key == 'Rank') {
					$rank = $value;
				}
				else if($key == 'DoctorID') {
					$doctor = $value;
				}
				
				if($key == 'Gender') {
					if($value == '1') {
						echo "<tr><th width='30%'><b>&nbsp;&nbsp;".$key.": </b></th><td>&nbsp;Male</td></tr>";
					}
					else if($value == '2') {
						echo "<tr><th width='30%'><b>&nbsp;&nbsp;".$key.": </b></th><td>&nbsp;Female</td></tr>";
					}
					else {
						echo "<tr><th width='30%'><b>&nbsp;&nbsp;".$key.": </b></th><td>&nbsp;Other</td></tr>";
					}
				}
				else if($key == 'Occupation') {
					if($value == '1') {
						echo "<tr><th width='30%'><b>&nbsp;&nbsp;".$key.": </b></th><td>&nbsp;Unemployed</td></tr>";
					}
					else if($value == '2') {
						echo "<tr><th width='30%'><b>&nbsp;&nbsp;".$key.": </b></th><td>&nbsp;Student</td></tr>";
					}
				}
				else if($key == 'Phone') {
					echo "<tr><th width='30%'><b>&nbsp;&nbsp;".$key.": </b></th><td>&nbsp;".$value."</td></tr>";
				}
				else  {
					echo "<tr><th width='30%'><b>&nbsp;&nbsp;".$key.": </b></th><td>&nbsp;".$value."</td></tr>";
				}
			}
		?>
		</table>
		</div>
		<div id="protabs-2">
			<h3>Edit Profile</h3>
			<form name="editform" method="post" action="dashboard.php" >
			<table width='100%'>
			<tr><th width='30%'><b>&nbsp;&nbsp;Name: </b></th><td><input type='text' name='Name' id='Name' value='<?echo $name;?>'></td>
			<tr><th width='30%'><b>&nbsp;&nbsp;Email: </b></th><td><input type='text' name='Email' id='Email' value='<?echo $email;?>'></td>
			<tr><th width='30%'><b>&nbsp;&nbsp;Phone: </b></th><td><input type='text' name='Phone' id='Phone' value='<?echo $phone;?>'></td>
			<tr><th width='30%'><b>&nbsp;&nbsp;Gender: </b></th><td><select name='Gender' id='Gender'>
			<?
			if($gender == '1') {
				echo "<option value='1' selected>Male</option><option value='2'>Female</option><option value='3'>Other</option>";
			}
			else if($gender == '2') {
				echo "<option value='1'>Male</option><option value='2' selected>Female</option><option value='3'>Other</option>";
			}
			else {
				echo "<option value='1'>Male</option><option value='2'>Female</option><option value='3' selected>Other</option>";
			}
			?>
			</select></td>
			<tr><th width='30%'><b>&nbsp;&nbsp;DOB: </b></th><td><input type='text' name='pdob' id='pdob' class='datepicker' value='<?echo $dob;?>' /></td>
			<tr><th width='30%'><b>&nbsp;&nbsp;Occupation: </b></th><td><select name='Occupation' id='Occupation'>
			<?
			if($job == '1') {
				echo "<option value='1' selected>Unemployed</option><option value='2'>Student</option>";
			}
			else if($job == '2') {
				echo "<option value='1'>Unemployed</option><option value='2' selected>Student</option>";
			}
			?>
			</select></td>
			<tr><th width='30%'><b>&nbsp;&nbsp;Rank: </b></th><td><select name='Rank' id='Rank'>
			<?
			if($rank == '1') {
				echo "<option value='1' selected>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option>";
			}
			else if($rank == '2') {
				echo "<option value='1'>1</option><option value='2' selected>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option>";
			}
			else if($rank == '3') {
				echo "<option value='1'>1</option><option value='2'>2</option><option value='3' selected>3</option><option value='4'>4</option><option value='5'>5</option>";
			}
			else if($rank == '4') {
				echo "<option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4' selected>4</option><option value='5'>5</option>";
			}
			else if($rank == '5') {
				echo "<option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5' selected>5</option>";
			}
			?>
			</select></td>
			<tr><td></td><td><input type="submit" name="edit" value="Edit"></td></td></tr>
		</table>
		<input type='hidden' name='LID' id='LID' value='<? echo $_POST['data'];?>'>
		</form>
		</div>
		<div id="protabs-3">
			<h3>Remove Profile</h3>
			<form name="removeform" method="post" action="dashboard.php" >
			<input type='hidden' name='LID' id='LID' value='<? echo $_POST['data'];?>'>
			<input type="submit" name="remove" value="Remove">
			</form>
		</div>
	</div>
	</div>
	<div id="tabs-2">
		<h3>Schedule</h3>
		<h4><label>Make Schedule</label></h4>
		
			<table>
				<tr><td>Date: </td><td><input type="text" name='sed' id='sed' class="datepicker" /></td></tr>
				<tr><td>Note: </td><td><textarea id='note' name='note'/></td></tr>
				<tr><td>Clinic Name: </td><td><textarea id='clinic' name='clinic'/></td></tr>
				<tr><td></td><td><button id='sbsed'>Submit</button></td></tr>
			</table>
		
	</div>
	<div id="tabs-3">
		<h3>Connect</h3>
		<h4><label>CallTTS</label></h4>
		<table><tr><td><label>Message: </label></td><td><textarea id='ttd' name='ttd'/></td><td><input type='submit' name='sttd' value='Send'/></td></tr></table>
		<h4><label>SendSMS</label></h4>
		<table><tr><td><label>Message: </label></td><td><textarea id='sms' name='sms'/></td><td><input type='submit' name='ssms' value='Send'/></td></tr></table>
		<h4><label>CallTTS Later</label></h4>
		<table><tr><td><label>Date</label></td><td><input type="text" name='dttd' id='dttd' class="datepicker" /></td></tr><tr><td><label>Message: </label></td><td><textarea id='lttd' name='lttd'/></td><td><input type='submit' name='slttd' value='Send'/></td></tr></table>
		<h4><label>Send SMS Later</label></h4>
		<table><tr><td><label>Date</label></td><td><input type="text" name='dsms' id='dsms' class="datepicker" /></td></tr><tr><td><label>Message: </label></td><td><textarea id='lsms' name='lsms'/></td><td><input type='submit' name='slsms' value='Send'/></td></tr></table>
	</div>
	<div id="tabs-4">
		<h3>Safety Plan</h3>
	</div>
	<div id="tabs-5">
		<h3>Evaluation</h3>
	</div>
</div>
<?

if(isset($_POST['sttd'])) {
	$http = "http://vivianpeter.com/SP_APP/Server_API1.php?action=CallTTS&phone=".$phone."&message=".$_POST['ttd']."&language=en";
	$g = file_get_contents($http);
	$ttd = json_decode($g,true);
}
if(isset($_POST['ssms'])) {
	$http = "http://vivianpeter.com/SP_APP/Server_API1.php?action=SendSMS&phone=".$phone."&message=".$_POST['sms'];
	$g = file_get_contents($http);
	$sms = json_decode($g,true);
}
if(isset($_POST['slttd'])) {
	//http://vivianpeter.com/SP_APP/Server_API2.php?action=CallTTSLater&phone=8322025716&message=time%20to%20eat%20baby&targetsentdatetime=2013-07-18%2018:15
	$http = "http://vivianpeter.com/SP_APP/Server_API1.php?action=CallTTSLater&phone=".$phone."&message=".$_POST['lttd']."&targetsentdatetime=".$_POST['dttd']." 12:00&language=en";
	$g = file_get_contents($http);
	$slttd = json_decode($g,true);
}
if(isset($_POST['slsms'])) {
	//http://vivianpeter.com/SP_APP/Server_API2.php?action=SendSMSLater&phone=9792090603&message=this%20is%20not%20just%20a%20test,%20see%20ya%20check%20this%20out%20:)&targetsenddatetime=2013-07-19%2010:15
	$http = "http://vivianpeter.com/SP_APP/Server_API1.php?action=SendSMSLater&phone=".$phone."&message=".$_POST['lsms']."&targetsenddatetime=".$_POST['dsms']." 12:00";
	$g = file_get_contents($http);
	$slsms = json_decode($g,true);
}
?>