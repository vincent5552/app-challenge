<?
session_start();
$_SESSION['did'] = '3';
$_SESSION['page'] = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
	<title>LightUp - Dashboard</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script>
	$(function() {
		$( "#accordion" ).accordion({
		  heightStyle: "content"
		});
		$( "#tabs" ).tabs();
		$( "#datepicker" ).datepicker({ dateFormat: "yy-mm-dd" });
		$( "#dialog" ).dialog({
			autoOpen: false,
			width: 400
		});
	 
		$( "#opener" ).click(function() {
		  $( "#dialog" ).dialog( "open" );
		});
		$('#highlight').click(function(){ 
			$('.pname').css('color', 'black');
		});
		$('.pname').click(function(){
			//$( "#accordion" ).accordion( "option", "active", 1 );
			$('.pname').css('color', 'black');
			$(this).css('color', 'red');
			var PID = $(this).attr('id');
			//alert(PID);
			$.ajax({
				url: 'display.php',
				type: "POST",
				data:  {data: PID},
				success : function(data) {
					$("#ppro").html(data);
				}
			});
			$( "#accordion" ).accordion( "option", "active", 1 );
		});
	
	$('#home').mouseover(function(){
		$('#home').attr("src", "web/home2.png");
		$('#home').mousedown(function(){$('#home').attr("src", "web/home3.png");});
		$('#home').mouseup(function(){$('#home').attr("src", "web/home2.png");});
	});
	$('#home').mouseout(function(){
		$('#home').attr("src", "web/home.png");
	});
	
	$('#dash').mouseover(function(){
		$('#dash').attr("src", "web/dashboard2.png");
		$('#dash').mousedown(function(){$('#dash').attr("src", "web/dashboard3.png");});
		$('#dash').mouseup(function(){$('#dash').attr("src", "web/dashboard2.png");});
	});
	$('#dash').mouseout(function(){
		$('#dash').attr("src", "web/dashboard.png");
	});
	
	$('#resource').mouseover(function(){
		$('#resource').attr("src", "web/resource2.png");
		$('#resource').mousedown(function(){$('#resource').attr("src", "web/resource3.png");});
		$('#resource').mouseup(function(){$('#resource').attr("src", "web/resource2.png");});
	});
	$('#resource').mouseout(function(){
		$('#resource').attr("src", "web/resource.png");
	});
	
	$('#help').mouseover(function(){
		$('#help').attr("src", "web/how2.png");
		$('#help').mousedown(function(){$('#help').attr("src", "web/how3.png");});
		$('#help').mouseup(function(){$('#help').attr("src", "web/how2.png");});
	});
	$('#help').mouseout(function(){
		$('#help').attr("src", "web/how.png");
	});
	
	$('#setting').mouseover(function(){
		$('#setting').attr("src", "web/setting2.png");
		$('#setting').mousedown(function(){$('#setting').attr("src", "web/setting3.png");});
		$('#setting').mouseup(function(){$('#setting').attr("src", "web/setting2.png");});
	});
	$('#setting').mouseout(function(){
		$('#setting').attr("src", "web/setting.png");
	});
	
	$('#login').mouseover(function(){
		$('#login').attr("src", "web/login2.png");
		$('#login').mousedown(function(){$('#login').attr("src", "web/login3.png");});
		$('#login').mouseup(function(){$('#login').attr("src", "web/login2.png");});
	});
	$('#login').mouseout(function(){
		$('#login').attr("src", "web/login.png");
	});
	
	});
	</script>
</head>
<style>
th {
    background-color: #81BEF7;
}
</style>
<body bgColor="#CCCCCC">
<center>

<? include 'header.html'; ?>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#F2F2F2" >
<tr height="90"><td>
	<table border="5"><tr><td align="left" valign="top" width="140">
		<table id="plist"><tr>
			<td>
				<div id="dialog" title="Create Profile">
					<form name="form" method="post" action="dashboard.php" >
					<table>
						<tr><th>Name: </th><td><input id='fname' name='fname' type="text"></td></tr>
						<tr><th>Email: </th><td><input id='email' name='email' type="text"></td></tr>
						<tr><th>Gender: </th><td><select name='gender' id='gender'><option value='1'>Male</option><option value='2'>Female</option><option value='3'>Other</option></select></td></tr>
						<tr><th>Phone: </th><td><input id='phone' name='phone' type="text"></td></tr>
						<tr><th>Birth: </th><td><input type="text" name='dob' id="datepicker" /></td></tr>
						<tr><th>Occupation: </th><td><select name='job' id='job'><option value='1'>Unemployed</option><option value='2'>Student</option></select></td></tr>
						<tr><th>Rank: </th><td><select name='rank' id='rank'><option value='1'>1</option>
																			<option value='2'>2</option>
																			<option value='3'>3</option>
																			<option value='4'>4</option>
																			<option value='5'>5</option></select>
						</td></tr>
						<tr><th>Lighupme ID: </th><td><input id='lid' name='lid' type="text"></td></tr>
						<tr><td></td><td><input type="submit" name="submit" value="Create Account"></td></tr>
					</table>
					</form>
				</div>
				&nbsp;<button id="opener">Add Patient</button>
			</td>
		</tr>
		<?
		$http = "http://vivianpeter.com/SP_APP/Server_API1.php?action=PatientListbyDoctorID&DoctorID=3";
		$g = file_get_contents($http);
		$list = json_decode($g,true);
		foreach($list['Result'] as $key => $value) {
			echo "<tr><td class='pname' id='".$value['LightUpMeID']."'><label><b>&nbsp;&nbsp;".$value['Name'];
			echo "</b></label></td></tr>";
		}
		?>
		</table>
	</td>
	<td width="860">
		<div id="accordion">
			<h3 id='highlight'>Hightlight</h3>
			<div></div>
			<h3>Infomation</h3>
			<div>
				<div id="ppro"></div>
			</div>
		</div>
	</td></tr></table>

</td></tr>
</table>

<iframe src="footer.html" width="1000" height="75" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>
<?
if(isset($_POST['submit'])) 
{
	//print_r($_POST);
	//http://vivianpeter.com/SP_APP/Server_API1.php?action=AddPatientbyPro&firstname=myfirst&lastname=mylast&email=aaa@yahoo.com&phone=1111111111&gender=1&DOB=2005-05-15&occupation=1&rank=5&doctorid=101
	// ERROR20 - Information is not completed, please check
	// ERROR21 - SQL ERROR
	// Create Okay - �System Random Generate Number�
	
	
	$http = "http://vivianpeter.com/SP_APP/Server_API1.php?action=AddPatientbyPro&name=".$_POST['fname']."&email=".$_POST['email']."&phone=".$_POST['phone']."&gender=".$_POST['gender']."&DOB=".$_POST['dob']."&occupation=".$_POST['job']."&rank=".$_POST['rank']."&doctorid=".$_SESSION['did']; 
	$g = file_get_contents($http);
	$sym = json_decode($g,true);
	// echo "<pre>";
	// print_r($sym['Result']);
	// echo "</pre>";
	foreach($sym['Result'] as $key => $value) {
		if($value == 'ERROR20 - Information is not completed, please check') {
			echo "<script>alert('Information is not completed!');</script>;";
		}
		else if($value == 'ERROR21 - SQL ERROR') {
			echo "<script>alert('SQL ERROR!');</script>;";
		}
		else {
			//echo "<script>alert('Success!');</script>;";
			header("Location: dashboard.php");
		}
	}
}

if(isset($_POST['edit'])) {
	$http = "http://vivianpeter.com/SP_APP/Server_API3.php?action=UpdatePatientProfile&lightupmeid=".$_POST['LID']."&Name=".$_POST['Name']."&DOB=".$_POST['pdob']."&Email=".$_POST['Email']."&Rank=".$_POST['Rank']."&Phone=".$_POST['Phone']."&Gender=".$_POST['Gender']."&Occupation=".$_POST['Occupation'];
	$g = file_get_contents($http);
	$edit = json_decode($g,true);
	header("Location: dashboard.php");
	//echo $http;
}
if(isset($_POST['remove'])) {
	$http = "http://vivianpeter.com/SP_APP/Server_API3.php?action=UpdatePatientProfile&lightupmeid=".$_POST['LID']."&Active=0";
	// $g = file_get_contents($http);
	// $edit = json_decode($g,true);
	// header("Location: dashboard.php");
	echo $http;
}
?>