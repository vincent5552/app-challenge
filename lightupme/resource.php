<?
function convert_state($name, $to='abbrev') {
	$states = array(
	array('name'=>'Alabama', 'abbrev'=>'AL'),
	array('name'=>'Alaska', 'abbrev'=>'AK'),
	array('name'=>'Arizona', 'abbrev'=>'AZ'),
	array('name'=>'Arkansas', 'abbrev'=>'AR'),
	array('name'=>'California', 'abbrev'=>'CA'),
	array('name'=>'Colorado', 'abbrev'=>'CO'),
	array('name'=>'Connecticut', 'abbrev'=>'CT'),
	array('name'=>'Delaware', 'abbrev'=>'DE'),
	array('name'=>'Florida', 'abbrev'=>'FL'),
	array('name'=>'Georgia', 'abbrev'=>'GA'),
	array('name'=>'Hawaii', 'abbrev'=>'HI'),
	array('name'=>'Idaho', 'abbrev'=>'ID'),
	array('name'=>'Illinois', 'abbrev'=>'IL'),
	array('name'=>'Indiana', 'abbrev'=>'IN'),
	array('name'=>'Iowa', 'abbrev'=>'IA'),
	array('name'=>'Kansas', 'abbrev'=>'KS'),
	array('name'=>'Kentucky', 'abbrev'=>'KY'),
	array('name'=>'Louisiana', 'abbrev'=>'LA'),
	array('name'=>'Maine', 'abbrev'=>'ME'),
	array('name'=>'Maryland', 'abbrev'=>'MD'),
	array('name'=>'Massachusetts', 'abbrev'=>'MA'),
	array('name'=>'Michigan', 'abbrev'=>'MI'),
	array('name'=>'Minnesota', 'abbrev'=>'MN'),
	array('name'=>'Mississippi', 'abbrev'=>'MS'),
	array('name'=>'Missouri', 'abbrev'=>'MO'),
	array('name'=>'Montana', 'abbrev'=>'MT'),
	array('name'=>'Nebraska', 'abbrev'=>'NE'),
	array('name'=>'Nevada', 'abbrev'=>'NV'),
	array('name'=>'New Hampshire', 'abbrev'=>'NH'),
	array('name'=>'New Jersey', 'abbrev'=>'NJ'),
	array('name'=>'New Mexico', 'abbrev'=>'NM'),
	array('name'=>'New York', 'abbrev'=>'NY'),
	array('name'=>'North Carolina', 'abbrev'=>'NC'),
	array('name'=>'North Dakota', 'abbrev'=>'ND'),
	array('name'=>'Ohio', 'abbrev'=>'OH'),
	array('name'=>'Oklahoma', 'abbrev'=>'OK'),
	array('name'=>'Oregon', 'abbrev'=>'OR'),
	array('name'=>'Pennsylvania', 'abbrev'=>'PA'),
	array('name'=>'Rhode Island', 'abbrev'=>'RI'),
	array('name'=>'South Carolina', 'abbrev'=>'SC'),
	array('name'=>'South Dakota', 'abbrev'=>'SD'),
	array('name'=>'Tennessee', 'abbrev'=>'TN'),
	array('name'=>'Texas', 'abbrev'=>'TX'),
	array('name'=>'Utah', 'abbrev'=>'UT'),
	array('name'=>'Vermont', 'abbrev'=>'VT'),
	array('name'=>'Virginia', 'abbrev'=>'VA'),
	array('name'=>'Washington', 'abbrev'=>'WA'),
	array('name'=>'West Virginia', 'abbrev'=>'WV'),
	array('name'=>'Wisconsin', 'abbrev'=>'WI'),
	array('name'=>'Wyoming', 'abbrev'=>'WY')
	);

	$return = false;
	foreach ($states as $state) {
		if ($to == 'name') {
			if (strtolower($state['abbrev']) == strtolower($name)){
				$return = $state['name'];
				break;
			}
		} else if ($to == 'abbrev') {
			if (strtolower($state['name']) == strtolower($name)){
				$return = strtoupper($state['abbrev']);
				break;
			}
		}
	}
	return $return;
}

$your_key = '5baaa0b862440e65dded909b2b1169e0d546ec52c8c3629e6b2cd405c7f6266e';
	$url = "http://api.ipinfodb.com/v3/ip-city/?key=$your_key&format=json&ip=".$_SERVER['REMOTE_ADDR'];
	$d = file_get_contents($url);
	$data = json_decode($d); 
	$state = convert_state($data->regionName);
	$city = $data->cityName;
?>

<!DOCTYPE html>
<html>
<head>
    <title>Treatment Locations</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
	<style>
		html, body { height: 100%; margin: 0; padding: 0; background:#CCCCCC; font-size: small; font-family: Arial; min-height:720px; }
		#container{ width:1000px; background:#F2F2F2; margin-left: auto; margin-right: auto; }
		#rightCol{ width: 300px; float: left;}
		#leftCol{ width: 680px; float: left; margin-left:20px;}
		#header {}
		#footer {clear:both;}
		#map-canvas, #map_canvas { width: 600px; height: 400px; margin-bottom: 50px;}
		.searchmethod {overflow: hidden;}
		.searchElement { margin:0 10px 20px 0px; float:left; display:inline; position:relative;}
		#address input {width:160px;}
		#city input {width:120px;}
		#state input {width:120px;}
		#zipcode input {width:120px;}
		#clear {clear: both;}	
		
	</style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <!--
    Include the maps javascript with sensor=true because this code is using a
    sensor (a GPS locator) to determine the user's location.
    See: https://developers.google.com/apis/maps/documentation/javascript/basics#SpecifyingSensor
    -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&language=en"></script>

    <script>
        var map;
        var markers = [];
        function initialize() {
            var mapOptions = {
                zoom: 4,
                center: new google.maps.LatLng(39.8282, -98.5795),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('map-canvas'),
                mapOptions);				
        }

        function addMarkers(locArray) {
            var infowindow = new google.maps.InfoWindow();
            var _marker, i;

            for (i = 0; i < locArray.length; i++) {
                _marker = new google.maps.Marker({
                    position: new google.maps.LatLng(parseFloat(locArray[i]['lat']), parseFloat(locArray[i]['lon'])),
                    icon: 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld='+ (i+1) +'|FF776B|000000',
					map: map
                });

                google.maps.event.addListener(_marker, 'click', (function (_marker, i) {
                    return function () {
						
                        infowindow.setContent((i+1).toString() + ": " + locArray[i]['name'] + "<br />" + 
						                                                locArray[i]['addr'] + "<br />" +
																		locArray[i]['city'] +", " + locArray[i]['state'] + "<br />" +
																		locArray[i]['phone']);
                        infowindow.open(map, _marker);
                    }
                })(_marker, i));

                markers.push(_marker);
            }
        }

        function setCenter(locArray) {
            if (locArray.length > 0) {
                map.setCenter(new google.maps.LatLng(parseFloat(locArray[0]['lat']), parseFloat(locArray[0]['lon'])));
                map.setZoom(7);
            }
        }

        function clearMarkers() {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <script>
        var locArray = [];

        function Location(name, phone, addr, city, state, lat, lon) {
            this.name = name;
            this.phone = phone;
            this.addr = addr;
            this.city = city;
            this.state = state;
            this.lat = lat;
            this.lon = lon;
        }


        function queryLocation(city, state, range) {
            var locationAPIUrl = "https://light-up.me/SP_APP/Server_API1.php?action=getNearbyCenter&city=" + city + "&state=" + state + "&range=" + range;
            $.getJSON(
              locationAPIUrl,
              function (data) {

                  // clear the previous results
                  locArray.length = 0;
                  $('#NearByLoc').empty();
                  // process and store new results
                  $.each(
                    data.Result,
                    function (i, loc) {
                        var name = "";
                        var phone = "";
                        var addr = "";
                        var city = "";
                        var state = "";
                        var lat = "";
                        var lon = "";

                        if(String(loc['Name2']) === "null")
                            name = loc['Name1'];
                        else
                            name = loc['Name1'] + " \ " + loc['Name2'];
                        
                        if(String(loc['Street2']) === "null")
                            addr = loc['Street1'];
                        else
                            addr = loc['Street1'] + " " + loc['Street2'];

                        phone = loc['Phone'];
                        city = loc['City'];
                        state = loc['State'];
                        
                        var _latlon = (loc['latlon']).split("\/");
                        lat = _latlon[0];
                        lon = _latlon[1];

                        var newLoc = new Location(name, phone, addr, city, state, lat, lon);
                        locArray.push(newLoc);

                        $('#NearByLoc').append('<p><b>' + (i+1).toString() + ":" + name + '</b></p>');
                        $('#NearByLoc').append('<p>' + phone + '</p>');
                        $('#NearByLoc').append('<p>' + addr + '</p>');
                        $('#NearByLoc').append('<p>' + city + ", " + state + '</p><br/>');
                    }
                  )
              }
            )
            .done(function (data) {
                clearMarkers();
                addMarkers(locArray);
                setCenter(locArray)
            });
        }

    </script>

    <script>
        $(document).ready(function () {
            
			$('#table1').css("width", "1000");
            $("#addrSearchBtn").click(function () {
                var valid0 = 0;
                var valid1 = 0;

                if ($("#cityInput").val().length == 0) {
                    $("#cityInput").css({ "border" : "2px solid red" });
                    valid0 = 0;
                }
                else {
                    $("#cityInput").css({ "border": "" });
                    valid0 = 1;
                }

                if ($("#stateSelect").val() == "NONE") {
                    $("#stateSelect").css({ "border": "2px solid red" });
                    valid1 = 0;
                    
                }
                else {
                    $("#stateSelect").css({ "border": "" });
                    valid1 = 1;
                }


                if(valid0 && valid1)
                    queryLocation($("#cityInput").val(), $("#stateSelect").val(), "50");
            });

            $("#zipcodeSearchBtn").click(function () {
                // validation
                if(isValidUSZip($("#zipcodeInput").val()))
                    queryLocation("college station", "TX", "50");
            });
			

        });

        function isValidUSZip(sZip) {
            return /^\d{5}?$/.test(sZip);
        }


		
    </script>
</head>


<body bgColor="#CCCCCC">

	<script>
	$.get('header.html', function(data) { $('#header').html(data); });
	$.get('footer.html', function(data) { $('#footer').html(data); });	
	</script>
	
<script>

</script>
	
    <div id="container">

        <!--header-->
        <div id="header"></div>

        <!-- column 1 -->
        <div id="leftCol" >
            <!-- map -->
            <div id="map-canvas"></div>

            <!-- search by city and state -->
            <div>
                <label><b>--- Search Treatment Locations by City and State:---</b></label>
            </div>
            <br />
            <div id="city" class="searchElement">
                <label>City *</label><br/>
                <input type="text" id="cityInput" />
            </div>
            <div id="state" class="searchElement">
                <label>State *</label><br/>
                <select id="stateSelect">
                    <option value="NONE">Select State</option>
                    <option value="AL">Alabama</option>
                    <option value="AK">Alaska</option>
                    <option value="AZ">Arizona</option>
                    <option value="AR">Arkansas</option>
                    <option value="CA">California</option>
                    <option value="CO">Colorado</option>
                    <option value="CT">Connecticut</option>
                    <option value="DE">Delaware</option>
                    <option value="DC">District of Columbia</option>
                    <option value="FL">Florida</option>
                    <option value="GA">Georgia</option>
                    <option value="HI">Hawaii</option>
                    <option value="ID">Idaho</option>
                    <option value="IL">Illinois</option>
                    <option value="IN">Indiana</option>
                    <option value="IA">Iowa</option>
                    <option value="KS">Kansas</option>
                    <option value="KY">Kentucky</option>
                    <option value="LA">Louisiana</option>
                    <option value="ME">Maine</option>
                    <option value="MD">Maryland</option>
                    <option value="MA">Massachusetts</option>
                    <option value="MI">Michigan</option>
                    <option value="MN">Minnesota</option>
                    <option value="MS">Mississippi</option>
                    <option value="MO">Missouri</option>
                    <option value="MT">Montana</option>
                    <option value="NE">Nebraska</option>
                    <option value="NV">Nevada</option>
                    <option value="NH">New Hampshire</option>
                    <option value="NJ">New Jersey</option>
                    <option value="NM">New Mexico</option>
                    <option value="NY">New York</option>
                    <option value="NC">North Carolina</option>
                    <option value="ND">North Dakota</option>
                    <option value="OH">Ohio</option>
                    <option value="OK">Oklahoma</option>
                    <option value="OR">Oregon</option>
                    <option value="PA">Pennsylvania</option>
                    <option value="RI">Rhode Island</option>
                    <option value="SC">South Carolina</option>
                    <option value="SD">South Dakota</option>
                    <option value="TN">Tennessee</option>
                    <option value="TX">Texas</option>
                    <option value="UT">Utah</option>
                    <option value="VT">Vermont</option>
                    <option value="VA">Virginia</option>
                    <option value="WA">Washington</option>
                    <option value="WV">West Virginia</option>
                    <option value="WI">Wisconsin</option>
                    <option value="WY">Wyoming</option>
                </select>
            </div>
            <div>
                <br />
                <button id="addrSearchBtn">Search</button>
            </div>

            <!-- reset float --->
            <div id="clear"></div>
            
            <!-- search by zipcode -->
            <!--
            <div>
                <label><b>--- Or search by ZIP CODE ---</b></label>
            </div>
            <br />
            <div id="zipcode" class="searchElement">
                <label>ZIP Code</label><br/>
                <input id="zipcodeInput" type="text" maxlength="5"/>
            </div>
            <div>
                <br />
                <button id="zipcodeSearchBtn">Search</button>
            </div>
            -->
        </div>

        <!-- column 2 -->
        <div id="rightCol">
            <div id="NearByLoc"></div>
        </div>

        <!--footer 
        <div id="footer"></div>-->
		<iframe src="footer.html" width="1000" height="85" scrolling="no" frameBorder="0" >
</iframe>
    </div>

	<script>
	queryLocation('<?=rawurlencode($city)?>','<?=rawurlencode($state)?>',30);
	// query current location
	/*
    if(navigator.geolocation) {
        var fallback = setTimeout(function() { fail('10 seconds expired'); }, 10000);
        navigator.geolocation.getCurrentPosition(
            function (pos) {
                clearTimeout(fallback);
                console.log('pos', pos);
				var point = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                new google.maps.Geocoder().geocode({'latLng': point}, function (res, status) {
                    if(status == google.maps.GeocoderStatus.OK && typeof res[0] !== 'undefined') {
                        // find city and state
						var city = "";
						var city2 = "";
						var state = "";
						var addr = res[0].address_components;
						for (var i = 0; i < addr.length; i++)
						{
							var x = addr[i];
							if (x.types[0] == 'administrative_area_level_1') {
								state = x.short_name;
							}	
							if (x.types[0] == 'administrative_area_level_3') {
								city = x.long_name;
							}
							if (x.types[0] == 'locality') {
								city2 = x.long_name;
							}							
						}
						queryLocation(city2,state,50);
						
						// find zip
						//var zip = res[0].formatted_address.match(/,\s\w{2}\s(\d{5})/);
                        //if(zip) {
                        //    alert("zip:" + zip[1]);
                        //} else {
						//	// failed to parse zipcode
						//}
                    } else {
                        // can't resolve location... 
                    }
                });
            }, function(err) {
				// can't resolve location...
            }
        );
    } 
	*/
	</script>
</body>
</html>
