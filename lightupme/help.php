<?
session_start();

$_SESSION['page'] = basename($_SERVER['PHP_SELF']);
?>
<html><head><title>LightUp- How to Help a Suicidal Person</title>
<!--<style type="text/css">ol{margin:0;padding:0}.c8{max-width:320pt;background-color:#f2f2f2;padding:0pt 0pt 0pt 0pt}.c9{list-style-type:decimal;margin:0;padding:0}.c1{font-size:12pt;font-weight:bold}.c2{color:inherit;text-decoration:inherit}.c5{padding-left:0pt;margin-left:18pt}.c4{color:#0000ff;text-decoration:underline}.c7{color:#1155cc;text-decoration:underline}.c3{font-size:12pt}.c6{height:11pt}.c10{color:#980000}.c0{direction:ltr}.title{padding-top:24pt;line-height:1.1500000000000001;text-align:left;color:#000000;font-size:36pt;font-family:"Calibri";font-weight:bold;padding-bottom:6pt}.subtitle{padding-top:18pt;line-height:1.1500000000000001;text-align:left;color:#666666;font-style:italic;font-size:24pt;font-family:"Georgia";padding-bottom:4pt}li{color:#000000;font-size:11pt;font-family:"Calibri"}p{color:#000000;font-size:11pt;margin:0;font-family:"Calibri"}h1{padding-top:24pt;line-height:1.1500000000000001;text-align:left;color:#365f91;font-size:14pt;font-family:"Cambria";font-weight:bold;padding-bottom:0pt}h2{padding-top:18pt;line-height:1.1500000000000001;text-align:left;color:#000000;font-size:18pt;font-family:"Calibri";font-weight:bold;padding-bottom:4pt}h3{padding-top:14pt;line-height:1.1500000000000001;text-align:left;color:#000000;font-size:14pt;font-family:"Calibri";font-weight:bold;padding-bottom:4pt}h4{padding-top:12pt;line-height:1.1500000000000001;text-align:left;color:#000000;font-size:12pt;font-family:"Calibri";font-weight:bold;padding-bottom:2pt}h5{padding-top:11pt;line-height:1.1500000000000001;text-align:left;color:#000000;font-size:11pt;font-family:"Calibri";font-weight:bold;padding-bottom:2pt}h6{padding-top:10pt;line-height:1.1500000000000001;text-align:left;color:#000000;font-size:10pt;font-family:"Calibri";font-weight:bold;padding-bottom:2pt}</style>-->
</head>
<style>
p
{
font-family:"Arial";
font-size:14px;
}
h1
{
font-family:"Arial";
font-size:28pt;
}
.c2
{
font-family:"Arial";
font-size:14px;
}
</style>

<script language="javascript"> 
function toggle( targetID, btnID ) {
  var ele = document.getElementById(targetID);
  var text = document.getElementById(btnID);
  if(ele.style.display == "block") {
      ele.style.display = "none";
      text.innerHTML = "(show)";
    }
  else {
      ele.style.display = "block";
      text.innerHTML = "(hide)";
  }
} 
</script>
<body bgColor="#CCCCCC" class="c8">
<center>

<? include 'header.html'; ?>
<table width="1000" cellspacing="0" cellpadding="10" border="0"  id="wrapper" bgcolor="#F2F2F2" >
<tr height="90"><td align='center'><table width='850'><tr><td>
<h1 class="c0"><span>How to Help a Suicidal Person</span></h1><p class="c6 c0"><span></span></p><p class="c0"><span>A suicidal person may not ask for help directly, but it doesn&#39;t mean that help isn&#39;t wanted. Most people who commit suicide don&#39;t want to die&mdash;they just want to stop hurting. You may not be sure what to do to help. Should you talk about suicide seriously? What if your intervention make the situation worse? No matter what, taking action is always the right choice. Here we will show you some basic principles for helping a suicidal person. </span></p><p class="c6 c0"><span></span></p>

<p class="c0"><span class="c1">Take it seriously</span>
<a id="btn1" class="c7" href="javascript:toggle('Take_it_seriously', 'btn1');">(show)</a>
</p>
<div id="Take_it_seriously" style="display: none">
<p class="c0"><span>Always take suicidal comments seriously. Almost everyone who commits or attempts suicide has given some clue or warning before committing suicide. Anyone expressing suicidal feelings needs immediate attention. Do not ignore suicide threats. Statements like &quot;you&#39;ll be sorry when I&#39;m dead&quot; or &quot;I can&#39;t see any way out,&quot; no matter how casually or jokingly it was said, may indicate serious suicidal feelings.</span></p><p class="c6 c0"><span></span></p>
</div>

<p class="c0"><span class="c1">Start by asking questions</span>
<a id="btn2" class="c7" href="javascript:toggle('Start_by_asking_questions', 'btn2');">(show)</a>
</p>

<div id="Start_by_asking_questions" style="display: none">
<p class="c0"><span>The first step is to find out whether the person is in danger of acting on suicidal feelings. Be sensitive, but ask direct questions, such as:</span></p><p class="c0 c6"><span></span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;How are you coping with what&#39;s been happening in your life?</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Do you ever feel like just giving up?</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Are you thinking about dying?</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Are you thinking about hurting yourself?</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Are you thinking about suicide?</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Have you thought about how you would do it?</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Do you know when you would do it?</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Do you have the means to do it?</span></p><p class="c6 c0"><span></span></p><p class="c0"><span>Asking about suicidal thoughts or feelings won&#39;t push someone into doing something self-destructive. In fact, offering an opportunity to talk about feelings may reduce the risk of acting on suicidal feelings.</span></p><p class="c6 c0"><span></span></p>
</div>

<p class="c0"><span class="c1">Listen and Look for warning signs</span>
<a id="btn3" class="c7" href="javascript:toggle('Listen', 'btn3');">(show)</a>
</p>

<div id="Listen" style="display: none">
<p class="c0"><span>Give the person every opportunity to unburden his/her troubles and ventilate his/her feelings. You don&#39;t need to say much and there are no magic words. If you are concerned, your voice and manner will show it. Give him relief from being alone with his pain; let him know you are glad he turned to you. Patience, sympathy, acceptance. Avoid arguments and advice giving.</span></p><p class="c0"><span>You can&#39;t always tell when a loved one or friend is considering suicide. But here are some common signs:</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Talking about suicide &mdash; for example, making statements such as &quot;I&#39;m going to kill myself,&quot; &quot;I wish I were dead&quot; or &quot;I wish I hadn&#39;t been born&quot;</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Getting the means to commit suicide, such as buying a gun or stockpiling pills</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Withdrawing from social contact and wanting to be left alone</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Having mood swings, such as being emotionally high one day and deeply discouraged the next</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Being preoccupied with death, dying or violence</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Feeling trapped or hopeless about a situation</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Increasing use of alcohol or drugs</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Changing normal routine, including eating or sleeping patterns</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Doing risky or self-destructive things, such as using drugs or driving recklessly</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Giving away belongings or getting affairs in order when there is no other logical explanation for why this is being done</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Saying goodbye to people as if they won&#39;t be seen again</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Developing personality changes or being severely anxious or agitated, particularly when experiencing some of the warning signs listed above</span></p><p class="c6 c0"><span></span></p>
</div>

<p class="c0"><span class="c1">Be willing to give and get help sooner rather than later</span>
<a id="btn4" class="c7" href="javascript:toggle('Be_willing_to_give', 'btn4');">(show)</a>
</p>

<div id="Be_willing_to_give" style="display: none">
<p class="c0"><span>Suicide prevention is not a last minute activity. All textbooks on depression say it should be reached as soon as possible. Unfortunately, suicidal people are afraid that trying to get help might bring them more pain: being told they are stupid, foolish, sinful, or manipulative; rejection; punishment; suspension from school or job; written records of their condition; or involuntary commitment. You need to do everything you can to reduce their pain, rather than increase or prolong it. Constructively involving yourself on their side as early as possible will reduce the risk of suicide.</span></p><p class="c6 c0"><span></span></p>
</div>

<p class="c0"><span class="c1">Urgent professional help, if needed</span>
<a id="btn5" class="c7" href="javascript:toggle('Urgent', 'btn5');">(show)</a>
</p>

<div id="Urgent" style="display: none">
<p class="c0"><span>If you believe someone is in danger of committing suicide or has made a suicide attempt:</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Don&#39;t leave the person alone.</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Call National Suicide Prevention Lifeline 1-800-273-TALK (8255), 911 or your local emergency number right away. Or, if you think you can do so safely, take the person to the nearest hospital emergency room yourself. </span><span class="c10">This information is also available by simply pressing the &ldquo;I Need Immediate Help&rdquo; button at the bottom of our Light Up app. </span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Try to find out if he or she is under the influence of alcohol or drugs or may have taken an overdose.</span></p><p class="c0"><span>&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tell a family member or friend what&#39;s going on right away .</span></p><p class="c0"><span>If a friend or family member talks or behaves in a way that makes you believe he or she might commit suicide, don&#39;t try to handle the situation without help &mdash; get help from a trained professional as quickly as possible. The person may need to be hospitalized until the suicidal crisis has passed.</span></p><p class="c6 c0"><span></span></p>
</div>

<p class="c0"><span class="c1">Other useful online resource </span></p>
<ol class="c9" start="1">
<li class="c5 c0"><p class="c0"><span class="c7 c3"><a class="c2" href="http://www.suicidepreventionlifeline.org/">National Suicide Prevention Lifeline</a></span></p></li>
<li class="c5 c0"><p class="c0"><span class="c7"><a class="c2" href="http://www.helpguide.org/mental/suicide_prevention.htm">Suicide Prevention (HELPGUIDE.org)</a></span></p></li>
<li class="c5 c0"><p class="c0"><span class="c7"><a class="c2" href="http://www.suicide.org/how-to-help-a-suicidal-person.html">How to Help a Suicidal Person (Suicide.org)</a></span></p></li>
<li class="c5 c0"><p class="c0"><span class="c7"><a class="c2" href="http://www.samaritanshope.org/helping-the-suicidal.html">Helping The Suicidal &nbsp;(Samaritans)</a></span></p></li>
<li class="c5 c0"><p class="c0"><span class="c7"><a class="c2" href="http://www.wikihow.com/Prevent-a-Suicide">How to Prevent a Suicide (wikiHow)</a></span></p></li>
<li class="c5 c0"><p class="c0"><span class="c7"><a class="c2" href="http://cms.bsu.edu/campuslife/counselingcenter/newsfooteritems/dealingwithstressdepressionanxietysuicideandtraumaticevents/suicide/dealingwithapotentiallysuicidalperson">Dealing with a Potentially Suicidal Person (Ball State University)</a></span><span>&nbsp;</span></p></li>
</ol>

<p class="c0"><span class="c1">Reference: &nbsp;</span></p><ol class="c9" start="1"><li class="c5 c0"><span class="c7"><a class="c2" href="http://www.suicidepreventionlifeline.org/gethelp/someone.aspx">suicidepreventionlifeline.org, Help for someone else </a></span></li><li class="c0 c5"><span class="c7"><a class="c2" href="http://www.mayoclinic.com/health/suicide/MH00058">Mayo Clinic, Suicide and suicidal thought</a></span><span>&nbsp; </span></li><li class="c5 c0"><span class="c7"><a class="c2" href="http://www.helpguide.org/mental/suicide_prevention.htm">Helpguide.org, Suicide Prevention</a></span></li><li class="c5 c0"><span class="c7"><a class="c2" href="http://www.metanoia.org/suicide/whattodo.htm">Mentanoa.org, What can I do to help someone who may be suicidal?</a></span><span>&nbsp;</span></li></ol>


</td></tr></table>
</td></tr>
</table>
<iframe src="footer.html" width="1000" height="85" scrolling="no" frameBorder="0" >
</iframe>
</center></body></html>