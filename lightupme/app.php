<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8" />
  <title>LightUp - MobileApp</title>
 <!-- <link rel="shortcut icon" href="http://LightUpMe.org/web/favicon.ico" />-->
	<link rel="stylesheet" href="js/css/redmond/jquery-ui-1.10.3.custom.css" />
	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/js/jquery-ui-1.10.3.custom.min.js"></script>	
	<script type="text/javascript">
	$(function() {

		$('#home').mouseover(function(){
			$('#home').attr("src", "web/home2.png");
			$('#home').mousedown(function(){$('#home').attr("src", "web/home3.png");});
			$('#home').mouseup(function(){$('#home').attr("src", "web/home2.png");});
		});
		$('#home').mouseout(function(){
			$('#home').attr("src", "web/home.png");
		});
		
		$('#dash').mouseover(function(){
			$('#dash').attr("src", "web/dashboard2.png");
			$('#dash').mousedown(function(){$('#dash').attr("src", "web/dashboard3.png");});
			$('#dash').mouseup(function(){$('#dash').attr("src", "web/dashboard2.png");});
		});
		$('#dash').mouseout(function(){
			$('#dash').attr("src", "web/dashboard.png");
		});
		
		$('#resource').mouseover(function(){
			$('#resource').attr("src", "web/resource2.png");
			$('#resource').mousedown(function(){$('#resource').attr("src", "web/resource3.png");});
			$('#resource').mouseup(function(){$('#resource').attr("src", "web/resource2.png");});
		});
		$('#resource').mouseout(function(){
			$('#resource').attr("src", "web/resource.png");
		});
		
		$('#help').mouseover(function(){
			$('#help').attr("src", "web/how2.png");
			$('#help').mousedown(function(){$('#help').attr("src", "web/how3.png");});
			$('#help').mouseup(function(){$('#help').attr("src", "web/how2.png");});
		});
		$('#help').mouseout(function(){
			$('#help').attr("src", "web/how.png");
		});
		
		$('#setting').mouseover(function(){
			$('#setting').attr("src", "web/setting2.png");
			$('#setting').mousedown(function(){$('#setting').attr("src", "web/setting3.png");});
			$('#setting').mouseup(function(){$('#setting').attr("src", "web/setting2.png");});
		});
		$('#setting').mouseout(function(){
			$('#setting').attr("src", "web/setting.png");
		});
		
		$('#login').mouseover(function(){
			$('#login').attr("src", "web/login2.png");
			$('#login').mousedown(function(){$('#login').attr("src", "web/login3.png");});
			$('#login').mouseup(function(){$('#login').attr("src", "web/login2.png");});
		});
		$('#login').mouseout(function(){
			$('#login').attr("src", "web/login.png");
		});
		
	});
	</script>
</head>
<body bgColor="#CCCCCC">

<center>
<? include 'header.html'; ?>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#F2F2F2" >
<tr><td height=20></td></tr>
<tr><td>

	<table border="0" width="950" cellspacing="10" cellpadding="5">
		<tr>
		<?if(!empty($_GET['type'])) {
				echo "<td colspan='2' align='center'><h2>Welcome! Your LigntUpMeID #: <b>".$_GET['type']."</b></h2></td>";
			}?>
		</tr>
		<tr>
			<td width="499" align='center'>
			<a href="https://light-up.me/download/lightup.apk" target="_blank">
			<img border="0" src="web/download_app_button.png" width="237" height="79"></a></td><td></td>
		</tr>
		
		<tr>
			<td colspan="2" align="left">
			<p>  <p><font face="Arial">The Light Up Mobile Application is currently 
			released through our website only, and is NOT yet published at 
			Google App Store. In order to install Light Up on an Android device, 
			one has to configure the Settings to allow the installation of apps 
			from &uml;unknown source&uml;. In this case, the unknown source is our app 
			and you are absolutely safe to install it. Please read either of the 
			following references to configure the settings in few seconds.<br>
			<br>
			<a href="http://developer.android.com/tools/publishing/publishing_overview.html#unknown-sources">
			http://developer.android.com/tools/publishing/publishing_overview.html#unknown-sources
			</a><br>
			(Look for User-Opt-In for Apps from Unknown Sources at the bottom of 
			the page) <br>
			<a href="http://www.androidcentral.com/allow-app-installs-unknown-sources">
			http://www.androidcentral.com/allow-app-installs-unknown-sources</a><br>
			<br>
			Once the mobile device has been properly configured, you can simply 
			click the link from the browser in your Android device. The 
			downloading and installing procedures should start shortly. Please 
			contact us if you encounter any problem in installing Light Up
			application.<br>
			<br>
			Email: <a href="mailto:info@light-up.me">info@Light-Up.me</a></font></td>
		</tr>
	</table>

</td></tr></table>
<iframe src="footer.html" width="1000" height="85" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>
