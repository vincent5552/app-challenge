<?
session_start();
//$_SESSION['did'] = 3;
if(empty($_SESSION['did'])) {
	$_SESSION['redirect'] = 1;
	header("Location: login.php");
}
$_SESSION['page'] = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>LightUp - Dashboard</title>
	<link rel="stylesheet" href="js/css/redmond/jquery-ui-1.10.3.custom.css" />
	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/js/jquery-ui-1.10.3.custom.min.js"></script>	
	<script>
	var PID;
	var url = 'highlight.php';
	function printPage()
	{
	   var html="<html>";
	   html+= document.getElementById("dialog").innerHTML;
	   html+="</html>";

	   var printWin = window.open('','','left=0,top=0,width=450,height=300,toolbar=0,scrollbars=0,status=0');
	   printWin.document.write(html);
	   printWin.document.close();
	   printWin.focus();
	   printWin.print();
	   //printWin.close();
	}
	
	$(function() {
		$.ajaxSetup({ cache: false }); 
		setInterval(function() {$("#highlight").load(url); }, 50000);
		$("#highlight").load(url);
		$( "#accordion" ).accordion({
		  heightStyle: "content"
		});
		$( "#tabs" ).tabs();
		$( ".datepicker" ).datepicker({ dateFormat: "yy-mm-dd", changeYear: true });
		$( ".datepicker" ).datepicker("option", "yearRange", "-100:+0");
		$( "#dialog" ).dialog({
			autoOpen: false,
			width: 450,
			close: function() {

			}
		});
	 
		$( "#opener" ).click(function() {
		  $( "#dialog" ).dialog( "open" );
		});
		$('#thighlight').click(function(){ 
			$('.pname').css('color', 'black');
		});
		$("#qrsum").click(function () {
			var x=document.forms["eform"]["phone"].value;
			if(x.length != 10) {
			  alert("Phone number should be 10 digits");
			  return false;
			}
			var x=document.forms["eform"]["email"].value;
			var atpos=x.indexOf("@");
			var dotpos=x.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
			{
				alert("Not a valid e-mail address");
				return false;
			}
		
			$.ajax({
				cache: false,
				type: 'POST',
				url: 'addpatient.php',
				data: $("#eform").serialize(),
				success : function(data) {
					$("#qr").html("<center><table><tr><td><img src='web/"+data+".png'></img><td><table><tr><td>LightUpMeID: <b>"+data+"</b></td></tr><tr><td>https://light-up.me/app.php?type="+data+"</td></tr></table></td></tr><tr><td><input type='button' value='Close' onClick='document.location.reload(true)'></td><td><input type='button' value='Print' onclick='printPage();'/></td></tr></table></center>");
					//$("#qr").html(data);
				},
				error: function(obj,text,error) {
				   alert("Information is not completed!");
				   //alert(text+"   "+error);
				}
			});
		});
		$('.pname').click(function(){
			//$( "#accordion" ).accordion( "option", "active", 1 );
			$('.pname').css('color', 'black');
			$(this).css('color', 'red');
			PID = $(this).attr('id');
			//alert(PID);
			$.ajax({
				url: 'display.php',
				type: "POST",
				data:  {data: PID},
				beforeSend: function() { $('#spinner').show(); },
				success : function(data) {
					$('#spinner').hide();
					$("#ppro").html(data);
				}
			});
			$( "#accordion" ).accordion( "option", "active", 1 );
		});
		$('#hinfo').click(function(){ 
			$('#'+PID).css('color', 'red');
		});

	$('#load').click(function(){ 
		var pa = document.getElementById("lid").value;
		
		$.ajax({
			url: 'loadpatient.php',
			type: "POST",
			data:  {data: pa},
			success : function(data) {
				location.reload();
			}
		});
	});

	<?
		if(!empty($_SESSION['user'])) {
		?>
			$( "#accordion" ).accordion( "option", "active", 2 );
			$("#<?echo $_SESSION['user']; ?>").click();
		<?}
		if($_SESSION['empty'] == 1) {
	?>
			$( "#dialog" ).dialog( "open" );
	<?
		}
		else if(!empty($_SESSION['array'])) {
	?>
			$( "#dialog" ).dialog( "open" );
	<?
		}
	?>
	});
	</script>
</head>
<style>
th {
    background-color: #E9E7E7;
}
.spinner {
    position: fixed;
    top: 50%;
    left: 50%;
    margin-left: -50px; /* half width of the spinner gif */
    margin-top: -50px; /* half height of the spinner gif */
    text-align:center;
    z-index:1234;
    overflow: auto;
    width: 100px; /* width of the spinner gif */
    height: 102px; /*hight of the spinner gif +2px to fix IE8 issue */
}
</style>
<body bgColor="#CCCCCC">
<center>

<? include 'header.html'; ?>
<table width="1000" cellspacing="0" cellpadding="0" border="0"  id="wrapper" bgcolor="#F2F2F2" >
<tr height="90"><td>
	<table border="0"><tr><td align="left" valign="top" width="140">
		<table id="plist"><tr>
			<td>
				<div id="dialog" title="Create Profile">
					<div id='qr'><form name='eform' id="eform" onsubmit="return validateForm()">
					<table>
						<?
						if($_SESSION['empty'] == 1) {
							unset($_SESSION['empty']);
							echo "<tr><td colspan='3'><font color='red'>Wrong LightUpMeID</font></td></tr>";
						}
						?>
						<?if(empty($_SESSION['array'])) {?>
						<tr><th>LighupmeID: </th><td><input id='lid' name='lid' type="text"></td><td><input type='button' id='load' value='Load'></td></tr>
						<tr><th>Name: </th><td><input id='fname' name='fname' type="text"></td><td></td></tr>
						<tr><th>Email: </th><td><input id='email' name='email' type="text"></td><td></td></tr>
						<tr><th>Gender: </th><td><select name='gender' id='gender'><option value='1'>Male</option><option value='2'>Female</option><option value='3'>Other</option></select></td><td></td></tr>
						<tr><th>Phone: </th><td><input id='phone' name='phone' type="text"></td><td></td></tr>
						<tr><th>Birth: </th><td><input type="text" name='dob' class="datepicker" /></td><td></td></tr>
						<tr><th>Occupation: </th><td><select name='job' id='job'><option value='1'>Professional</option><option value='2'>Retired</option><option value='3'>Government</option></select></td><td></td></tr>
						<tr><th>Rank: </th><td><select name='rank' id='rank'><option value='1'>1</option>
																			<option value='2'>2</option>
																			<option value='3'>3</option>
																			<option value='4'>4</option>
																			<option value='5'>5</option></select>
						</td><td></td></tr>
						<? } else { ?>
						<tr><th>LighupmeID: </th><td><input id='lid' name='lid' type="text" value="<?echo $_SESSION['array']['lid'];?>"></td><td><input type='button' id='load' value='Load'></td></tr>
						<tr><th>Name: </th><td><input id='fname' name='fname' type="text" value="<?echo $_SESSION['array']['name'];?>"></td><td></td></tr>
						<tr><th>Email: </th><td><input id='email' name='email' type="text" value="<?echo $_SESSION['array']['email'];?>"></td><td></td></tr>
						<tr><th>Gender: </th><td><select name='gender' id='gender'>
						<?if($_SESSION['array']['gender'] == 1) {?>
							<option value='1' selected>Male</option><option value='2'>Female</option><option value='3'>Other</option></select></td><td></td></tr>
						<?} else if($_SESSION['array']['gender'] == 2){ ?>
							<option value='1'>Male</option><option value='2' selected>Female</option><option value='3'>Other</option></select></td><td></td></tr>
						<? } else {?>
							<option value='1'>Male</option><option value='2'>Female</option><option value='3' selected>Other</option></select></td><td></td></tr>
						<? } ?>
						<tr><th>Phone: </th><td><input id='phone' name='phone' type="text" value="<?echo $_SESSION['array']['phone'];?>"></td><td></td></tr>
						<tr><th>Birth: </th><td><input type="text" name='dob' class="datepicker" value="<?echo $_SESSION['array']['dob'];?>"></td><td></td></tr>
						<tr><th>Occupation: </th><td><select name='job' id='job'><option value='1'>Professional</option><option value='2'>Retired</option><option value='3'>Government</option></select></td><td></td></tr>
						<tr><th>Rank: </th><td><select name='rank' id='rank'><option value='1'>1</option>
																			<option value='2'>2</option>
																			<option value='3'>3</option>
																			<option value='4'>4</option>
																			<option value='5'>5</option></select>
						</td><td></td></tr>
						<? unset($_SESSION['array']);}?>
						<tr><td></td><td><input type='hidden' name='LID' id='LID' value='<?echo $_SESSION['did'];?>'/><input type="button" id='qrsum' value="Create Account"></td><td></td></tr>
					</table>
					</form></div>
				</div>
				&nbsp;<button id="opener">Add Patient</button>
			</td>
		</tr>
		<?
		$http = "https://light-up.me/SP_APP/Server_API1.php?action=PatientListbyDoctorID&DoctorID=".$_SESSION['did'];
		$g = file_get_contents($http);
		$list = json_decode($g,true);
		foreach($list['Result'] as $key => $value) {
			echo "<tr><td class='pname' id='".$value['LightUpMeID']."'><label><b>&nbsp;&nbsp;".$value['Name'];
			echo "</b></label></td></tr>";
		}
		?>
		</table>
	</td>
	<td width="860" valign='top'>
		<div id="accordion">
			<h3 id='thighlight'>Hightlight</h3>
			<div>
				<div id='highlight'></div>
			</div>
			<h3 id='hinfo'>Infomation         <div id="spinner" class="spinner" style="display:none;"><img id="img-spinner" src="web/spinner.gif" alt="Loading"/></div></h3>
			<div>
				<div id="ppro"></div>
			</div>
		</div>
	</td></tr></table>

</td></tr>
</table>

<iframe src="footer.html" width="1000" height="85" scrolling="no" frameBorder="0" >
</iframe>
</center>

</body>
</html>
<?
if(isset($_POST['submit'])) 
{
	$http = "https://light-up.me/SP_APP/Server_API1.php?action=AddPatientbyPro&name=".$_POST['fname']."&email=".$_POST['email']."&phone=".$_POST['phone']."&gender=".$_POST['gender']."&DOB=".$_POST['dob']."&occupation=".$_POST['job']."&rank=".$_POST['rank']."&doctorid=".$_SESSION['did']; 
	$g = file_get_contents($http);
	$sym = json_decode($g,true);

	foreach($sym['Result'] as $key => $value) {
		if($value == 'ERROR20 - Information is not completed, please check') {
			echo "<script>alert('Information is not completed!');</script>;";
		}
		else if($value == 'ERROR21 - SQL ERROR') {
			echo "<script>alert('SQL ERROR!');</script>;";
		}
		else {
			echo "<script>var r=confirm(\"Add Patient Successfully!\");";
			echo "if(r==true){window.location.reload(); r=false;}</script>;";
		}
	}
}


?>