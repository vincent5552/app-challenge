<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LightUpMe - Home Page</title>
<link rel="icon" type="image/png" href="https://light-up.me/favicon.ico" />
<style type="text/css">
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #333;
}
body {
	background-color: #CCC;
}
</style>
</head>

<body>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><? include 'header.html'; ?></td>
  </tr>
  <tr>
    <th height="506" align="left" valign="top" bgcolor="#F2F2F2" scope="col">
	<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF">
		<tr>
			<td colspan="2" bgcolor="#F2F2F2">
			<p align="center">
			<img border="0" src="web/words.png" width="406" height="120"></p>
			<p align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center" colspan="2" bgcolor="#F2F2F2">
			<iframe width="560" height="315" src="//www.youtube.com/embed/PLV8PTRcQJ8?rel=0&amp;vq=hd720" frameborder="0" allowfullscreen></iframe></td>
		</tr>
		<tr>
			<td width="97%" colspan="2" bgcolor="#F2F2F2">
			<p style="line-height: 150%" align="center">&nbsp;</p>
			<p>&nbsp;</td>
		</tr>
		<tr><td valign='middle' align='center' bgcolor="#F2F2F2" colspan='2'><p><a href='http://suicidepreventionlifeline.org/GetHelp/LifelineChat.aspx' target="_blank"><img src='web/hotline.png' alt='National Suicide Prevention Lifeline'></a></p></td></tr>

		<tr>
			<td colspan="2" background="web/line.png" height="40">&nbsp;</td>
		</tr>
		<tr>
			<td width="39%" height="47">&nbsp;</td>
			<td width="58%" height="47">&nbsp;</td>
		</tr>
		<tr>
			<td width="97%" colspan="2" align="center">
			<p align="center">
			<img border="0" src="web/what.png" width="339" height="252"></p>
			<table border="0" width="700" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<p style="font-family: Times New Roman; font-size: 28pt; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; line-height: 24px" align="center">
					<font face="Arial Narrow">What Is Light-Up?</font></p>
					<p style="color: rgb(0, 0, 0); font-family: Times New Roman; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px; line-height: 32px" align="center">
					<font face="Arial" color="#666666" size="3">Light-up is a complete suicide prevention and follow-up care system that includes a lot of great and innovative features, likes profile management, connection system, facility and resource locator, suicide prevention lifeline integration, education, safety plan customization, suicide evaluation and social network. All with one goal - to prevent suicide, providing continuity and good quality of care.</font></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td width="97%" align="center" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td width="97%" colspan="2" align="center">
			<p align="center">
			<img border="0" src="web/patient.png" width="129" height="200"></p>
			<table border="0" width="700" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
					<p style="line-height: 24px; color: rgb(0, 0, 0); font-family: Times New Roman; font-size: 28pt; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">
					<font color="#006699" face="Arial Narrow">As a Suicidal 
					Subject</font></p>
					<p style="line-height: 32px; color: rgb(0, 0, 0); font-family: Times New Roman; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">
					<font face="Arial" color="#666666" size="3">You can use 
					Light Up mobile app to connect with your family members, 
					friends and professionals whenever you need. You can also create, view and use your safety plan, complete customized evolution that assign by the professionals to detect early warning sign.</font></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td width="97%" colspan="2" align="center" height="16">
			</td>
		</tr>
		<tr>
			<td width="97%" colspan="2" align="center" height="358">
			<p align="center">
			<img border="0" src="web/helper.png" width="129" height="200"></p>
			<table border="0" width="700" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<p style="line-height: 24px; color: rgb(0, 0, 0); font-family: Times New Roman; font-size: 28pt; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">
					<font color="#006699" face="Arial Narrow">As a Family Member 
					or a Friend</font></p>
					<p style="line-height: 32px; color: rgb(0, 0, 0); font-family: Times New Roman; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">
					<font face="Arial" color="#666666" size="3">When invited as helper by suicidal subjects, Light-Up lets you stay connected with them via phone call or by sending instant message within the app. Moreover, you can share happy memories with them by posting phrases or pictures to cheer them up. When observing abnormal behaviors, you can also create flag to notify other helpers and provide help if available.</font></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td width="97%" align="center" colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td width="97%" colspan="2" align="center">
			<p align="center">
			<img border="0" src="web/dr.png" width="225" height="179"></p>
			<table border="0" width="700" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<p style="line-height: 24px; color: rgb(0, 0, 0); font-family: Times New Roman; font-size: 28pt; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">
					<font color="#006699" face="Arial Narrow">As a Professional 
					Consultant or Doctor</font></p>
					<p style="line-height: 32px; color: rgb(0, 0, 0); font-family: Times New Roman; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">
					<font face="Arial" color="#666666" size="3">You can use the management system to manage patients, check status, schedule appointments, create and assign evaluations, setup and customize safety plans and make connection by SMS text and text to speech phone call.</font></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td width="39%">&nbsp;</td>
			<td width="58%">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" background="web/line2.png" height="40">&nbsp;</td>
		</tr>
		<tr>
			<td width="97%" colspan="2" bgcolor="#F2F2F2">
			<p style="line-height: 24px; font-family: Times New Roman; font-size: medium; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">&nbsp;</p>
			<p style="line-height: 24px; font-family: Times New Roman; font-size: 28pt; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">
			<font face="Arial Narrow">Download Light-Up App</font></p>
			<p style="line-height: 150%" align="center">&nbsp;</p>
			<p style="line-height: 150%" align="center">
			<img border="0" src="web/ios.png" width="250" height="74">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="https://light-up.me/app.php" target="_blank">
			<img border="0" src="web/android.png" width="250" height="74"></a></p>
			<p style="line-height: 150%" align="center">&nbsp;</p>
			<p>&nbsp;</td>
		</tr>
		<tr>
			<td width="97%" colspan="2" align="center" bgcolor="#F2F2F2">

			<table border="0" width="700" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<p style="line-height: 32px; color: rgb(0, 0, 0); font-family: Times New Roman; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; orphans: auto; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">
					<font face="Arial" color="#666666" size="3">"Both mobile apps are submitted and under reviewing by Apple App Store and Google play and will be available soon for public to download and use."</font></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	</th>
	<!--<td valign='top'><iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FLight-Up%2F172620336253790&amp;width=240&amp;height=590&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=true&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:240px; height:590px; background-color:#f2f2f2" allowTransparency="true"></iframe></td>-->
  </tr>
  <tr>
		<td><iframe src="footer.html" width="1000" height="85" scrolling="no" frameBorder="0" ></iframe></td>
  </tr>
</table>
</body>
</html>
