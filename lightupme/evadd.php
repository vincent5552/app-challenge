<?
session_start();

$_SESSION['page'] = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
	<title>LightUp - Resources</title>
	<link rel="stylesheet" href="js/css/redmond/jquery-ui-1.10.3.custom.css" />
	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/js/jquery-ui-1.10.3.custom.min.js"></script>	
</head>

<body>
	<h2>Add question</h2>
	<form name='addevalform' method='post' action='addeval.php'>
	<table>
		<tr><th>Question:</th></tr>
		<tr><td><input type='text' id='question' name='question'></td></tr>
		<tr><th><table><tr><td>Answer</td><td>Score</td></tr></table></th></tr>
		<tr><td><table><tr><td><input type='text' id='ans1' name='ans1'></td><td><input type='text' id='score1' name='score1'></td></tr></table></td></tr>
		<tr><td><table><tr><td><input type='text' id='ans2' name='ans2'></td><td><input type='text' id='score2' name='score2'></td></tr></table></td></tr>
		<tr><td><table><tr><td><input type='text' id='ans3' name='ans3'></td><td><input type='text' id='score3' name='score3'></td></tr></table></td></tr>
		<tr><td><table><tr><td></td><td><input type='hidden' name='DID' id='DID' value='<?echo $_SESSION['did'];?>'/><input type='submit' name='add' value='Add'/></td></tr></table></td></tr>
	</table></form>

</body>
</html>