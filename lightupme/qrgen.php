<?
	include('phpqrcode/qrlib.php');
	$ppid = $_GET['ppid'];

	$tempDir = "./web/"; 
     
    $codeContents = 'https://light-up.me/app.php?type='.$ppid; 
     
    // we need to generate filename somehow,  
    // with md5 or with database ID used to obtains $codeContents... 
    $fileName = $ppid'.png'; 
     
    $pngAbsoluteFilePath = $tempDir.$fileName; 
    $urlRelativeFilePath = "https://light-up.me/web/".$fileName; 
     
    // generating 
    if (!file_exists($pngAbsoluteFilePath)) { 
        QRcode::png($codeContents, $pngAbsoluteFilePath); 
        // echo 'File generated!'; 
        // echo '<hr />'; 
    } else { 
        echo 'File already generated! We can use this cached file to speed up site on common codes!'; 
        echo '<hr />'; 
    } 
     
    // echo 'Server PNG File: '.$pngAbsoluteFilePath; 
    // echo '<hr />'; 
     
    // displaying 
    // echo '<img src="'.$urlRelativeFilePath.'" />'; 

?>