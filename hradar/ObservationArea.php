<?php
ini_set('memory_limit', '256M');

//image RGB array
$image = imagecreatefrompng("images/1.png");
$width = imagesx($image);
$height = imagesy($image);
	
$left = $width-1;
$right = 0;
$top = $height-1;
$bottom = 0;

for ($y = 0; $y < $height; $y++) {
	for ($x = 0; $x < $width; $x++)	{
		$rgb = imagecolorat($image, $x, $y);
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
			
		if($r!=255||$g!=255||$b!=255) {
			if($x < $left)
				$left = $x;
			if($x > $right)
				$right = $x;
			if($y < $top)
				$top = $y;
			if($y > $bottom)
				$bottom = $y;
		}
	}
}

$grey = imagecolorallocate($image, 128, 128, 128);

$lines = file("positions.txt");
foreach($lines as $line_num => $line) {

	$info = explode(",", $line);
	if($info[1]=="bad")
		continue;
		
	$wiki_center_x = $info[1];
	$wiki_center_y = $info[2];
	$wiki_top = $info[3];
	$wiki_bottom = $info[4];
	$wiki_left = $info[5];
	$wiki_right = $info[6];
	
	$center_x = (int)($left + ($right - $left)/($wiki_right -$wiki_left) * ($wiki_center_x - $wiki_left));
	$center_y = (int)($top + ($bottom - $top)/($wiki_bottom -$wiki_top) * ($wiki_center_y - $wiki_top));
	//experience adjust
	$center_x +=4;
	$center_y +=1;
	imagefilledrectangle($image , $center_x-1, $center_y-1, $center_x+1, $center_y+1, $grey);
	echo trim($info[0]).",".($center_x-1).",".($center_y-1).",".($center_x+1).",".($center_y+1);
	echo "<br>";
}

imagepng($image, "images/2.png");

?>