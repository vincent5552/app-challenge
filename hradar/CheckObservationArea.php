<?php
ini_set('memory_limit', '256M');

//image RGB array
$image = imagecreatefrompng("images/1.png");
$width = imagesx($image);
$height = imagesy($image);
	
$left = $width-1;
$right = 0;
$top = $height-1;
$bottom = 0;

for ($y = 0; $y < $height; $y++) {
	for ($x = 0; $x < $width; $x++)	{
		$rgb = imagecolorat($image, $x, $y);
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
			
		if($r!=255||$g!=255||$b!=255) {
			if($x < $left)
				$left = $x;
			if($x > $right)
				$right = $x;
			if($y < $top)
				$top = $y;
			if($y > $bottom)
				$bottom = $y;
		}
	}
}

$grey = imagecolorallocate($image, 128, 128, 128);

$lines = file("TexasBoxes.txt");
foreach($lines as $line_num => $line) {

	$info = explode(",", $line);
		
	$left = $info[1];
	$top = $info[2];
	$right = $info[3];
	$bottom = $info[4];
	
	imagefilledrectangle($image , $left, $top, $right, $bottom, $grey);
	echo "<br>";
}

imagepng($image, "images/3checked.png");

?>