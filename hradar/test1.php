<?php
ini_set('memory_limit', '256M');
//image RGB array
$image = imagecreatefrompng("1.png");
$width = imagesx($image);
$height = imagesy($image);
$colors_R = array();
$colors_G = array();
$colors_B = array();

for ($y = 0; $y < $height; $y++)
{
    for ($x = 0; $x < $width; $x++)
    {
        $rgb = imagecolorat($image, $x, $y);
        $r = ($rgb >> 16) & 0xFF;
        $g = ($rgb >> 8) & 0xFF;
        $b = $rgb & 0xFF;
		
		$colors_R[$x][$y] = $r;
		$colors_G[$x][$y] = $g;
		$colors_B[$x][$y] = $b;
    } 
}

//read county sample data and cehck for flu
$load_user = file('TexasBoxes.txt');
foreach ($load_user as $line_num => $line) {
	//initial;
	$level_1=0;
	$level_2=0; 
	$level_3=0; 
	$level_4=0; 
	$level_5=0; 
	$level_6=0; 
	$level_7=0;

	$level_final="";
	$info = explode(",", $line);		
	$count=0;
	
	for ($x = $info[1]; $x <=$info[3] ; $x++){
		 for ($y = $info[2]; $y <= $info[4]; $y++){
			$count++;
			if($colors_R[$x][$y]==255 && $colors_G[$x][$y]==255 && $colors_B[$x][$y]==255)
				$level_2++;
			if($colors_R[$x][$y]==0 && $colors_G[$x][$y]==0 && $colors_B[$x][$y]==255)
				$level_3++;
			if($colors_R[$x][$y]==0 && $colors_G[$x][$y]==255 && $colors_B[$x][$y]==255)
				$level_4++;
			if($colors_R[$x][$y]==255 && $colors_G[$x][$y]==255 && $colors_B[$x][$y]==0)
				$level_5++;
			if($colors_R[$x][$y]==0 && $colors_G[$x][$y]==255 && $colors_B[$x][$y]==0)
				$level_6++;
			if($colors_R[$x][$y]==255 && $colors_G[$x][$y]==0 && $colors_B[$x][$y]==0)
				$level_7++;
			
		 }
	}
	
	
	if($level_2==$count){
		$level_final=2;
	}else if($level_3==$count){
		$level_final=3;
	}else if($level_4==$count){
		$level_final=4;
	}else if($level_5==$count){
		$level_final=5;
	}else if($level_6==$count){
		$level_final=6;
	}else if($level_7==$count){	
		$level_final=7;
	}else{
		$level_final=1;
	}
	
	echo $info[0]."-".$level_final;
	echo "<br>";
	

}

?>