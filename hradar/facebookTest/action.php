<?php
require_once("db_conn.php");


//return state(s)
//DONE
if($_GET["action"]=="getStateList"){
	$sql="select state from vivianpe_e04.TB_GEOGRAPHY group by state";
	$result_sql=mysql_query($sql);
	$result_json=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		$state=trim($row["state"]);
		$temp=array();
		if($state!="") {
			$temp["STATE"]=$state;
			array_push($result_json, $temp);
		}
	}
	$result_json=array_values($result_json);		
	$result_json_out=array("StateList"=>$result_json);		
	echo json_encode($result_json_out);
		
} else 
//return county(s) by requested State
//DONE
if($_GET["action"]=="getCounty"){
	$state =$_GET["state"];
	if(strlen($state)==2){
		$sql="select county from vivianpe_e04.TB_GEOGRAPHY where state ='".$state."' group by county";
		$result_sql=mysql_query($sql);
		$result_json=array();
		while ($row = mysql_fetch_assoc($result_sql)) {
			$county=trim($row["county"]);
			$temp=array();
			if($county!="") {
				$temp["COUNTY"]=$county;
				array_push($result_json, $temp);
			}
		}
		$result_json=array_values($result_json);		
		$result_json_out=array($state=>$result_json);		
		echo json_encode($result_json_out);
	}else{
		echo "ERROR - parameter 1 length not correct";
	}
	

} else
//return symptom(s), symptomID(s)
//DONE
if ($_GET["action"]=="getSymptomsList") {
	$sql="select Symptom, SymptomID from vivianpe_e04.TB_SYMPTOM where Active = 1";
	$result_sql=mysql_query($sql);
	$result_json=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		$Symptom=trim($row["Symptom"]);
		$SymptomID=trim($row["SymptomID"]);
		$temp=array();
		if($Symptom!=""&&$SymptomID!="") {
			$temp["ID"] = $SymptomID;
			$temp["SYMPTOM"] = $Symptom;
			array_push($result_json, $temp);
		}
	}
	$result_json=array_values($result_json);
	$result_json_out=array("SymptomList"=>$result_json);
	echo json_encode($result_json_out);

} else
//return diseases id(s), diseases(s), description(s) by requested symptom id(s)
//DONE
if ($_GET["action"]=="getDiseasesBySymptoms") {
	$symptoms =$_GET["symptoms"];
	$symptomIDs = split(',', $symptoms);
	$sql="select id, diseases, description, symptom from vivianpe_e04.TB_DISEASES where active = 1";
	$result_sql=mysql_query($sql);
	$result_json=array();
	
	while ($row = mysql_fetch_assoc($result_sql)) {
		$id=trim($row["id"]);
		$diseases=trim($row["diseases"]);
		$description=trim($row["description"]);
		$symptom=split('/', trim($row["symptom"]));
		//$good=false;
		$cnt = 0;
		if($diseases!=""&&$id!=""&&$description!="") {
			foreach($symptomIDs as $requestID) {
				foreach($symptom as $DataID) {
					if($requestID==$DataID) {
						$cnt += 1;
					}
				}
			}
			$precision = (double)$cnt/sizeof($symptom);
			$recall = (double)$cnt/sizeof($symptomIDs);
			$fmeasure = 0;
			if($precision!=0||$recall!=0)
				$fmeasure = 2 * $precision * $recall / ($precision + $recall);
			
			$temp = array();
			$temp["ID"] = $id;
			$temp["DISEASE"] = $diseases;
			$temp["DESC"] = $description;
			$temp["FMEASURE"] = $fmeasure;
			array_push($result_json, $temp);
		}
	}

	$length = sizeof($result_json);
	for($i = 0; $i < $length; $i++) {
		$max_index = $i;
		for($j = $i + 1; $j < $length; $j++) {
			if($result_json[$j]["FMEASURE"] > $result_json[$max_index]["FMEASURE"]) {
				$max_index = $j;
			}
		}
		$temp = $result_json[$max_index];
		$result_json[$max_index] = $result_json[$i];
		$result_json[$i] = $temp;
	}
	
	$result_json=array("DiseaseList"=>$result_json);
	echo json_encode($result_json);

} else
//return diseases id(s), diseases(s), description(s)
//DONE
if ($_GET["action"]=="getDiseasesList") {
	$sql="select id, diseases, description from vivianpe_e04.TB_DISEASES where active = 1 Order By id asc";
	$result_sql=mysql_query($sql);
	$result_json=array();
	
	if($_GET["description"]=="Y"){
	
		while ($row = mysql_fetch_assoc($result_sql)) {
			$id=trim($row["id"]);
			$diseases=trim($row["diseases"]);
			$description=trim($row["description"]);
			if($diseases!=""&&$id!="") {
				$temp = array();
				$temp["ID"]=$id;
				$temp["DISEASE"]=$diseases;
				$temp["DESC"]=$description;
				array_push($result_json, $temp);
			}
		}
		$result_json=array("DiseaseList"=>$result_json);
		echo json_encode($result_json);
	
	}else if($_GET["description"]=="N"){
	
		while ($row = mysql_fetch_assoc($result_sql)) {
			$id=trim($row["id"]);
			$diseases=trim($row["diseases"]);
			$description=trim($row["description"]);
			if($diseases!=""&&$id!="") {
				$temp = array();
				$temp["ID"]=$id;
				$temp["DISEASE"]=$diseases;
				//$temp["DESC"]=$description;
				array_push($result_json, $temp);
			}
		}
		$result_json=array("DiseaseList"=>$result_json);
		echo json_encode($result_json);	
	}else{
		echo "ERROR - parameter 1 length not correct";
	}
	
	

} else
//return symptom id(s), symptom(s) by requested diseases id
//DONE
if ($_GET["action"]=="getSymptomsByDisease") {
	$diseases =$_GET["diseases"];
	$sql="select symptom from vivianpe_e04.TB_DISEASES where active = 1 AND id =".$diseases;
	$result_sql=mysql_query($sql);
	$result_json=array();
	
	$num_rows = mysql_num_rows($result_sql);
	if ($num_rows==0){
		echo "ERROR - parameter 1 not valid";
		break;
	}
	
	
	if($row = mysql_fetch_assoc($result_sql)) {
		$symptoms = split('/', trim($row["symptom"]));
		$max = sizeof($symptoms);
		if($max==0)
			break;

		$sql="select Symptom, SymptomID from vivianpe_e04.TB_SYMPTOM where Active = 1 AND (SymptomID=".$symptoms[0];
		for($i = 1; $i < $max; $i++) {
			$sql .= " OR SymptomID=".$symptoms[$i];
		}
		$sql .= ")";
		$result_sql=mysql_query($sql);
		while ($row = mysql_fetch_assoc($result_sql)) {
			$Symptom=trim($row["Symptom"]);
			$SymptomID=trim($row["SymptomID"]);
			if($Symptom!=""&&Symptom_ID!="") {
				$temp = array();
				$temp["ID"]=$SymptomID;
				$temp["SYMPTOM"]=$Symptom;
				array_push($result_json, $temp);
			}
		}
	}
	$result_json=array("SymptomList"=>$result_json);
	echo json_encode($result_json);

} else
//insert record into DiseaseInfo requested disease id, symptom id(s), userid, usergroupid, state, county, createtime
if ($_GET["action"]=="report") {
	$DiseasesID =$_GET["diseases"];
	$SymptomIDs =$_GET["symptoms"];
	$UserID = $_GET["userid"];
	$UserGroupID = $_GET["usergroupid"];
	$State = $_GET["state"];
	$County = $_GET["county"];
	$CreateTime = $_GET["createtime"];
	$sql = "insert into vivianpe_e04.TB_DISEASEINFO (DiseaseID, SymptomID, UserID, UserGroupID, State, County, CreateTime, Active) values (";
	$sql .= "'".$DiseasesID."',";
	$sql .= "'".$SymptomIDs."',";
	$sql .= "'".$UserID."',";
	$sql .= $UserGroupID.",";
	$sql .= "'".$State."',";
	$sql .= "'".$County."',";
	$sql .="UTC_TIMESTAMP(),";
	$sql .= "1)";
	
	$con=mysqli_connect($dbhost, $dbuser, $dbpasswd, $db) or die("Unable to connect to SQL server for insertion");
	if (!mysqli_query($con,$sql)) {
		die('Error: ' . mysqli_error());
	} else {
		$temp = array();
		$temp["result"]="1 record added";
		$result_json=array();
		array_push($result_json, $temp);
		$result_json=array("Report"=>$result_json);
		echo json_encode($result_json);
	}
	//echo "1 record added";	
	mysqli_close($con);
//
//return symptom id(s), symptom(s) by requested user id, user group id
//the range of query is not decided yet. now it is 7 days before and after the query time
//done
} elseif ($_GET["action"]=="getHistory") {
	$UserID = $_GET["userid"];
	$UserGroupID = $_GET["usergroupid"];
	$sql = "select Symptom, SymptomID FROM vivianpe_e04.TB_SYMPTOM WHERE Active=1";
	$result_sql=mysql_query($sql);
	$Symptoms=array();
	$SymptomIDs=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		array_push($Symptoms, trim($row["Symptom"]));
		array_push($SymptomIDs, trim($row["SymptomID"]));
	}
	$max = sizeof($SymptomIDs);

	$sql = "select State, County, TBD.id, TBD.diseases, TBD.description, SymptomID, CreateTime from vivianpe_e04.TB_DISEASEINFO as TBINFO ";
	$sql .= "INNER JOIN vivianpe_e04.TB_DISEASES as TBD ON TBINFO.DiseaseID = TBD.id WHERE TBINFO.Active=1";
	$sql .=" AND UserID='".$UserID."' AND UserGroupID=".$UserGroupID;
	$sql .=" ORDER BY CreateTime DESC";
	//$sql .= " AND (TBINFO.CreateTime >= DATE_ADD(STR_TO_DATE('".$QueryTime."', '%Y/%m/%d %H:%i:%s'), INTERVAL -7 DAY) AND TBINFO.CreateTime <= DATE_ADD(STR_TO_DATE('".$QueryTime."', '%Y/%m/%d %H:%i:%s'), INTERVAL 7 DAY))";

	$result_sql=mysql_query($sql);
	$result_json=array();

	while ($row = mysql_fetch_assoc($result_sql)) {
		$State=trim($row["State"]);
		$County=trim($row["County"]);
		$diseaseID=trim($row["id"]);
		$disease=trim($row["diseases"]);
		$description=trim($row["description"]);
		$hist_symptomID=trim($row["SymptomID"]);
		$createTime=trim(trim($row["CreateTime"]));
		$symptomID=split(',', $hist_symptomID);
		if($State!=""&&$County!="") {
			$temp = array();
			$temp["State"] = $State;
			$temp["County"] = $County;
			$temp["DiseaseID"] = $diseaseID;
			$temp["Disease"] = $disease;
			$temp["DESC"]= $description;
			$temp["CreateTime"] = $createTime;
			//$temp["SymptomIDs"] = $hist_symptomID;
			$sympArray = array();
			foreach($symptomID as $hist_sympID) {
				for($i = 0; $i < $max; $i++) {
					if($hist_sympID==$SymptomIDs[$i]) {
						$sympRecord = array();
						$sympRecord["SymptomID"] = $SymptomIDs[$i];
						$sympRecord["Symptom"] = $Symptoms[$i];
						array_push($sympArray, $sympRecord);
					}
				}
			}
			$temp["Symptoms"] = $sympArray;
			array_push($result_json, $temp);
		}
	}
	$result_json=array("HistoryList"=>$result_json);
	echo json_encode($result_json);
//
} elseif ($_GET["action"]=="getNotification") {
	//time interval(day)
	//$QueryTime = $_GET["time"];
	$QueryInterval = 14;
	$result_json=getNearByDiseases($QueryInterval);
	$result_json=array("NotificationList"=>$result_json);
	echo json_encode($result_json);
	

//
} elseif ($_GET["action"]=="getNearByDiseases") {
	//time interval(day)
	//$QueryTime = $_GET["time"];
	$QueryInterval = 14;
	$result_json=getNearByDiseases($QueryInterval);
	$result_json=array("NearByDiseaseList"=>$result_json);
	echo json_encode($result_json);
	
} elseif ($_GET["action"]=="getNearBySymptoms") {
	//time interval(day)
	//$QueryTime = $_GET["time"];
	$QueryInterval = 14;
	$result_json=getNearBySymptoms($QueryInterval);
	$result_json=array("NearBySymptomList"=>$result_json);
	echo json_encode($result_json);	
//
} elseif ($_GET["action"]=="getCountyByFuzzyName") {
	$fuzzy=$_GET["fuzzy"];
	$state=$_GET["state"];
	$sql="select county from vivianpe_e04.TB_GEOGRAPHY where county Like '".$fuzzy."%' AND state='".$state."' group by county";
	$result_sql=mysql_query($sql);
	$result_json=array();
	if ($row = mysql_fetch_assoc($result_sql)) {
		$county=trim($row["county"]);
		array_push($result_json, $county);
	}
	$result_json=array("CountyName"=>$result_json);
	echo json_encode($result_json);
	
}

function distance($lat1, $lng1, $lat2, $lng2, $miles = true) {
	$pi80 = M_PI / 180;
	$lat1 *= $pi80;
	$lng1 *= $pi80;
	$lat2 *= $pi80;
	$lng2 *= $pi80;

	$r = 6372.797; 
	$dlat = $lat2 - $lat1;
	$dlng = $lng2 - $lng1;
	$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
	$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
	$km = $r * $c;

	return $km * 0.621371192;
}
	
function getNearByDiseases($QueryInterval) {
	$county = $_GET["county"];
	$state = $_GET["state"];
	$range = $_GET["range"];
	

	//get latitude and logitude of the given county
	$sql="SELECT  Latitude, Longitude FROM vivianpe_e04.TB_ZIP where State='".$state."' and County like '%".$county."%' limit 1";
	$row = mysql_fetch_assoc(mysql_query($sql)); //Get source LAT/LON
	
	//find near by county
	$sql="SELECT distinct county, state, Latitude, Longitude FROM vivianpe_e04.TB_ZIP";
	$result_sql=mysql_query($sql);
	
	$result_state=array();
	$result_county=array();
	while ($row1 = mysql_fetch_assoc($result_sql)) {
		if(distance($row["Latitude"], $row["Longitude"], $row1["Latitude"], $row1["Longitude"]) <=$range){
			array_push($result_state, $row1["state"]);
			array_push($result_county, $row1["county"]);
		}
	}

	$self_report= array();
	$cdc_flu=array();
	$cdc_wonder=array();
	
	//self report
	$sql = "select Count(*) as Cnt, State, County, TBD.id, TBD.diseases from vivianpe_e04.TB_DISEASEINFO as TBINFO ";
	$sql .= "INNER JOIN vivianpe_e04.TB_DISEASES as TBD ON TBINFO.DiseaseID = TBD.id WHERE TBINFO.Active=1 AND DATE_ADD(TBINFO.CreateTime, INTERVAL ".$QueryInterval." DAY) > UTC_TIMESTAMP() AND TBINFO.Active=1 AND ";
	$sql .= "(1=2";

	$i = 0;
	foreach($result_state as $state) {
		$sql .= " OR (State='".$state."' AND County='".$result_county[$i]."')";
		$i +=1;
	}
	$sql .= ") Group By State, County, TBD.id";

	$result_sql=mysql_query($sql);
	$result_json=array();
	while ($row = mysql_fetch_assoc($result_sql)) {
		$State=trim($row["State"]);
		$County=trim($row["County"]);
		$diseaseID=trim($row["id"]);
		$disease=trim($row["diseases"]);
		$Cnt=trim($row["Cnt"]);
		if($State!=""&&$County!="") {
			$temp = array();
			$temp["State"] = $State;
			$temp["County"] = $County;
			$temp["DiseaseID"] = $diseaseID;
			$temp["Disease"] = $disease;
			$temp["Number"] = $Cnt;
			//$temp["Source"] = "Self Report";
			//array_push($result_json, $temp);
			array_push($self_report, $temp);
		}
	}
	$result_json["self report"]=$self_report;
	
	//$querytime = strtotime($QueryTime);
	//$year = date("Y", $querytime);
	//$week = date("W", $querytime);
	
	//CDC Flu
	$sql = "select Year, Week, TBCFWR.Level, Description, County, State from TB_CDCFluWeeklyReport as TBCFWR INNER JOIN TB_CDCFluLevel as TBCFL";
	$sql .= " ON TBCFWR.Level = TBCFL.Level WHERE (Year > Year(DATE_ADD(UTC_TIMESTAMP, INTERVAL -".$QueryInterval." DAY)) OR (Year=Year(DATE_ADD(UTC_TIMESTAMP, INTERVAL -".$QueryInterval." DAY)) AND Week>=WEEKOFYEAR(DATE_ADD(UTC_TIMESTAMP, INTERVAL -".$QueryInterval." DAY)))) AND TBCFWR.Active=1 AND ";
	$sql .= "(1=2";
	$i = 0;
	foreach($result_state as $state) {
		$sql .= " OR (State='".$state."' AND County='".$result_county[$i]."')";
		$i +=1;
	}
	$sql .= ")";

	//echo $sql."<br>";

	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
		$State=trim($row["State"]);
		$County=trim($row["County"]);
		$Level=trim($row["Level"]);
		$Description=trim($row["Description"]);
		$Year=trim($row["Year"]);
		$Week=trim($row["Week"]);
		if($State!=""&&$County!="") {
			$temp = array();
			$temp["State"] = $State;
			$temp["County"] = $County;
			$temp["Level"] = $Level;
			$temp["Description"] = $Description;
			$temp["Year"] = $Year;
			$temp["Week"] = $Week;
			//$temp["Source"] = "CDC Flu Weekly Report";
			//array_push($result_json, $temp);
			array_push($cdc_flu, $temp);
		}
	}
	$result_json["cdc flu"]=$cdc_flu;

	//CDC Wonder
	$subsql = "";
	$sql = "select OLDGPO, ANSI FROM TB_USStateAbbr WHERE ANSI NOT LIKE '%,%'";
	//echo $sql."<br>";

	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
		$ANSI = $row["ANSI"];
		$hasMeet = false;
		foreach($result_state as $state) {
			if($state==$ANSI) {
				$hasMeet = true;
				break;
			}
		}
		if($hasMeet)
			$subsql .= " OR (Location='".$row["OLDGPO"]."')";
	}
	
	$sql = "select Year, Week, Disease, Type, ANSI, Number from TB_CDCWonderWeeklyReport as CWWR";
	$sql .= " INNER JOIN TB_USStateAbbr as USSA ON CWWR.Location = USSA.OLDGPO";
	$sql .= " WHERE (Year > Year(DATE_ADD(UTC_TIMESTAMP, INTERVAL -".$QueryInterval." DAY)) OR (Year=Year(DATE_ADD(UTC_TIMESTAMP, INTERVAL -".$QueryInterval." DAY)) AND Week>=WEEKOFYEAR(DATE_ADD(UTC_TIMESTAMP, INTERVAL -".$QueryInterval." DAY)))) AND Active = 1 AND ";
	//for special condition, ugly
	$sql .= " (CWWR.Type<>'Dengue Hemorrhagic Fever' AND CWWR.Type<>'Undetermined' AND CWWR.Type<>'Probable' AND CWWR.Type<>'Age <5 years') AND ";
	$sql .= "(1=2";
	$sql .= $subsql;
	$sql .= ")";

	//echo $sql."<br>";
	
	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
		$State=trim($row["ANSI"]);
		$Number=trim($row["Number"]);
		$Disease=trim($row["Disease"]);
		$Type=trim($row["Type"]);
		$Year=trim($row["Year"]);
		$Week=trim($row["Week"]);
		if($State!=""&&$County!="") {
			$temp = array();
			$temp["State"] = $State;
			$temp["Number"] = $Number;
			$temp["Disease"] = $Disease;
			$temp["Type"] = $Type;
			$temp["Year"] = $Year;
			$temp["Week"] = $Week;
			//$temp["Source"] = "CDC Wonder Weekly Report";
			//array_push($result_json, $temp);
			array_push($cdc_wonder, $temp);
		}
	}
	$result_json["cdc wonder"]=$cdc_wonder;
	
	return $result_json;
}

function getNearBySymptoms($QueryInterval) {
	$county = $_GET["county"];
	$state = $_GET["state"];
	$range = $_GET["range"];

	$SymptomDB = array();
	$sql = "SELECT SymptomID, Symptom FROM vivianpe_e04.TB_SYMPTOM";
	$result_sql=mysql_query($sql);
	while ($row = mysql_fetch_assoc($result_sql)) {
		$SymptomDB[$row["SymptomID"]] = $row["Symptom"];
	}
	
	//get latitude and logitude of the given county
	$sql="SELECT  Latitude, Longitude FROM vivianpe_e04.TB_ZIP where State='".$state."' and County like '%".$county."%' limit 1";
	$row = mysql_fetch_assoc(mysql_query($sql)); //Get source LAT/LON
	
	//find near by county
	$sql="SELECT distinct county, state, Latitude, Longitude FROM vivianpe_e04.TB_ZIP";
	$result_sql=mysql_query($sql);
	
	$result_state=array();
	$result_county=array();
	while ($row1 = mysql_fetch_assoc($result_sql)) {
		if(distance($row["Latitude"], $row["Longitude"], $row1["Latitude"], $row1["Longitude"]) <=$range){
			array_push($result_state, $row1["state"]);
			array_push($result_county, $row1["county"]);
		}
	}

	//self report
	$sql = "select State, County, SymptomID from vivianpe_e04.TB_DISEASEINFO";
	$sql .= " WHERE Active=1 AND DATE_ADD(CreateTime, INTERVAL ".$QueryInterval." DAY) > UTC_TIMESTAMP() AND Active=1 AND ";
	$sql .= "(1=2";

	$i = 0;
	foreach($result_state as $state) {
		$sql .= " OR (State='".$state."' AND County='".$result_county[$i]."')";
		$i +=1;
	}
	$sql .= ") Order By State, County";
	
	$preState = "";
	$preCounty = "";

	$SymptomList = array();
	$States = array();
	$Counties = array();
	
	$isStart = false;
	$result_sql=mysql_query($sql);
	$result_json=array();
	
	$temp = array();

	while ($row = mysql_fetch_assoc($result_sql)) {
		$State=trim($row["State"]);
		$County=trim($row["County"]);
		$SymptomIDs = split(',', trim($row["SymptomID"]));
		if($preState!=$State||$preCounty!=$County) {
			if($isStart) {
				if(sizeof($temp) > 0) {
					array_push($SymptomList, $temp);
					array_push($States, $preState);
					array_push($Counties, $preCounty);
				}
			} else {
				$isStart = true;
			}
			$temp = array();
			$preState = $State;
			$preCounty = $County;
		}
		if(sizeof($SymptomIDs)>0) {
			foreach($SymptomIDs as $SymptomID) {
				if(array_key_exists($SymptomID, $temp)) {
					$temp[$SymptomID] += 1;
				} else {
					$temp[$SymptomID] = 1;
				}
			}
		}
	}
	if(isStart) {
		if(sizeof($temp) > 0) {
			array_push($SymptomList, $temp);
			array_push($States, $preState);
			array_push($Counties, $preCounty);
		}
	}
	
	for($i = 0; $i < sizeof($SymptomList); $i++) {
		foreach($SymptomList[$i] as $ID => $Number) {
			$temp = array();
			$temp["State"] = $States[$i];
			$temp["County"] = $Counties[$i];
			$temp["SymptomID"] = trim($ID);
			$temp["Symptom"] = $SymptomDB[$ID];
			$temp["Number"] = trim($Number);
			array_push($result_json, $temp);
		}
	}
	
	return $result_json;
}

?>