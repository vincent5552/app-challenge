<?php
ini_set('memory_limit', '256M');

$path = "images/CDCTXFluMap/";
$outputpath = "images/CDCTXFluMapCheck/";
$base = "2013_11.png";
$file =$_GET["file"];

//image RGB array
$image = imagecreatefrompng($path.$base);
$width = imagesx($image);
$height = imagesy($image);

$image_s = imagecreatefrompng($path.$file);
$width_s = imagesx($image_s);
$height_s = imagesy($image_s);
	
$left = $width-1;
$right = 0;
$top = $height-1;
$bottom = 0;

$left_s = $width_s-1;
$right_s = 0;
$top_s = $height_s-1;
$bottom_s = 0;

for ($y = 0; $y < $height; $y++) {
	for ($x = 0; $x < $width; $x++)	{
		$rgb = imagecolorat($image, $x, $y);
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
			
		if($r!=255||$g!=255||$b!=255) {
			if($x < $left)
				$left = $x;
			if($x > $right)
				$right = $x;
			if($y < $top)
				$top = $y;
			if($y > $bottom)
				$bottom = $y;
		}
	}
}

for ($y = 0; $y < $height_s; $y++) {
	for ($x = 0; $x < $width_s; $x++)	{
		$rgb = imagecolorat($image_s, $x, $y);
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
			
		if($r!=255||$g!=255||$b!=255) {
			if($x < $left_s)
				$left_s = $x;
			if($x > $right_s)
				$right_s = $x;
			if($y < $top_s)
				$top_s = $y;
			if($y > $bottom_s)
				$bottom_s = $y;
		}
	}
}

echo "right-left difference in base image ".($right - $left).", right-left difference in input image ".($right_s - $left_s)."<br>";
echo "bottom-top difference in base image ".($bottom - $top).", bottom-top difference in input image ".($bottom_s - $top_s)."<br>";

if(($right - $left) == ($right_s - $left_s) && (($bottom - $top) == ($bottom_s - $top_s))) {
	echo "no scaling needed<br>";
	$lines = file("TexasBoxes.txt");

	$grey = imagecolorallocate($image, 128, 128, 128);
	foreach($lines as $line_num => $line) {
		$info = explode(",", $line);
			
		$left_c = $info[1];
		$top_c = $info[2];
		$right_c = $info[3];
		$bottom_c = $info[4];

		$left_new = $left_s + $left_c - $left;
		$top_new = $top_s + $top_c - $top;
		$right_new = $right_s + $right_c - $right;
		$bottom_new = $bottom_s + $bottom_c - $bottom;
		
		//experience adjust
		imagefilledrectangle($image_s , $left_new, $top_new, $right_new, $bottom_new, $grey);
		echo trim($info[0]).",".$left_new.",".$top_new.",".$right_new.",".$bottom_new;
		echo "<br>";
	}
	imagepng($image, $outputpath.$file);
	
} else {
	echo "scaling are required, bad... scaling is just implemented, please check the result carefully<br>";
	
	$lines = file("TexasBoxes.txt");

	$grey = imagecolorallocate($image, 128, 128, 128);
	foreach($lines as $line_num => $line) {
		$info = explode(",", $line);
			
		$left_c = $info[1];
		$top_c = $info[2];
		$right_c = $info[3];
		$bottom_c = $info[4];

		$left_new = (int)($left_s + (double)($left_c - $left) / ($right- $left) * ($right_s - $left_s));
		$top_new = (int)($top_s + (double)($top_c - $top) / ($bottom- $top) * ($bottom_s - $top_s));
		$right_new = $left_new + 2;
		$bottom_new = $top_new + 2;
		
		//experience adjust
		imagefilledrectangle($image_s , $left_new, $top_new, $right_new, $bottom_new, $grey);
		echo trim($info[0]).",".$left_new.",".$top_new.",".$right_new.",".$bottom_new;
		echo "<br>";
	}
	imagepng($image_s, $outputpath.$file);
}

?>