<?php
ini_set('memory_limit', '256M');

$lines = file("filenames.txt");
$cnt = 0;
foreach($lines as $line_num => $line) {
	//for the limit of memory
	$cnt++;
	if($cnt<168)
		continue;

	//image RGB array
	$image = imagecreatefrompng(trim($line));
	$width = imagesx($image);
	$height = imagesy($image);
	
	$left = $width-1;
	$right = 0;
	$top = $height-1;
	$bottom = 0;

	$sum_x = 0;
	$sum_y = 0;
	$red_points = 0;
	
	for ($y = 0; $y < $height; $y++)
	{
		for ($x = 0; $x < $width; $x++)
		{
			$rgb = imagecolorat($image, $x, $y);
			$r = ($rgb >> 16) & 0xFF;
			$g = ($rgb >> 8) & 0xFF;
			$b = $rgb & 0xFF;
			
			if($r!=255||$g!=255||$b!=255) {
				if($x < $left)
					$left = $x;
				if($x > $right)
					$right = $x;
				if($y < $top)
					$top = $y;
				if($y > $bottom)
					$bottom = $y;
			}
			if($r==255&&$g==0&&$b==0) {
				$sum_x += $x;
				$sum_y += $y;
				$red_points++;
			}
		}
	}
	$center_x = (int)($sum_x/$red_points);
	$center_y = (int)($sum_y/$red_points);
	//check center
	$check = true;
	for($x = $center_x-1; $x < $center_x + 2; $x++) {
		for($y = $center_y-1; $y < $center_y + 2; $y++) {
			$rgb = imagecolorat($image, $x, $y);
			$r = ($rgb >> 16) & 0xFF;
			$g = ($rgb >> 8) & 0xFF;
			$b = $rgb & 0xFF;
			if($r==255&&$g==0&&$b==0) {
			} else {
				$check = false;
			}
		}
	}

	if($check)
		echo trim($line).",".$center_x.",".$center_y.",".$top.",".$bottom.",".$left.",".$right;
	else
		echo $line.",bad";
	echo "<br>";
}

?>